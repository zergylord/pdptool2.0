function [valid, data,errmsg] = read_pair_srndata(Filename)
global net;
valid=0;
data=[];
errmsg='';
slocal =0;
ninputs = net.ninputs;
noutputs = net.noutputs;
if numel(net.pool) <=1
   return;
end
fid = fopen (Filename,'r');
tline='';
while isempty(tline)
      tline = strtrim(fgetl(fid));
end
if strncmpi(tline,'seqlocal',8) 
   slocal = 1;
end
if slocal == 1
   str = strtrim(tline(9:end));
   grammarstr = strsplit(' ',str);
   z = cellfun('isempty',grammarstr);
   grammarstr = grammarstr(~z);
   tline='';
   while isempty(tline)
         tline = strtrim(fgetl(fid));      
   end
   lineno=1;
   while tline ~= -1  
         [pname rem] = strtok(tline);
         patt_string = regexp(rem,'\S','match');  % get a list of non-white space entries of the line;
         ipattern = patt_string;
         sequence = struct('ipat',[],'tpat',[]);
         for seqn= 1: numel(patt_string)-1
             sequence(seqn).ipat = double(ismember(grammarstr, patt_string(seqn)));
             sequence(seqn).tpat = double(ismember(grammarstr, patt_string(seqn+1)));
             data(lineno).ipattern = ipattern;
             data(lineno).sequence = sequence;
         end
         data(lineno).pname = pname;
         tline = fgetl(fid);
         if ischar(tline) && isempty(strtrim(tline))
            continue;
         end
         lineno=lineno + 1;
   end   
else
%    tline='';
%    while isempty(tline)
%          tline = strtrim(fgetl(fid));      
%    end
   lineno=1;   %seqpattern does not need header
   while tline ~= -1 
         data(lineno).pname = strtrim(tline);
         try
            tline = strtrim(fgetl(fid));
         catch
            break;
         end
         seqn =0;
         sequence = struct('ipat',[],'tpat',[]);             
         while ~strncmpi(tline,'end',3)
               pstring = strsplit(' ',tline);  
               ind = ~cellfun(@isempty,pstring);
               pstring = pstring(ind);
               seqn =  seqn +1;
               sequence(seqn).ipat = (str2num(char(pstring{1:ninputs})))';
               sequence(seqn).tpat = (str2num(char(pstring{ninputs+1 : end})))';
               try
                   tline = strtrim(fgetl(fid));
               catch
                   break;
               end                   
          end
          data(lineno).ipattern = ' ';
          data(lineno).sequence = sequence;             
          tline = fgetl(fid);
          if ischar(tline) && isempty(strtrim(tline))
             continue;
          end
          lineno=lineno + 1;
   end
end
valid =1;