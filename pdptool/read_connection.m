function connect_mat = read_connection(varargin)
global iacnet readhandles;
connect_mat=0.0;
if nargin==1
   connect_mat = load(varargin{1});
   return;
else
   if nargin == 2 
      if varargin{2}(1) == 't'
         connect_mat = load(varargin{1})';
         return;
      else
         fprintf(1,'If called with two arguments, second argument must be ''transpose'' or short form of it');
      end
   else
      if nargin < 4
         disp('Not sufficient number of arguments');
         return;
      end
   end
end
if  isempty(iacnet) || ~isfield(iacnet,'pool')
    rh = numel(readhandles)+1;
    readhandles(rh).handle = @read_connection;
    readhandles(rh).arg(1,:)= varargin;
    return;
end
pools= iacnet.pool;
pool_1 = varargin{2};
pool_2 = varargin{3};
File=varargin{1};
constraint = varargin{4};
% p1 = strmatch(pool_1,{pools.name},'exact');
% p2 = strmatch(pool_2,{pools.name},'exact');
[tf,p1] = ismember(pool_1,{pools.name});
[tf,p2] = ismember(pool_2,{pools.name});
transposeind=0;
if strcmpi(pools(p1).type,'input') && strcmpi(pools(p2).type,'hidden') && pools(p1).nunits==26
   inum=p1;
   hnum=iacnet.pool(p2).nunits;
else
   if strcmpi(pools(p2).type,'input') && strcmpi(pools(p1).type,'hidden') && pools(p2).nunits==26
      inum = p2;
      hnum=iacnet.pool(p1).nunits;
      transposeind=1;      
   else
       disp('Letter pools must be input type and number of units must be 26');
       return;
   end
end
connect_mat=repmat(0,[hnum pools(inum).nunits]);
% find number of input pools
pfind=strcmpi('input',{iacnet.pool.type});
inpnum = sum(pfind);
porder = 1:inpnum;
pnums = find(pfind);
% get rows equal to hidden layer size ,each row containing as many
% characters as number of input pools
formatstr= sprintf('%%%dc%%*[^\\n]',inpnum);
[words]=textread(File,formatstr,hnum,'headerlines',1);
colnum = porder(pnums == inum);
chars = words(:,colnum);
for i=1:numel(chars)
    pos =chars(i) -'a'+1;
    connect_mat(i,pos)=constraint;
end
if transposeind==1
   connect_mat = connect_mat';
end
    

% n
