function setdisplayobject(objectstr)
global PDPAppdata;
pdp_handle=findobj('tag','mainpdp');
displayobj = PDPAppdata.dispobjects; %getappdata(pdp_handle,'dispobjects');
if isempty(displayobj)
   displayobj=struct('source',{},'position',[],'orient','','dir','','labeltxt','',...
                 'rowrange',[],'colrange',[],'disptype','','vcslope',1);
end
structfields=fieldnames(displayobj);
index=numel(displayobj) +1;
comma_ind = regexp(objectstr,',');
p=1;
for j=1:numel(comma_ind)
    q=comma_ind(j)-1;
    component{j}=objectstr(p:q);
    p=q+2;
end
component{j+1}=objectstr(p:end);
prop_val_pairs = component(2:end);
for i=1:numel(structfields)
       tempcell = strmatch(structfields{i},prop_val_pairs);
       if ~isempty(tempcell)
           property = prop_val_pairs{tempcell};
           value =prop_val_pairs{tempcell+1};
           if ismember(property,{'rowrange','colrange','position','vcslope'})
              value = str2num(value);
           end
           displayobj(index).(property)=value;
       end
end
if strncmp(component{1},'{}',2)
   src={};
else
   src = splitfields(component{1});  %display object source
end
displayobj(index).source = src;
PDPAppdata.dispobjects = displayobj;
% setappdata(pdp_handle,'dispobjects',displayobj);


function fields = splitfields(var)
fields={};
dotind = regexp(var,'\.');
pos = 1;
for i=1:numel(dotind)
    temp =var(pos : dotind(i)-1);
    findpara = regexp(temp,'\(\d+\)'); %look for digits enclosed in paranthesis
    fields{1}{i} = temp(1:findpara-1);
    remtemp=temp(findpara+1:end);
    findnum = regexp(remtemp,'\d');
    fields{2}(i)=3;
    fields{2}(i) = str2double(remtemp(findnum));
    pos = dotind(i) + 1;
end
fields{3}=var(pos:end);