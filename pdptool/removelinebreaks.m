function newlines = removelinebreaks(lines)
newlines = lines;
breaks =find(~cellfun('isempty',regexp(lines,'\.\.\.$')));
if isempty(breaks)
   return;
end
while ~isempty(breaks)
    rem_lines =[];
    for i=1:numel(breaks)
        index = breaks(i);
        tline = lines{index+1};
        if isempty(regexp(tline,'\.\.\.$', 'once'))
           join_lines = [lines{index} lines{index+1}];
           lines{index} = join_lines;
           rem_lines =[rem_lines index+1];
        end
    end
    lines(rem_lines)=[];
    breaks=find(~cellfun('isempty',regexp(lines,'\.\.\.$')));    
end
    
newlines = regexprep(lines,'\.\.\.','');