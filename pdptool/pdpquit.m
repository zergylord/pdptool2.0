function pdpquit
global PDPAppdata;
if isempty(PDPAppdata)
   delete (findobj('tag','mainpdp'));
   return;
end
% Delete any windows that might be open
if PDPAppdata.gui
   p = mfilename('fullpath');
   l = length(mfilename);
   p = p(1:end-l);
   pd = dir(p);
   allf = {pd.name};
   findfigs = strfind(allf,'.fig');
   cfigs = cellfun('isempty',findfigs);
   figtags = regexprep(allf(~cfigs),'\.fig','');
   figtags =[figtags ,{'mainpdp','dispfig','strexplore','setwriteopts','arreditor','aboutfig'}];
   for i = 1:numel(figtags)
       h = findobj('tag',figtags{i});
       if ~isempty(h)
        delete(h);
       end
   end
end
if PDPAppdata.lognetwork
   updatelog('pdpquit;');
end
clear global;
clear functions;
% fclose('all');
delf = sprintf('%s%stemp.m',pwd,filesep);
if exist(delf,'file') ==2
   delete(delf);
end