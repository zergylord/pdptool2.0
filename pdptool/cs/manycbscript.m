ncubes = 100; 
mccycles = 20;
goodlog = 1:ncubes;
x = 0:.5:16;
for i = 1:ncubes
    newstart;
    runprocess ('granularity','cycle','count',mccycles,'range', [1 1;mccycles 16]);
    goodlog(i) = net.goodness;
end
histvals = histc(goodlog,x);
figure; bar(x,histc(goodlog,x)); set(gca,'xtick',[0:16],'xlim',[-.5 16.5],'ylim',[0 100]);
