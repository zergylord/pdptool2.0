



twoEx(testnum,:) = [net.pool(1).activation net.pool(2).activation];



%{
% cycle by cycle record
cube;
seeds = [0 211307 207741 211197 206655];

settestopts ('ncycles',5);

for testNum = 1:5
    cs_reset;
    setseed(seeds(testNum))
    testNum
    for i = 5:5:20
        runprocess ('granularity','cycle','count',1 ,'range', [i-4 1;i 1]);
        twoCBC(i/5, :, testNum) = [net.pool(1).activation net.pool(2).activation];

    end

   
end
%}

