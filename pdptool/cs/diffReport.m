

% Check differences in activation by minusing the two matrices together and
% giving me a. the maximum difference b. where the maximum difference
% occurs c. how many significant differences there are. 

% change the struct to a cell structure, that way it's easier to work with.
% 

sigDif = 0.1;

dAct = abs(TWO.act - THREE.act);


dActReport = ones(7,2); 
dProjVisReport = ones(6,2);
dProjVisHidReport = ones(6,2);
dProjHidReport = ones(6,2);
% differences in activation will be a matrix 6 by 3 s.t. column 1 is the
% maximum difference, column 2 is its index,  column 3 will give me
% the number of significant differences. 





for testnum = 1:7
    dActReport(testnum,1:2) = [max(dAct(:,:,testnum))  sum(dAct(:,:,testnum) > sigDif)];



end




dActReport


        %{
dProjVis = abs(TWO.projVis - THREE.projVis);
dProjVisHid = abs(TWO.projVisHid - THREE.projVisHid);
dProjHid = abs(TWO.projHid - THREE.projHid);
dProjVisReport
dProjVisHidReport
dProjHidReport
    dProjVisReport(testnum,1:2) = max(max(dProjVis(:,:,testnum))); 
         dProjVisReport(testnum,3) = sum(sum(dProjVis(:,:,testnum) > sigDif));
    dProjVisHidReport(testnum,1:2) = max(max((dProjVisHid(:,:,testnum)))) ;
        dProjVisHidReport(testnum,3) = sum(sum(dProjVisHid(:,:,testnum) > sigDif));
    dProjHidReport(testnum,1:2) = max(max(dProjHid(:,:,testnum)));
        dProjHidReport(testnum,3) = sum(sum(dProjHid(:,:,testnum) > sigDif));
        %}
    

