function varargout = edittemplateparams(varargin)
% EDITTEMPLATEPARAMS M-file for edittemplateparams.fig
%      EDITTEMPLATEPARAMS, by itself, creates a new EDITTEMPLATEPARAMS or raises the existing
%      singleton*.
%
%      H = EDITTEMPLATEPARAMS returns the handle to a new EDITTEMPLATEPARAMS or the handle to
%      the existing singleton*.
%
%      EDITTEMPLATEPARAMS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EDITTEMPLATEPARAMS.M with the given input arguments.
%
%      EDITTEMPLATEPARAMS('Property','Value',...) creates a new EDITTEMPLATEPARAMS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before edittemplateparams_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to edittemplateparams_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help edittemplateparams

% Last Modified by GUIDE v2.5 04-Sep-2007 16:27:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @edittemplateparams_OpeningFcn, ...
                   'gui_OutputFcn',  @edittemplateparams_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before edittemplateparams is made visible.
function edittemplateparams_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to edittemplateparams (see VARARGIN)

global net PDPAppdata;
tsize = PDPAppdata.templatesize;
set(findobj('-regexp','tag','trows*','style','edit','-and'),...
    'String',tsize(1));
set(findobj('-regexp','tag','tcols*','style','edit','-and'),...
    'String',tsize(2));
set(findobj('-regexp','tag','csize*','style','edit','-and'),...
    'String',PDPAppdata.cellsize);
set(findobj('tag','filenameedit'),'String',net.templatefile);
% Choose default command line output for edittemplateparams
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes edittemplateparams wait for user response (see UIRESUME)
% uiwait(handles.edittemplateparams);


% --- Outputs from this function are returned to the command line.
function varargout = edittemplateparams_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function trowsedit_Callback(hObject, eventdata, handles)


function trowsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tcolsedit_Callback(hObject, eventdata, handles)


function tcolsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function csizeedit_Callback(hObject, eventdata, handles)


function csizeedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function trowsnewedit_Callback(hObject, eventdata, handles)


function trowsnewedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tcolsnewedit_Callback(hObject, eventdata, handles)


function tcolsnewedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function csizenewedit_Callback(hObject, eventdata, handles)


function csizenewedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function savechk_Callback(hObject, eventdata, handles)
status = 'off';
if get(hObject,'Value')
   status = 'on';
end
set(handles.filenameedit,'enable',status);
set(handles.changefilebtn,'enable',status)

function filenameedit_Callback(hObject, eventdata, handles)


function filenameedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function changefilebtn_Callback(hObject, eventdata, handles)
[filename, pathname] = uiputfile('*.tem', 'Save to template file');
if isequal([filename,pathname],[0,0])
    return;
end
File = fullfile(pathname,filename);
set(handles.filenameedit,'String',File);

function okbtn_Callback(hObject, eventdata, handles)
applychanges;
delete(handles.edittemplateparams);

function applybtn_Callback(hObject, eventdata, handles)
applychanges;

function cancelbtn_Callback(hObject, eventdata, handles)
delete(handles.edittemplateparams);

function applychanges
global net;
handles = guihandles;
csize = str2double(get(handles.csizenewedit,'String'));
tr = str2double(get(handles.trowsnewedit,'String'));
tc = str2double (get(handles.tcolsnewedit,'String'));
tsize = [tr tc];
args = {'cellsize',csize,'templatesize',tsize};
if get(handles.savechk,'Value')
   n = numel(args) + 1;
   args(n:n+1) = {'save','yes'};
   fname = get(handles.filenameedit,'String');
   if ~strcmp(fname,net.templatefile)
      n = numel(args) + 1;       
      args(n:n+1) = {'saveto',fname};
   end
end
resettemplate(args{1:end});
