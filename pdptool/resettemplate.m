function resettemplate(varargin)
global net PDPAppdata;
layout.cellsize = PDPAppdata.cellsize;
layout.save = 'no';
layout.saveto = net.templatefile;
layout.templatesize = PDPAppdata.templatesize;
layout = parse_pairs(layout,varargin);
if isempty(layout.cellsize) || layout.cellsize < 0 ||...
   ~isnumeric(layout.cellsize)
    error('Incorrect value of cellsize');
end
if isempty(layout.templatesize) || any(layout.templatesize < 0) ||...
   ~isnumeric(layout.cellsize)
    error('Incorrect value of cellsize');
end
save = 0;
tlines = [];
if strncmpi(layout.save,'yes', length(layout.save))
   save = 1;
   [fpath fname fext] = fileparts(layout.saveto);
   if ~strcmpi(fext,'.tem')
       fprintf(1,['WARNING : Template file should be of .tem extension,'...
              'creating a .tem file\n']);
       fext = '.tem';
   end
   layout.saveto = fullfile(fpath,[fname fext]);
   if layout.cellsize == PDPAppdata.cellsize && ...
      isequal(layout.templatesize,PDPAppdata.templatesize) 
      if ~strcmpi(layout.saveto,net.templatefile)
         copyfile(net.templatefile,layout.saveto);
      end
      return;
   end
   tlines = textread(net.templatefile,'%s','delimiter','\n');
   if layout.cellsize ~= PDPAppdata.cellsize   
       cl = find(~cellfun('isempty',regexpi(tlines,'cellsize')));
       if ~isempty(cl)
          parts = strsplit('cellsize',tlines{cl(end)},'include');
          parts{end} = sprintf(' %d;',layout.cellsize);
          tlines{cl(end)} = [parts{1:end}];
       end
   end
   if ~isequal(layout.templatesize ,PDPAppdata.templatesize)
       tl = find(~cellfun('isempty',regexpi(tlines,'template')));
       if ~isempty(tl)
          parts = strsplit('template',tlines{tl(end)},'include');
          parts{end} = sprintf(' %s;',mat2str(layout.templatesize));
          tlines{tl(end)} = [parts{1:end}];
       end
   end
end
oldlayout = PDPAppdata.templatesize .* PDPAppdata.cellsize;
newlayout = layout.templatesize .* layout.cellsize;
dispobjs = PDPAppdata.dispobjects;
for i = 1:numel(dispobjs)
    fp = dispobjs(i).position;
    newfp(1) = fix(fp(1)/oldlayout(2) * newlayout(2));
    newfp(2) = fix(fp(2)/oldlayout(1) * newlayout(1));
    if strcmp(dispobjs(i).disptype,'label')
       newfp(3) = fp(3);
    else
       newfp(3) = fix(fp(3)/PDPAppdata.cellsize * layout.cellsize);
    end
    newfp(4) = fix(fp(4)/PDPAppdata.cellsize * layout.cellsize);
    dispobjs(i).position = newfp;
    if save && ~isempty(tlines)
       pat = sprintf('%s',mat2str(fp));
       tl = find(~cellfun('isempty',strfind(tlines,pat)));
       if ~isempty(tl)
          parts = strsplit('position',tlines{tl(end)},'include');
          t = regexp(parts{end},{'[',']'});
          if numel(t) ~= 2
             error('%s : Unbalanced brackets in position parameter',...
                   tlines{tl(end)});
          end
          parts{end} = sprintf('%s%d %d %d %d%s',parts{end}(1:t{1}),...
                       newfp(1),newfp(2), newfp(3),newfp(4),...
                       parts{end}(t{2}:end));
          tlines{tl(end)} = [parts{1:end}];
       end
    end
end
if save && ~isempty(tlines)
   try
     fd = fopen(layout.saveto,'w');
     fprintf(fd,'%s\n',tlines{1:end});
     fclose(fd);
   catch
     e = lasterror;
     fprintf(1,'ERROR : %s', e.message);
   end
end   
PDPAppdata.cellsize = layout.cellsize;
PDPAppdata.templatesize = layout.templatesize;
PDPAppdata.dispobjects = dispobjs;
if PDPAppdata.lognetwork
   statement = sprintf ('%s (',mfilename);
   for i = 1:numel(varargin)
       item = varargin{i};
       if ischar(item)
          statement = sprintf('%s''%s'',',statement,item);
       elseif ~isscalar(item)
          statement = sprintf('%s %s,',statement,mat2str(item));
       else
           statement = sprintf('%s %s,',statement,num2str(item));
       end
   end
   statement(end:end+1)=');';
   updatelog(statement);
end

   