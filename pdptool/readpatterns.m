function data = readpatterns(File)
global net;
data = [];
errmsg = '';
valid = 1;

%Check net type of current network and do pair wise reading if pa,bp and
%non-pair otherwise
type = net.type;
switch type
    case {'pa','bp'}
        %do input-target pair pattern read
        [valid ,data, errmsg] = read_pair_data(File);
    case 'srn'
        [valid ,data, errmsg] = read_pair_srndata(File);
    case 'rbp'
        %do input-target pair pattern read
        [valid ,data, errmsg] = read_pair_rbpdata(File);
    case {'cs','iac','cl'}
        [valid ,data, errmsg] = read_nonpair_data(File);
    case {'tdbp'}
        %data is just the environment class name in this case
        [valid, data, errmsg] = read_envir_data(File);
    otherwise
end
if (valid ==0)
   if ~isempty(errmsg)
       errmsg = sprintf('ERROR in pattern file: %s ',errmsg);
   end   
   fprintf(1,errmsg);
end