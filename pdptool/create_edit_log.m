function varargout = create_edit_log(varargin)
% CREATE_EDIT_LOG M-file for create_edit_log.fig
%      CREATE_EDIT_LOG, by itself, creates a new CREATE_EDIT_LOG or raises the existing
%      singleton*.
%
%      H = CREATE_EDIT_LOG returns the handle to a new CREATE_EDIT_LOG or the handle to
%      the existing singleton*.
%
%      CREATE_EDIT_LOG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CREATE_EDIT_LOG.M with the given input arguments.
%
%      CREATE_EDIT_LOG('Property','Value',...) creates a new CREATE_EDIT_LOG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before create_edit_log_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to create_edit_log_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help create_edit_log

% Last Modified by GUIDE v2.5 23-Feb-2008 16:17:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @create_edit_log_OpeningFcn, ...
                   'gui_OutputFcn',  @create_edit_log_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before create_edit_log is made visible.
function create_edit_log_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to create_edit_log (see VARARGIN)

% Choose default command line output for create_edit_log
handles.output = hObject;
global outputlog listlog;
if isempty(outputlog)
   set(handles.logfilepop,'String',{'none'});
   set(handles.logeditbtn,'Enable','off');
else
   lfiles={outputlog.file};
   set(handles.logfilepop,'String',lfiles,'Value',1);
   set(handles.logeditbtn,'Enable','on');   
   listlog.fname = lfiles{1};
   listlog.index = 1;
end
set(handles.binarychk,'Value',1);
set(handles.extedit,'String','.mat');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes create_edit_log wait for user response (see UIRESUME)
% uiwait(handles.create_edit_log);


% --- Outputs from this function are returned to the command line.
function varargout = create_edit_log_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function basenameedit_Callback(hObject, eventdata, handles)


function basenameedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function extedit_Callback(hObject, eventdata, handles)


function extedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function binarychk_Callback(hObject, eventdata, handles)
global listlog;
listlog.binarychk = get(hObject,'Value');
if get(hObject,'Value') == 1
   set(handles.extedit,'String','.mat');
else
   set(handles.extedit,'String','.out');
end

function createnewbtn_Callback(hObject, eventdata, handles)
global listlog;
base = get(handles.basenameedit,'String');
if isempty(base) || all(isspace(base))
   return;
end
lfiles = cellstr(get(handles.logfilepop,'String'));
ext = get(handles.extedit,'String');
f = getfilename(base,ext);
if any(strcmp(lfiles,f))
   return;
end
listlog.fname = f;
listlog.index = -1;
listlog.base =  base;
listlog.ext = ext;
listlog.binarychk = get(handles.binarychk,'Value');
popindex = 1;
if ~strcmpi(lfiles{1},'none')
   popindex = numel(lfiles) + 1;
end
lfiles{popindex} = f;
set(handles.logfilepop,'String',lfiles,'Value',popindex);
set(handles.logeditbtn,'enable','on');

function logfilepop_Callback(hObject, eventdata, handles)
global outputlog listlog;
val = get(hObject,'Value');
lnames = cellstr(get(hObject,'String'));
listlog.fname = lnames{val};
if val > numel(outputlog)
   listlog.index = -1;
else
   listlog.index = val;
end

function logfilepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function logeditbtn_Callback(hObject, eventdata, handles)

setwriteopts;
delete(handles.create_edit_log);

