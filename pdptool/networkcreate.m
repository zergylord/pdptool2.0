function varargout = networkcreate(varargin)
% NETWORKCREATE M-file for networkcreate.fig
%      NETWORKCREATE, by itself, creates a new NETWORKCREATE or raises the existing
%      singleton*.
%
%      H = NETWORKCREATE returns the handle to a new NETWORKCREATE or the handle to
%      the existing singleton*.
%
%      NETWORKCREATE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NETWORKCREATE.M with the given input arguments.
%
%      NETWORKCREATE('Property','Value',...) creates a new NETWORKCREATE or raises
%      the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before networkcreate_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to networkcreate_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help networkcreate

% Last Modified by GUIDE v2.5 20-Jul-2007 14:57:54

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @networkcreate_OpeningFcn, ...
                   'gui_OutputFcn',  @networkcreate_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before networkcreate is made visible.
function networkcreate_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to networkcreate (see VARARGIN)

% Choose default command line output for networkcreate

figch = get(hObject,'children');
handles.output = hObject;
handles.ht=uitoolbar(hObject,'Tag','scripttool');
fname='cmapout.bmp';
[X,map]=imread(fname);
fname1='cmapin.bmp';
[X1,map]=imread(fname1); %X1 is saved for switching CData value with it,when button is toggled
% X=ind2rgb(X,map);
uitoggletool(handles.ht,'tag','scripttoggle','CData',X,...
                  'TooltipString','View Script','visible','off',...
                  'UserData',X1,'OnCallback',{@oncallback_toggletool},...
                  'OffCallback',{@offcallback_toggletool});
fname='save.bmp';
[X,map]=imread(fname,'bmp');
% X=ind2rgb(X,map);
uipushtool(handles.ht,'CData',X,'TooltipString','Save Network',...
                'tag','savetool','Visible','off','ClickedCallback',{@savenetwork});
fname='StopControl.bmp';
[X,map]=imread(fname,'bmp');
uipushtool(handles.ht,'CData',X,'TooltipString','Abort network creation',...
                'tag','aborttool','Visible','off','ClickedCallback',{@abortnetwork});
mainfigpos = get(hObject,'Position');
set(hObject,'Resize','on');
scriptpnlpos = get(handles.scriptpanel,'Position');
mainfigpos(2)= mainfigpos(2) +  scriptpnlpos(4);
mainfigpos(4)= mainfigpos(4) -  scriptpnlpos(4);
cobjs = figch(figch ~= handles.scriptpanel);
cpos = get(cobjs,'Position');
cposnew = cellfun(@(x) [x(1) x(2) - scriptpnlpos(4) x(3) x(4)],cpos,...
              'UniformOutput',false);
set(cobjs,{'Position'},cposnew,'UserData',cpos);
% for i=1:size(figch,1)
%     obj = figch(i);
%     if ~strcmp(get(obj,'tag'),'scriptpanel')
%         objpos = get(obj,'Position');
%         set(obj,'Position',[objpos(1), objpos(2) - scriptpnlpos(4) ,objpos(3), objpos(4)]);
%     end
% end
set(hObject,'Position',mainfigpos);
types={'pa','iac','cs','bp','srn','rbp','cl'};
setappdata(hObject,'nettypes',types);
% Update handles structure
guidata(hObject, handles);
% UIWAIT makes networkcreate wait for user response (see UIRESUME)
% uiwait(handles.networkcreate);


% --- Outputs from this function are returned to the command line.
function varargout = networkcreate_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function netnameedit_Callback(hObject, eventdata, handles)
netinfo = getappdata(handles.nettypepop,'netinfo');
netinfo.name=get(hObject,'String');
setappdata(handles.nettypepop,'netinfo',netinfo);
if strcmpi (get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function netnameedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function oncallback_toggletool(hObject,eventdata)
handles = guihandles;
refreshscript(handles);
mainfigpos = get(handles.networkcreate,'Position');
spanel = handles.scriptpanel;
set(spanel,'visible','on');
X=get(hObject,'UserData');
Y=get(hObject,'CData');
set(hObject,'TooltipString','Hide Script','CData',X,'UserData',Y);
spanelpos = get(spanel,'Position');
mainfigpos(2)= mainfigpos(2) - spanelpos(4);
mainfigpos(4)= mainfigpos(4) + spanelpos(4);
figch = get(handles.networkcreate,'children');
cobjs = figch((figch ~= spanel) & (figch ~= handles.scripttool));
cpos = get(cobjs,'Position');
cposnew = cellfun(@(x) [x(1) x(2) + spanelpos(4) x(3) x(4)],cpos,...
              'UniformOutput',false);
set(cobjs,{'Position'},cposnew,'UserData',cpos);
% 
% for i=1:size(figch,1)
%     obj = figch(i);
%     if (~strcmp(get(obj,'tag'),'scriptpanel') && ~strcmp(get(obj,'tag'),'scripttool'))
%         objpos = get(obj,'Position');
%         set(obj,'Position',[objpos(1) (objpos(2) + spanelpos(4)) objpos(3) objpos(4)]);
%     end
% end
set(handles.networkcreate,'Position',mainfigpos);

function offcallback_toggletool(hObject,eventdata)
handles=guihandles;
mainfigpos = get(gcbf,'Position');
spanel = handles.scriptpanel;
set(spanel,'visible','off');
X=get(hObject,'UserData');
Y=get(hObject,'CData');
set(hObject,'TooltipString','View Script','CData',X,'UserData',Y);
spanelpos = get(spanel,'Position');
mainfigpos(2)= mainfigpos(2) + spanelpos(4);
mainfigpos(4)= mainfigpos(4) - spanelpos(4);
figch = get(gcbf,'children');
cobjs = figch((figch ~= spanel) & (figch ~= handles.scripttool));
cpos = get(cobjs,'Position');
cposnew = cellfun(@(x) [x(1) x(2) - spanelpos(4) x(3) x(4)],cpos,...
              'UniformOutput',false);
set(cobjs,{'Position'},cposnew,'UserData',cpos);
% for i=1:size(figch,1)
%     obj = figch(i);
%     if (~strcmp(get(obj,'tag'),'scriptpanel') && ~strcmp(get(obj,'tag'),'scripttool'))
%         objpos = get(obj,'Position');
%         set(obj,'Position',[objpos(1), objpos(2) - spanelpos(4) ,objpos(3), objpos(4)]);
%     end
% end
set(gcbf,'Position',mainfigpos);

function savenetwork(hObject,evendata)
global PDPAppdata;
h=guihandles;
pdp_handle=findobj('tag','mainpdp');
[netname, pathname] = uiputfile({'*.net','Network Files (*.net)'}, 'New Network');
% If "Cancel" is selected then return
if isequal([netname,pathname],[0,0])
	return
else
 File = fullfile(pathname,sprintf('%s', netname));
end
fid = fopen(File, 'wt');
data= get(h.scriptedit,'String');
for i=1:numel(data)
    fprintf(fid,'%s;\n',data{i});
end
fclose(fid);
loadscript(File);
% logind = getappdata(pdp_handle,'lognetwork');
if PDPAppdata.lognetwork
   stmt = sprintf('loadscript (''%s'');',File);
   updatelog(stmt);
end
script = PDPAppdata.netscript; %getappdata(pdp_handle,'netscript');
netind = size(PDPAppdata.networks,1) ; %getappdata(pdp_handle,'networks'),1);
script{netind} = File;
PDPAppdata.netscript = script;
% setappdata(pdp_handle,'netscript',script);
delete(h.networkcreate);

function nettypepop_Callback(hObject, eventdata, handles)
h=guihandles;
val=get(hObject,'Value');
type=get(hObject,'String');
typepos = get(hObject,'Position');
set(hObject,'visible','off');
set(h.nettypetext,'Position',typepos,'visible','on','String',type{val});
set(h.seltype,'visible','on');
set(h.scripttoggle,'visible','on');
set(h.poolbtn,'enable','on');
set(h.savetool,'visible','on','separator','on');
set(h.aborttool,'visible','on','separator','on');
netinfo = getappdata(hObject,'netinfo');
types= getappdata(handles.networkcreate,'nettypes');   
netinfo.name = get(handles.netnameedit,'String');
netinfo.type=types{val};
switch get(hObject,'Value')
    case 1  %pa
        list={'input','output'};
    case 2 %iac
        list={'input','hidden'};
    case 3  %cs
        list={'input'};        
    case {4,5}  %feed forward bp and simple recurrent bp
        list={'input','hidden','output'};
%         set(h.bptypepop,'Visible','on');
%         netinfo.bptype = 'sff';
    case 6 %recurrent bp through time
        list={'input','hidden','output','inout'};
        set(findobj('-regexp','tag','rbptt*'),'Visible','on');
    case 7  %cl
        list={'input','hidden'};        
        set(h.cltopochk,'Visible','on');
        netinfo.standardcl = 'on';
    otherwise
        list={'input','hidden','output','inout'};
end
setappdata(hObject,'netinfo',netinfo);
ptypelist=cellstr(get(handles.poolstypepop,'String'));
ptypelist=[ptypelist, list];
set(handles.poolstypepop,'String',ptypelist);
if  strcmpi(get(handles.scriptedit,'Visible'),'on')
    refreshscript(handles);
end

function nettypepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function seltype_Callback(hObject, eventdata, handles)
h=guihandles;
qstring = sprintf('This will restart the network creation process.\nDo you want to continue?');
buttons = questdlg(qstring);
if strcmp(buttons,'Yes')
    set(h.nettypetext,'visible','off');
    set(h.nettypepop,'visible','on','Value',1);
    set(h.netnameedit,'String','net1');
    set(h.scripttoggle,'visible','off','state','off');
    set(h.savetool,'visible','off','separator','off');
    set(h.aborttool,'visible','off','separator','off');
    set(get(h.placeholderpnl,'children'),'Visible','off');
    set(h.scriptedit,'String', ' ');
    setappdata(h.currplist,'pools',get(h.currplist,'UserData'));
    set(h.currplist,'String',' ');
    set(h.projsenderpop,'String',{'bias'},'Value',1);
    set(h.projrcvrpop,'String',' ');
    set(h.cltopochk,'Visible','off');
    set(findobj('-regexp','tag','rbptt*'),'Visible','off');
    setappdata(h.nettypepop,'netinfo',[]);
%     setappdata(h.projdefconstrbtn,'mat',[]);
    ptypelist=get(h.poolstypepop,'String');
    if numel(ptypelist) > 1
        ptypelist(2:end)=[];
    end
    set(h.poolstypepop,'String',ptypelist,'Value',1);
%     setappdata(h.sigmabtn,'sigvector',[]);
    set(h.projconstredit,'String','0.0');
    set(h.poolbtn,'enable','off');
    set(h.projbtn,'enable','off');
%     set(h.patfilebtn,'enable','off');
    set(hObject,'visible','off');
end


function scriptedit_Callback(hObject, eventdata, handles)


function scriptedit_CreateFcn(hObject, eventdata, handles)
% createscript(hObject);
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function scriptclosebtn_Callback(hObject, eventdata, handles)
h=guihandles;
set(h.scripttoggle,'state','off');


function abortnetwork(hObject,eventData)
h=guihandles;
delete(h.networkcreate);


function seedtxt_Callback(hObject, eventdata, handles)
% seed = get(hObject,'String');
% sedit = handles.scriptedit;
% udata = get(sedit,'UserData');
% udata.network.seed=sprintf('  network.seed=%s;',seed);
% set(sedit,'UserData',udata);

function seedtxt_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function seedpnl_SelectionChangeFcn(hObject, eventdata, handles)
if strcmp(get(hObject,'tag'),'rseedradio')
   set(handles.seedtxt,'String',' ','enable','off');
%    sedit = handles.scriptedit;
%    udata = get(sedit,'UserData');
%    if isfield(udata.network,'seed')
%        udata.network=rmfield(udata.network,'seed');
%    end
%    set(sedit,'UserData',udata);
else
   set(handles.seedtxt,'enable','on');
end

function poolstypepop_Callback(hObject, eventdata, handles)
status='off';
pname='';
punits='';
if get(hObject,'Value') ~=1
   status='on';
end
set(handles.pooladdbtn,'enable',status);
set(handles.poolnameedit,'String', pname,'Visible','on');
set(handles.currplist,'Visible','off');
set(handles.poolunitsedit,'String',punits);
set(handles.topoxedit,'String','');
set(handles.topoyedit,'String','');
set(handles.topolrangeedit,'String','1.0');
set(handles.toposwanchk,'Value',0);

function poolstypepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function poolnameedit_Callback(hObject, eventdata, handles)


function poolnameedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function poolunitsedit_Callback(hObject, eventdata, handles)
nettypeval = get(handles.nettypepop,'Value');
if ismember(nettypeval,[2 3 7]) %cs ,iac, cl
%    set(handles.sigmabtn,'enable','on');
   set(handles.unamepop,'enable','on');
   if strcmpi(get(handles.topoxedit,'Visible'),'on')
       units = get(handles.poolunitsedit,'String');
       if ~isnan(units)
           set(handles.topoxedit,'String',1);
           set(handles.topoyedit,'String',units);
       end
   end
end

function poolunitsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end







function poolbtn_Callback(hObject, eventdata, handles)
set(handles.projbtn,'enable','on');
set(get(handles.placeholderpnl,'children'),'Visible','off');
pobjects = findobj(handles.placeholderpnl,'-regexp','tag','pool*');
set(pobjects,'Visible','on')
nettypeval = get(handles.nettypepop,'Value');
if ismember(nettypeval,[2 3 7]) %cs ,iac, cl
%    set(handles.sigmabtn,'Visible','on');
   set(handles.unamepop,'Visible','on');
end
if nettypeval==7 && get(handles.cltopochk,'Value') ~= 0 
   h_topo = findobj(handles.placeholderpnl,'-regexp','tag','^topo*');
   set(h_topo,'Visible','on');
end


function pooladdbtn_Callback(hObject, eventdata, handles)
nunits  = str2double(get(handles.poolunitsedit,'String'));
if isnan(nunits)
   set(handles.poolunitsedit,'backgroundcolor','red');
   disp('Incorrect format for number of units');
   return;
else
   set(handles.poolunitsedit,'backgroundcolor','white');
end
pools = getappdata(handles.currplist,'pools');
newpool=get(handles.poolnameedit,'String');
pnames = cellstr(get(handles.currplist,'String'));
if strcmpi(get(handles.currplist,'Visible'),'on')
   pnum = get(handles.currplist,'Value');
else
    [tf,loc]=ismember(pnames,newpool);
    if any(loc)
        set(handles.poolnameedit,'backgroundcolor','red');
        disp('Pool name already exists');
        return;
    else
        set(handles.poolnameedit,'backgroundcolor','white');
    end    
   pnum = numel(pools)+1;
   pnames{pnum}=get(handles.poolnameedit,'String');   
end
ind = get(handles.poolstypepop,'Value');
ptypes=get(handles.poolstypepop,'String');
pools(pnum).name= pnames{pnum};
pools(pnum).type=ptypes{ind};
pools(pnum).nunits=nunits;
nettypeval=get(handles.nettypepop,'Value');
if ismember(nettypeval,[2 3 7]) %cs ,iac, cl
%    sigvec= getappdata(handles.sigmabtn,'sigmavector');
%    vec = repmat(0,1,nunits);
%    if ~isempty(sigvec)
%        vec= sigvec;
%    end
%    pools(pnum).sigma = vec;
   unamef=getappdata(handles.unamepop,'unamefile');
   if ~isempty(unamef)
       pools(pnum).unames = sprintf('textread(''%s'',''%%s'')',unamef);
   end  
end
if nettypeval==7
   tx = str2double(get(handles.topoxedit,'String'));
   ty = str2double(get(handles.topoyedit,'String'));
   shape=[1 nunits];   
   if ~isnan(tx) && ~isnan(ty)
       if tx*ty == nunits
          shape = [tx ty];
       end
   end
   pools(pnum).geometry =shape;
   pools(pnum).lrange = str2double(get(handles.topolrangeedit,'String'));
   pools(pnum).swan = get(handles.toposwanchk,'Value');
end
if strcmpi(get(handles.currplist,'Visible'),'off')
   set(handles.poolstypepop,'Value',1);
   set(handles.pooladdbtn,'enable','off');
   set(handles.poolnameedit,'String', '');
   set(handles.poolunitsedit,'String','');
%    set(handles.sigmabtn,'enable','off');
   set(handles.unamepop,'enable','off');
   set(handles.topoxedit,'String','');
   set(handles.topoyedit,'String','');
%    setappdata(handles.sigmabtn,'sigmavector',[]);
   setappdata(handles.unamepop,'unamefile',[]);
end
setappdata(handles.currplist,'pools',pools);
set(handles.currplist,'String',pnames);
if strcmpi(get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function projbtn_Callback(hObject, eventdata, handles)
set(get(handles.placeholderpnl,'children'),'Visible','off');
pobjects = findobj(handles.placeholderpnl,'-regexp','tag','proj*');
set(pobjects,'Visible','on');
pools = getappdata(handles.currplist,'pools');
slist = cellstr(get(handles.projsenderpop,'String'));
rlist = cellstr(get(handles.projrcvrpop,'String'));
if ~isempty(pools)
    for i = 1:numel(pools)    
        slist{i+1} = pools(i).name;
        rlist{i} = pools(i).name;
    end
end
nettypeval= get(handles.nettypepop,'Value');
switch nettypeval
    case 1
        set(findobj('-regexp','tag','lrate*'),'Visible','on');
        optslist={'Scalar','Read file','Set by hand'};        
    case 4
       optslist={'Scalar','Random','pRandom','nRandom'};  
       set(findobj('-regexp','tag','lrate*'),'Visible','on');       
    case {5,6}
       optslist={'Scalar','Random','pRandom','nRandom','copyback'}; 
       set(findobj('-regexp','tag','lrate*'),'Visible','on');
    case 7
       tbobjs = findobj(handles.placeholderpnl,'-regexp','tag','^tbias*');        
       if get(handles.cltopochk,'Value') ~=0 
          optslist={'Scalar','Random','Tbias'};
          set(tbobjs,'Visible','on');
       else
          optslist={'Scalar','Random'};
          set(tbobjs,'Visible','off');          
       end
    otherwise
       optslist={'Scalar','Read file','Set by hand'};
end
set(handles.projconstropts,'String',optslist,'Value',1);
set(handles.projsenderpop,'String',slist,'Value',1);
set(handles.projrcvrpop,'String',rlist,'Value',1);
    


function projsenderpop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','Yellow');
% setappdata(handles.defconstrbtn,'mat',[]);
set(handles.projconstredit,'String','0.0');
set(handles.projconstropts,'Value',1);

function projsenderpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function projrcvrpop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','Yellow');
% setappdata(handles.defconstrbtn,'mat',[]);
set(handles.projconstredit,'String','0.0');
set(handles.projconstropts,'Value',1);


function projrcvrpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function projconstredit_Callback(hObject, eventdata, handles)


function projconstredit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function projokbtn_Callback(hObject, eventdata, handles)
constr=[];
pools = getappdata(handles.currplist,'pools');
slist = get(handles.projsenderpop,'String');
sval = get(handles.projsenderpop,'Value');
rlist = get(handles.projrcvrpop,'String');
ind = get(handles.projrcvrpop,'Value');
nettypeval = get(handles.nettypepop,'Value');
if nettypeval ==4 %bp
   if get(handles.projconstropts,'Value')~=5 && sval > ind+1
      disp('Projections allowed only in the order of creation of pools');
      return;
   end
end
if nettypeval==5 %srn
   if get(handles.projconstropts,'Value')==5 && sval < ind+1
      disp('Copyback projections allowed only in the reverse order of creation of pools');
      return;
   end   
end
projopts = get(handles.projconstropts,'Value');
constr = str2double(get(handles.projconstredit,'String'));
if isnan(constr)
   set(handles.projconstredit,'backgroundcolor','red');
   disp('Incorrect format for constraint value');
   return;
else
   set(handles.projconstredit,'backgroundcolor','white');
end
projind =1;
if isfield(pools(ind),'proj') && ~isempty(pools(ind).proj)
    projs = pools(ind).proj;
    frompnames = {projs.frompool};
    pind = strmatch(slist{sval},frompnames,'exact');
    if isempty(pind)
       projind=numel(projs) +1;
    else
       projind = pind;
    end
end
pools(ind).proj(projind).frompool = slist{sval};
if projopts ==1
    pools(ind).proj(projind).constraint = constr;
else
    if ~ismember(nettypeval,[4 5 6 7])
        mfile=getappdata(handles.projconstropts,'matfile');
        arrayfile=mfile.fname;
        args=sprintf('''%s''',arrayfile);
        if mfile.alphadef == 1
           args=sprintf('%s,''%s'',''%s'',%.2f',args,slist{sval},rlist{ind},constr);
        end
        pools(ind).proj(projind).constraint= sprintf('read_connection(%s)',args);
    end
end
if ismember(nettypeval,[4 5 6]) %bp
   ctypes= get(handles.projconstropts,'String');
   pools(ind).proj(projind).constraint_type = ctypes{projopts};
end
if ismember(nettypeval,[1 4 5 6])
   pools(ind).proj(projind).lrate = str2double(get(handles.lrateedit,'String'));
end
if nettypeval==7 &&  projopts==3
   pools(ind).proj(projind).topbias = str2double(get(handles.tbiastopbiasedit,'String'));
   pools(ind).proj(projind).ispread = str2double(get(handles.tbiasspreadedit,'String'));
end
setappdata(handles.currplist,'pools',pools);
set(findobj('BackgroundColor','Yellow'),'BackgroundColor','white');
if strcmpi(get(handles.scriptpanel,'Visible'),'on')
   refreshscript(handles);
end

function defconstrbtn_Callback(hObject, eventdata, handles)
slist = get(handles.projsenderpop,'String');
sval = get(handles.projsenderpop,'Value');
pools = getappdata(handles.poolstypepop,'pools');
sender=slist{sval};
rlist = get(handles.projrcvrpop,'String');
rval = get(handles.projrcvrpop,'Value');
rcvr = rlist{rval};
constrmatrix.sender=sender;
if sval==1
  constrmatrix.sunits=1;
else
  constrmatrix.sunits=pools(sval).nunits;
end
constrmatrix.receiver=rcvr;
constrmatrix.runits=pools(rval+1).nunits;
setbyhand(2,constrmatrix);



function sigmabtn_Callback(hObject, eventdata, handles)
sl = str2double(get(handles.poolunitsedit,'String'));
if isnan(sl)
   set(handles.poolunitsedit,'backgroundcolor','red');
   disp('Incorrect format for number of units');
   return;
else
   set(handles.poolunitsedit,'backgroundcolor','white');
end
sigmat.sender=get(handles.poolnameedit,'String');;
sigmat.sunits =sl;
sigmat.receiver='';
sigmat.runits=1;
setbyhand(1,sigmat);



function refreshscript(h)
script='';
snum=1;
netinfo =getappdata(h.nettypepop,'netinfo');
poolinfo = getappdata(h.currplist,'pools');
if isempty(netinfo)
    set(h.scriptedit,'String','');
    return;
end
netfields = fieldnames(netinfo);
for neti = 1:numel(netfields)
    if ischar(netinfo.(netfields{neti}))
        script{snum} = sprintf('net.%s = ''%s''',netfields{neti},netinfo.(netfields{neti}));
    else
        script{snum} = sprintf('net.%s = %d', netfields{neti},netinfo.(netfields{neti}));
    end
    snum = snum+1;
end
if isempty(poolinfo)
    set(h.scriptedit,'String',script,'ForegroundColor','Black');
    return;
end
poolfields = fieldnames(poolinfo);
for i = 1: numel(poolinfo)
    for p = 1:numel(poolfields)
        f = poolinfo(i).(poolfields{p});
        if isempty(f)
           continue;
        end
        script{snum} = sprintf('pool(%d).%s',i,poolfields{p});        
        if isstruct(f)
            tscript=script{snum};
            ps = fieldnames(f);
            for j=1:numel(f)
                script{snum} = sprintf('%s(%d).',tscript,j);
                ttscript = script{snum};
                for q= 1:numel(ps)
                    ff = f(j).(ps{q});
                    script{snum} = sprintf('%s%s = ',ttscript,ps{q});
                    if ischar(ff)
                       if strncmp(ff,'read_connection',15)
                          script{snum} = sprintf('%s %s',script{snum},ff);
                       else
                          script{snum} = sprintf('%s''%s''',script{snum},ff);
                       end
                    end
                    if isnumeric(ff)
                        if numel(ff) == 1
                           script{snum} = sprintf('%s%s',script{snum},num2str(ff));
                        else
                          script{snum} = sprintf('%s%s',script{snum},mat2str(ff));
                        end
                    end
                    snum=snum+1;
                end
                snum = snum +1;
            end
            snum=snum+1;
            continue;
        end
                    
        if ischar(f)
           if strncmp(f,'textread',8)
              script{snum} = sprintf('%s = %s',script{snum},f);
           else            
              script{snum} = sprintf('%s = ''%s''',script{snum},f);
           end
        end                     
        if isnumeric(f)
            if numel(f) == 1
               script{snum} = sprintf('%s = %s',script{snum},num2str(f));
            else
               script{snum} = sprintf('%s = %s',script{snum},mat2str(f));
            end
        end
        snum = snum+1;
    end
end
mt = cellfun('isempty',script);
script = script(~mt);
set(h.scriptedit,'String',script,'ForegroundColor','Black');

    


function poolunitsedit_KeyPressFcn(hObject, eventdata, handles)
if get(handles.poolstypepop,'Value')==1
   set(handles.pooladdbtn,'enable','off');
end
if (get(handles.nettypepop,'Value')==3)
%     set(handles.sigmabtn,'enable','on');
    set(handles.unamepop,'enable','on');
end



function projconstropts_Callback(hObject, eventdata, handles);
nettypeval = get(handles.nettypepop,'Value');
if ismember(nettypeval ,[4 5 6])
   return;
end
if nettypeval ==7 
   tbobjs = findobj(handles.placeholderpnl,'-regexp','tag','^tbias*'); 
   if get(hObject,'Value')==3
      set(tbobjs,'Visible','on');
   else
      set(tbobjs,'Visible','off');
   end
   return;  
end
   
pools = getappdata(handles.currplist,'pools');
sval = get(handles.projsenderpop,'Value');
rval = get(handles.projrcvrpop,'Value');
if sval==1
    su =1;
else
    su = pools(sval-1).nunits;
end
ru = pools(rval).nunits;
alphadef=0;
switch get(hObject,'Value')
    case 1
        return;
    case 2
        [filename, pathname] = uigetfile('*.*','Read array from file');
        if isequal([filename,pathname],[0,0])
           set(hObject,'Value',1);
           return;
        end
        if strcmpi(pathname(1:end-1),pwd)
           File=filename;
        else
           File = fullfile(pathname,filename);
        end
        [wrdcheck] = textread(File,'%s',1);
        if isnan(str2double(wrdcheck{1}))
           if strfind(wrdcheck{1},'alpha') ~=1
               return;
           end
           inpnum = sum(strcmpi('input',{pools.type}));
           hp = strcmpi('hidden',{pools.type});
           su = sum(pools(hp==1).nunits);
           formatstr= sprintf('%%%dc%%*[^\\n]',inpnum);
           [readm]=textread(File,formatstr,'headerlines',2);
           sz = [su inpnum];
           alphadef=1;           
        else
            readm =  dlmread(File);
            sz = [ru su];
        end
        if isequal(size(readm),sz)
            matfile.alphadef = alphadef;
            matfile.fname=File;
            setappdata(hObject,'matfile',matfile);
        else
            disp(sprintf ('Array in file %s (%s) does not match pool sizes(%s)',...
                File,mat2str(size(readm)),mat2str(sz)));
            set(hObject,'Value',1);
            return;
        end
    case 3
        slist = get(handles.projsenderpop,'String');
        constrmatrix.sender=slist{sval};
        rlist = get(handles.projrcvrpop,'String');
        constrmatrix.receiver = rlist{rval};
        constrmatrix.sunits=su;
        constrmatrix.runits=ru;
        setbyhand(2,constrmatrix);
end


function projconstropts_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function poolnameedit_ButtonDownFcn(hObject, eventdata, handles)
if  ~strcmpi(get(handles.networkcreate,'SelectionType'),'alt')
    return;
end
pos = get(hObject,'Position');
set(hObject,'Visible','off');
set(handles.currplist,'Visible','on','Position',pos);
pools = getappdata(handles.currplist,'pools');
if isempty(pools)
   return;
end
types= get(handles.poolstypepop,'String');
ind = strmatch(pools(1).type,types,'exact');
set(handles.poolstypepop,'Value',ind);
set(handles.poolunitsedit,'String',num2str(pools(1).nunits));
set(handles.pooladdbtn,'enable','on');

function currplist_Callback(hObject, eventdata, handles)
pnum=get(hObject,'Value');
pools = getappdata(handles.currplist,'pools');
types= get(handles.poolstypepop,'String');
ind = strmatch(pools(pnum).type,types,'exact');
set(handles.poolstypepop,'Value',ind);
set(handles.poolunitsedit,'String',num2str(pools(pnum).nunits));

function currplist_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
pools(1).name = 'bias';
pools(1).type = 'bias';
pools(1).nunits = 1;
setappdata(hObject,'pools',pools);
set(hObject,'UserData',pools);



function currplist_ButtonDownFcn(hObject, eventdata, handles)
if  ~strcmpi(get(handles.networkcreate,'SelectionType'),'alt')
    return;
end
set(hObject,'Visible','off');
set(handles.poolnameedit,'Visible','on','String',' ');
set(handles.poolunitsedit,'String',' ');
set(handles.poolstypepop,'Value',1);
set(handles.pooladdbtn,'enable','off');



function unamesbtn_Callback(hObject, eventdata, handles)


function unamepop_Callback(hObject, eventdata, handles)
nu = str2double(get(handles.poolunitsedit,'String'));
if isnan(nu)
   set(handles.poolunitsedit,'backgroundcolor','red');
   disp('Incorrect format for number of units');
   return;
else
   set(handles.poolunitsedit,'backgroundcolor','white');
end
switch get(hObject,'Value')
    case 1
        return;
    case 2
        umat.sender=get(handles.poolnameedit,'String');
        umat.sunits =nu;
        umat.receiver='';
        umat.runits=1;
        setbyhand(3,umat);
    case 3
        [filename, pathname] = uigetfile('*.*','Read unames from file');
        if isequal([filename,pathname],[0,0])
           set(hObject,'Value',1);
           return;
        end
        if strcmpi(pathname(1:end-1),pwd)
           File =filename;
        else
           File = fullfile(pathname,filename);
        end
        readm =  textread(File,'%s');
        if isequal(size(readm),[nu 1])
           setappdata(hObject,'unamefile',File);
        else
           disp(sprintf ('List in file %s does not match pool size',File));
           set(hObject,'Value',1);
           return;
        end
end


function unamepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function poolnameedit_KeyPressFcn(hObject, eventdata, handles)
if get(handles.poolstypepop,'Value')==1
   set(handles.pooladdbtn,'enable','off');
end


function topoxedit_Callback(hObject, eventdata, handles)


function topoxedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function topoyedit_Callback(hObject, eventdata, handles)


function topoyedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function tbiastopbiasedit_Callback(hObject, eventdata, handles)


function tbiastopbiasedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tbiasspreadedit_Callback(hObject, eventdata, handles)


function tbiasspreadedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tbiassigmaedit_Callback(hObject, eventdata, handles)


function tbiassigmaedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function topolrangeedit_Callback(hObject, eventdata, handles)


function topolrangeedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function cltopochk_Callback(hObject, eventdata, handles)
status = 'off';
netinfo = getappdata(handles.nettypepop,'netinfo');
netinfo.standardcl = 'on';
pcopts = cellstr(get(handles.projconstropts,'String'));
if get(hObject,'Value') ~= 0
   status = 'on';
   netinfo.standardcl = 'off';
   pcopts{3} = 'Tbias';
else
   pcopts = pcopts(1:2);
end
set(handles.projconstropts,'String',pcopts,'Value',1);
h_topo=[];
if strcmpi(get(handles.poolstypepop,'Visible'),'on')
   h_topo = findobj(handles.placeholderpnl,'-regexp','tag','^topo*');
else
   if strcmpi(get(handles.projsenderpop,'Visible'),'on')
      h_topo = findobj(handles.placeholderpnl,'-regexp','tag','^tbias*');
   end
end
if ~isempty(h_topo)
   set(h_topo,'Visible',status);
end
setappdata(handles.nettypepop,'netinfo',netinfo);
if  strcmpi(get(handles.scriptedit,'Visible'),'on')
    refreshscript(handles);
end   



% 
% function bptypepop_Callback(hObject, eventdata, handles)
% netinfo = getappdata(handles.nettypepop,'netinfo');
% val = get(hObject,'Value');
% switch (val)
%     case 1
%         netinfo.bptype = 'sff';
%     case 2
%         netinfo.bptype = 'srn';
%     case 3
%         netinfo.bptype = 'rtt';
% end
% setappdata(handles.nettypepop,'netinfo',netinfo);
% if  strcmpi(get(handles.scriptedit,'Visible'),'on')
%     refreshscript(handles);
% end 

function bptypepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function rbpttnstepsedit_Callback(hObject, eventdata, handles)
netinfo = getappdata(handles.nettypepop,'netinfo');
netinfo.nintervals = str2double(get(hObject,'String'));
setappdata(handles.nettypepop,'netinfo',netinfo);
if  strcmpi(get(handles.scriptedit,'Visible'),'on')
    refreshscript(handles);
end   

function rbpttnstepsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function rbptttpsedit_Callback(hObject, eventdata, handles)
netinfo = getappdata(handles.nettypepop,'netinfo');
netinfo.ticksperinterval = str2double(get(hObject,'String'));
setappdata(handles.nettypepop,'netinfo',netinfo);
if  strcmpi(get(handles.scriptedit,'Visible'),'on')
    refreshscript(handles);
end 

function rbptttpsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function lrateedit_Callback(hObject, eventdata, handles)


function lrateedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




function toposwanchk_Callback(hObject, eventdata, handles)


