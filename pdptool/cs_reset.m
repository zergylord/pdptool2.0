function cs_reset
global net PDPAppdata;
if exist('RandStream','file') == 2 % for version 7.8 and above    
   stream = RandStream.getGlobalStream;
   reset(stream);
else
   rand('seed',net.seed)
   randn('seed',net.seed)
end
refresharrays();
net.cycleno = 0;
net.patno = 0;
net.cpname = '';
net.cuname = '';
net.unitno = 0;
net.updateno = 0;
PDPAppdata.tstinterrupt_flag = 0;
if ~isempty(findobj('tag','netdisplay'))
    update_display(0);
end
if ~isempty(net.outputfile)
   resetlog;
end
if PDPAppdata.lognetwork
   updatelog(sprintf('%s;',mfilename));
end