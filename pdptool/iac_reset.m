function iac_reset
global net PDPAppdata
refresh_iac();
PDPAppdata.tstinterrupt_flag = 0;
if ~isempty(findobj('tag','netdisplay'))
    update_display(0);
end
if ~isempty(net.outputfile)
   resetlog;
end
if PDPAppdata.lognetwork
   updatelog(sprintf('%s;',mfilename));
end