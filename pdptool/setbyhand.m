function setbyhand(indicator,cmatrix)
rnum=cmatrix.runits;
snum=cmatrix.sunits;
editsz= 30;
Margin=10;
sname = ' ';
rname= ' ';
fillstr='0.0';
%indicator 1 is for sigma (cs) and 2 for projection constraints
switch indicator
    case 1
       titlestr = 'Set sigma vector';
    case 2
       titlestr='Set constraint matrix';
       sname= sprintf('pool :%s',cmatrix.sender);
       rname= sprintf('pool :%s',cmatrix.receiver);
    case 3
       titlestr='Set unit names';
       sname= sprintf('pool :%s',cmatrix.sender);  
       editsz=50;
       fillstr=' ';
end
sname_width= numel(sname)*6;
rname_width = numel(rname)*6;
filltxtwidth=50;
filleditwidth=30;
donebtnwidth=40;
basewidth = sname_width + filltxtwidth + filleditwidth + donebtnwidth + 4*Margin;
fig_width = (snum+2) *editsz + sname_width + 3*Margin;
if fig_width < basewidth
   fig_width = 2* basewidth - fig_width + 4 *Margin;
end
fig_height = (rnum+2) * editsz + 4*Margin;
% % Get screensize in pixels
Units=get(0,'units'); 
set(0,'units','pixels'); 
ss = get(0,'ScreenSize');
set(0,'units',Units);
swidth = ss(3); 
sheight = ss(4);
if fig_width > swidth
    fig_width = swidth;
end
if fig_height > sheight
    fig_height = sheight;
end
left = (swidth-fig_width)/2; 
bottom = (sheight-fig_height)/2;
figrect = [left bottom fig_width fig_height];
figure('Name',titlestr,'NumberTitle','off','Units','pixels',...
       'Position',figrect,'Menubar','none','resize','off','windowstyle','modal',...
       'tag','setmatfig','Color',get(0,'defaultUicontrolBackgroundColor'));
pos=[Margin fig_height/2-editsz rname_width 20];
uicontrol('Style','text','String',rname,...
         'Units','pixels','Position',pos);
y=(editsz+2)*rnum;
for i=1:rnum
    x=pos(1)+pos(3)+Margin;
%     y=y+editsz+2;    
    for j=1:snum
        edpos=[x y editsz editsz];
        tagname=sprintf('cmat%d%d',i,j);
        uicontrol('Style','edit','String',fillstr,'Position',edpos,...
                  'tag',tagname,'Userdata',i);
        x=x+editsz+2;
    end
    y = y-(editsz+2);
end
pos=[pos(1)+pos(3)+Margin (editsz+2)*rnum+Margin+editsz sname_width 20];
uicontrol('Style','text','String',sname,...
         'Units','pixels','Position',pos,'HorizontalAlignment','left');    
pos(1)=pos(1)+ pos(3) + Margin;
pos(3)=filltxtwidth;
uicontrol('Style','text','String','Fill with : ','Units','pixels',...
          'Position', pos);
vis = 'on';      
if indicator ==3      
    btnpos(1)=pos(1);
    btnpos(2)= pos(2) + 5;
    btnpos(3)=filltxtwidth+filleditwidth +Margin;
    btnpos(4)= pos(4)+5;
    uicontrol('Style','pushbutton','String','Save to file','Units','pixels',...
             'Position',btnpos);
    vis='off';         
end
pos(1)=pos(1)+pos(3)+Margin;
pos(3)=filleditwidth;
pos(2)=pos(2)+5;
uicontrol('Style','edit','String','0.0','Units','pixels','Position',pos,...
          'BackgroundColor','white','HorizontalAlignment','left',...
          'Visible',vis,'callback',@fillmatrix);
pos(1)=pos(1)+pos(3)+Margin;
pos(3)=donebtnwidth;
pos(4) = pos(4) +5;
uicontrol('style','pushbutton','String','Done','Units','pixels',...
          'Position',pos,'Callback',{@derive_matrix,indicator,snum,rnum});


function fillmatrix(hObject,event)
mathandles = findobj('-regexp','tag','cmat+');
set(mathandles,'String',get(hObject,'String'));

function derive_matrix(hObject, event,indic,num_s,num_r)
handles=guihandles;
obj=repmat(0,num_r,num_s);  %repmat is faster that zeros
for i=1:num_r
    mathandles=findobj('UserData',i); %get a row
    z= cellstr(get(mathandles(end:-1:1),'String'));
    x=char(z{1:end});
    obj(i,:) = str2num(x);
end
[filename, pathname] = uiputfile('*.*','Save array to file');
if isequal([filename,pathname],[0,0])
    return;
end
File = fullfile(pathname,filename);
dlmwrite(File,obj,' ');
if indic == 1
    sigbtn=findobj('tag','sigmabtn');
    sigmavec=obj;
    setappdata(sigbtn,'sigmavector',sigmavec);
else
    defbtn = findobj('tag','projconstropts');
%     constredit = findobj('tag','projconstredit');
%     set(constredit,'String',sprintf('[%d X %d]',size(obj,1),size(obj,2)));
    setappdata(defbtn,'matfile',File);
end
delete(handles.setmatfig);
