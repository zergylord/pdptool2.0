function [v b] = get_display_attributes(objectval)
% map = get(gcf,'Colormap');
% [cmin cmax] = caxis;
% ind=fix((objectval - cmin)/(cmax-cmin) * length(map))+1;
% b= map(ind,:);
grey = [0.8 0.8 0.8];
red = [1 0 0];
blue = [0 0 1];
b= grey;
if objectval< -0.01
   b=blue;
end
if objectval > 0.01
   b=red;
end
v= sprintf('%.2f',objectval);