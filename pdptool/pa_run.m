function pa_run 
global net outputlog binarylog patn runparams trpatn tstpatn PDPAppdata;
[options,data,cpno,N,cepoch, lepoch,patfield,orig_tss,appflag] = initialize_params;
logoutflag = 0;
freqrange = {};
if ~isempty(outputlog) || ~isempty(net.outputfile)
   [logoutflag freqrange] = getloggingdetails(runparams.process);
end
PDPAppdata.stopprocess = [];
updatecount = 0;
for iter = cepoch:lepoch 
    pause(0);
    net.epochno = iter;
    net.tss = orig_tss;
    if iter > cepoch
       patn = getpatternrange(data,options); % gets fresh pattern range for next epoch
       if strcmpi(runparams.process,'test')    
          tstpatn = patn;
       else
          trpatn = patn;
       end
    end
    if iter == lepoch && ~isempty(runparams.range)
       N = runparams.range(2,1);
    end
    for p = cpno: N
        abort = PDPAppdata.stopprocess;
        if ~isempty(abort)
            PDPAppdata.stopprocess = [];
            if strncmp(runparams.granularity,'epoch',...
                       length(runparams.granularity)) || p == 1       
              net.epochno = net.epochno - 1;
            end
            if PDPAppdata.gui
               update_display(patn(net.(patfield))); 
            end
            PDPAppdata.(appflag) = 1;
            return;
        end
        net.(patfield) = p;
        pno = patn(p);
        setinput(data,pno,options);
        compute_output(data,pno,options);
        compute_error;
        sumstats;
        if logoutflag && ~isempty(strmatch('cycle',lower(freqrange)))
           writeoutput(runparams.process,'cycle');
        end        
        if PDPAppdata.gui && strncmp(runparams.granularity,'cycle',...
           length(runparams.granularity))
           updatecount = update_display_step(updatecount,pno);
           if updatecount < 0 %stepping through cycles, returns only after sumstats and change_weights
              uiwait(findobj('tag','netdisplay'));
              updatecount = 0;
           end            
        end        
        if (options.lflag)
            change_weights(options);
        end
        if logoutflag && ~isempty(strmatch('pattern',lower(freqrange)))
            writeoutput(runparams.process,'pattern');
        end        
        if PDPAppdata.gui && (strncmp(runparams.granularity,'pattern',...
           length(runparams.granularity)) || strncmp(runparams.granularity,...
           'cycle',length(runparams.granularity)))
           updatecount = update_display_step(updatecount,pno);
           if updatecount < 0 && p ~= N % 'step' mode
              PDPAppdata.(appflag) = 1;
              return;
           end
        end        
    end
    cpno = 1;
    if logoutflag && ~isempty(strmatch('epoch',lower(freqrange)))
       writeoutput(runparams.process,'epoch');
    end    
    if PDPAppdata.gui && strncmp(runparams.granularity,'epoch',...
       length(runparams.granularity))
       updatecount = update_display_step(updatecount,patn(net.(patfield)));
    end
    if net.tss < options.ecrit
       disp('Error criterion reached');
       if PDPAppdata.gui
          update_display(patn(net.(patfield)));
       end
       net.(patfield) = 0;
       break;
    end 
    if PDPAppdata.gui && updatecount < 0 %when p==N for 'step' mode or 'stepping' through each epoch 
       break; % to check if binary output files need to be written
    end 
    orig_tss = 0.0;    
end
if PDPAppdata.gui
   update_display(-1);
end
PDPAppdata.(appflag) = 0;
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1)) 
   writematfiles;
end

function [opts,dat,spat,npat,sepoch,lepoch,pfield,otss,flag]= initialize_params
global net runparams patn trpatn tstpatn PDPAppdata rangelog;
otss = 0.0;
flag ='tstinterrupt_flag';
if PDPAppdata.gui && (~isequal(size(net.nancolor),[1 3]) || ...
   any(net.nancolor > 1) || any(net.nancolor < 0))
   fprintf(1,'net.nancolor has an invalid value,switching to default RGB triplet [0.97 0.97 0.97]\n');
   net.nancolor = [0.97 0.97 0.97];
end
if strcmpi(runparams.process,'test')
   opts = net.testopts;
   dat = PDPAppdata.testData;
   pfield = 'testpatno';
   if PDPAppdata.tstinterrupt_flag ~= 1
      tstpatn = getpatternrange(dat,opts);
   end
   patn = tstpatn;
   pnum = get(findobj('tag','tstpatlist'),'Value');      
   if strncmpi(runparams.mode,'run',length(runparams.mode))
      if runparams.alltest
         net.testpatno = 1;
         npat = numel(dat);
      else
         net.testpatno = find(patn==pnum,1);
         npat = net.testpatno;         
      end
   else
      if runparams.alltest
         net.testpatno = net.testpatno + 1;
         net.pss = 0;
         npat = numel(dat);
         if net.testpatno > npat
            net.testpatno = 1;
         end
         if net.testpatno == 1
            net.tss = 0;
         end
         otss = net.tss;
      else
         net.testpatno = find(patn==pnum,1);
         net.pss = 0;
         npat = net.testpatno;
      end
   end
   if isempty(runparams.range)
      sepoch = net.epochno;
      lepoch = net.epochno;
      spat = net.testpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);        
   end
else
   opts = net.trainopts;
   pfield = 'trainpatno';   
   dat = PDPAppdata.trainData;
   npat = numel(dat);
   if PDPAppdata.trinterrupt_flag 
      otss = net.tss; % in interrupt mode, start with current tss
   else       
      trpatn = getpatternrange(dat,opts);
   end
   patn = trpatn;
   flag = 'trinterrupt_flag';
   net.trainpatno = net.trainpatno + 1;
   if net.trainpatno > numel(dat)
      net.trainpatno = 1;
   end
   if net.trainpatno == 1
      net.epochno = net.epochno + 1;
      otss = 0;
   end
   if isempty(runparams.range)
      sepoch = net.epochno;
      if isempty(runparams.nepochs)
         lepoch = opts.nepochs + floor((sepoch-1)/opts.nepochs)*opts.nepochs;
%          lepoch = sepoch + opts.nepochs - 1;
      else
          net.trainpatno = 1;
          lepoch = sepoch + runparams.nepochs - 1;
      end
      spat = net.trainpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);   
   end
end
rangelog = [spat sepoch];

function retval = logistic(netinp,temp)
retval = netinp./temp;
retval(retval > 15.9357739741644) = 15.9357739741644;  % returns 0.99999988;
retval(retval < -15.9357739741644) = -15.9357739741644; % returns 0.00000012
retval = 1.0 ./ (1.0 + exp(-1.0 * retval));

function setinput(pattern,patnum,opts)
global net;
net.cpname=pattern(patnum).pname;
%distort input
for i=1:numel(net.inindx)
    x=net.inindx(i);
    nin = net.pool(x).nunits;    
    net.pool(x).input = pattern(patnum).ipattern + ((1.0 -2.0*rand(1,nin)) * opts.noise);
    %set output of input pool
    net.pool(x).output = net.pool(x).input;
end


function compute_output(pattern,patnum,opts)
global net;
nout = net.noutputs;
pools = {net.pool.name};
for i=1:numel(net.outindx)
    y=net.outindx(i);  %index into net.pool array 
    p=net.pool(y);
    %distort target
    p.target = pattern(patnum).tpattern + ((1.0 - 2.0*rand(1,nout)) * ...
               opts.noise);
    [m,n] = size(p.netinput);
    p.netinput(:) = 0; %repmat(0,size(p.netinput)); %zeros(m,n);
    for j=1:numel(p.proj)
        frpool=p.proj(j).frompool;
        k = strmatch(frpool,pools);
        senderout = net.pool(k).output;
        p.netinput = p.netinput + (senderout * p.proj(j).weight');
    end
    switch opts.actfunction
            case 'st'  %stochastic
                 logout = logistic(p.netinput, opts.temp);
                 r = rand(1,n);
                 p.output(r < logout) = 1.0;
                 p.output(r >= logout) = 0.0;
            case 'li'   %linear
                 p.output = p.netinput;
            case 'cs'   %continuous sigmoid
                 p.output = logistic(p.netinput,opts.temp);
            case 'lt'   %linear threshold
                 p.output(p.netinput > 0) = 1.0;
                 p.output(p.netinput <= 0) = 0.0;
            otherwise
                error('Invalid activation function - %s',opts.actfunction);
    end
    net.pool(y) = p;
end

function compute_error
global net;
for i=1:numel(net.outindx)
    y=net.outindx(i);  %index into net.pool array 
    net.pool(y).error=net.pool(y).target - net.pool(y).output;
end


function sumstats
global net;
nout = net.noutputs;
px=0;
py=0;
pz=0;
net.pss = 0;
net.ndp =0;
net.vcor = 0;
net.nvl =0;
for i=1:numel(net.outindx)
    y=net.outindx(i);  %index into net.pool array 
    p=net.pool(y);
    %get pss
    net.pss = net.pss + sum((p.target - p.output) .^ 2);
    %get vector correlation
    px= px + sum(p.target .^2);
    py= py + sum(p.output .^ 2);
    pz= pz + sum(p.target .* p.output);
    net.ndp = net.ndp + dot(p.target,p.output);
end
net.ndp = net.ndp / nout;
net.tss  = net.tss + net.pss;
net.nvl = sqrt(py/nout);
if (px==0) || (py== 0)
    net.vcor = 0;
 else
    net.vcor = pz/sqrt(px*py);
end

function change_weights(opts)
global net;
pools = {net.pool.name};
for i = 1:numel(net.outindx)
    y = net.outindx(i);  %index into net.pool array 
    p = net.pool(y);
    if strncmpi(opts.lrule,'delta',length(opts.lrule))
       scalewith = p.error;
    else
       p.output = p.target;        
       scalewith = p.output; %hebb 
    end
    for j= 1 : numel(p.proj)
        frpool = p.proj(j).frompool;
        k = strmatch(frpool,pools);
        if ~isempty(k)
           lr = p.proj(j).lrate;
           if isnan(lr)
              lr = opts.lrate;
           end
           p.proj(j).weight = p.proj(j).weight + (scalewith' * ...
                              net.pool(k).output * lr);
        end
    end
    net.pool(y) = p;
end

function currcount = update_display_step(currcount,pno)
global runparams;
currcount = currcount+1;
if currcount == runparams.count
    currcount = 0;
    update_display(pno);
    if strncmp(runparams.mode,'step',length(runparams.mode))
       currcount = -1; 
    end
end


function range = getpatternrange(patterns,procoptions)
N = numel(patterns);
switch procoptions.trainmode(1)
    case 's'
        range = 1:N;
    case 'p'
        range = randperm(N);
    case 'r'
        range = ceil(N * rand(1,N));
    otherwise
        range =1: N;
end