function srn_run_fast
global net outputlog inpools contextpool outpools hidpools patn binarylog;
global runparams trpatn tstpatn PDPAppdata;
[options,data,cseqno,N,cepoch lepoch,patfield,seqfield,stpat,orig_tss,orig_tce,...
appflag] = initialize_params;
logoutflag = 0;
freqrange = {};
if ~isempty(outputlog) || ~isempty(net.outputfile)
    [logoutflag freqrange] = getloggingdetails(runparams.process);
end
PDPAppdata.stopprocess = [];
inpools  = find(strcmpi( {net.pool.type},'input'));
hidpools = find(strcmpi({net.pool.type},'hidden'));
outpools = find(strcmpi({net.pool.type},'output'));
contxt = 1;
for i = 1:numel(hidpools)
    hind = hidpools(i);
    for j = 1:numel(net.pool(hind).proj)
        if strcmpi(net.pool(hind).proj(j).constraint_type,'copyback')
           from = net.pool(hind).proj(j).frompool;
           fromind = find(strcmpi({net.pool.name},from));
           contextpool(contxt).poolnum = hind;
           contextpool(contxt).fpoolnum = fromind;
           contxt = contxt + 1;
        end
    end
end
if runparams.alltest == 0
    clear_wed();
end
updatecount = 0;
lgraincount = 1;
runmode = 0;
if strncmpi(runparams.process,'train',length(runparams.process))
   runmode =1;
end
for iter = cepoch:lepoch
    abort = PDPAppdata.stopprocess; 
    if ~isempty(abort)
       PDPAppdata.stopprocess = [];         
       PDPAppdata.(appflag) = 1;           
       return;       
    end    
    net.epochno = iter;
    net.tss = orig_tss;
    net.tce = orig_tce;
    if iter > cepoch
       patn = getpatternrange(data,options);   % gets fresh pattern range for next epoch
       if runmode  
          trpatn = patn;           
       else
          tstpatn = patn;
       end
    end
    for p = cseqno: N
        pno = patn(p);
        endpat = numel(data(pno).sequence);
        net.(seqfield) = p;
        net.sss = 0.0;
        net.sce = 0.0;        
        for sp = 1:endpat
            net.(patfield) = sp;
            setpattern(pno,data,options,patfield);
            net.numpat = sp;
            srn_compute_output_error(net,net.pool,[hidpools outpools],outpools,[outpools hidpools(end:-1:1)],runmode,1);              
            sumstats();
            if (options.lflag)
               compute_wed();
                if isequal(lower(options.lgrain(1)),'p')
                   if isequal(options.lgrainsize,lgraincount)
                      if (options.follow)
                          srn_change_weights_follow_mx(net.pool,options,[hidpools outpools]);
                      else
                          srn_change_weights_mx(net.pool,options,[hidpools outpools]);
                      end
                      lgraincount = 1;
                   else
                      lgraincount = lgraincount+1;
                   end
                end
            end
        end
    end
    cseqno = 1;
    trpatn = [];
    tstpatn = [];    
    if (options.lflag) && isequal(lower(options.lgrain(1)),'e')
        if isequal(options.lgrainsize,lgraincount)
           if (options.follow)
                srn_change_weights_follow_mx(net.pool,options,[hidpools outpools]);
           else
                srn_change_weights_mx(net.pool,options,[hidpools outpools]);
           end
           lgraincount = 1;
        else
           lgraincount = lgraincount + 1;
        end
    end
    if logoutflag && ~isempty(strmatch('epoch',lower(freqrange)))
       writeoutput(runparams.process,'epoch');
    end    
    if PDPAppdata.gui && strncmpi(runparams.granularity,'epoch',...
       length(runparams.granularity)) 
       updatecount = update_display_step(updatecount,patn(net.(seqfield)));     
    end
    if (strcmpi(options.errmeas,'sse') && (net.tss < options.ecrit)) || ...
       (strcmpi(options.errmeas,'cee') && (net.tce < options.ecrit))
       disp('Error criterion reached');
       if PDPAppdata.gui
          update_display(net.(seqfield));
       end
       net.(seqfield) = 0;
       break;
    end
    orig_tss = 0.0;
    orig_tce = 0.0;      
end 
if PDPAppdata.gui
   update_display(net.(seqfield));   
end
srn_compute_output_error(-1);
PDPAppdata.(appflag) = 0;
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1)) 
    writematfiles;
end

function [opts,dat,sseq,nseq,sepoch,lepoch,pfield,sfield,spat,otss,otce,...
flag]= initialize_params
global net runparams patn trpatn tstpatn PDPAppdata rangelog;
otss = 0.0;
otce = 0.0;
flag = 'tstinterrupt_flag';
spat = 1;
if PDPAppdata.gui && (~isequal(size(net.nancolor),[1 3]) || ...
   any(net.nancolor > 1) || any(net.nancolor < 0))
   fprintf(1,'net.nancolor has an invalid value,switching to default RGB triplet [0.97 0.97 0.97]\n');
   net.nancolor = [0.97 0.97 0.97];
end
if strcmpi(runparams.process,'test')
   opts = net.testopts;
   dat = PDPAppdata.testData;
   pfield = 'testpatno';
   sfield = 'testseqno';
   if PDPAppdata.tstinterrupt_flag ~= 1
      tstpatn = getpatternrange(dat,opts);
   end
   patn = tstpatn;
   pnum = get(findobj('tag','tstpatlist'),'Value'); 
   if strncmpi(runparams.mode,'run',length(runparams.mode))
      net.testpatno = 1;
      if runparams.alltest
         net.testseqno = 1;
         nseq = numel(dat);
      else
         net.testseqno = find(patn==pnum,1); %in case patn is in permuted or random order  
         nseq = net.testseqno;
      end
   else
      net.testpatno = net.testpatno + 1;
      if net.testseqno > 0 && net.testpatno > ...
                           numel(dat(net.testseqno).sequence)
         net.testpatno = 1;
      end
      if runparams.alltest      
         if net.testpatno == 1
            net.testseqno = net.testseqno + 1;
            net.sss = 0.0;
            net.pss = 0.0;
            net.pce = 0.0;
            net.sce = 0.0;
         end
         nseq = numel(dat);                
         if net.testseqno > numel(dat)
            net.testseqno = 1;
            net.tss = 0.0;
            net.tce = 0.0;
         end
      else
         net.testseqno = find(patn==pnum,1);
         if net.testpatno == 1
            net.sss = 0.0;
            net.tss = 0.0;
            net.sce = 0.0;
            net.tce = 0.0;
         end
         nseq = net.testseqno; 
      end
      otss = net.tss;
      otce = net.tce;
   end
   if isempty(runparams.range)
      sepoch = net.epochno;
      lepoch = net.epochno;
      sseq = net.testseqno;
      spat = net.testpatno;
   else
      sseq = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);
      if size(runparams.range,2)==3
         spat = runparams.range(1,3);
      end
   end
else  
   opts = net.trainopts;
   pfield = 'trainpatno';
   sfield = 'trainseqno';   
   dat = PDPAppdata.trainData; 
   nseq = numel(dat);          
   if PDPAppdata.trinterrupt_flag 
      otss = net.tss; % in interrupt mode, start with current tss
      otce = net.tce;       
   else
      trpatn = getpatternrange(dat,opts);
   end
   patn = trpatn;
   flag = 'trinterrupt_flag';
   net.trainpatno = net.trainpatno + 1;
   if net.trainseqno > 0 && net.trainpatno > ...
                           numel(dat(net.trainseqno).sequence)
      net.trainpatno = 1;
   end
   if net.trainpatno == 1
      net.trainseqno = net.trainseqno + 1;
   end
   if net.trainseqno > numel(dat)
      net.trainseqno = 1;
   end
   if net.trainseqno == 1 && net.trainpatno == 1
      net.epochno = net.epochno + 1;
      otss = 0.0;
      otce = 0.0;
   end
   if isempty(runparams.range)
      sepoch = net.epochno;
      if isempty(runparams.nepochs)
         lepoch = sepoch + opts.nepochs - 1;
      else
          net.trainpatno = 1;
          net.trainseqno = 1;
          lepoch = sepoch + runparams.nepochs - 1;
      end
      sseq = net.trainseqno;
      spat = net.trainpatno;
   else
      sseq = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      if size(runparams.range,2) > 2
         spat =  runparams.range(1,3);
      end
      lepoch = runparams.range(2,2);
   end
end
rangelog = [sseq sepoch spat];


function setpattern(pnum,patterns,procoptions,pfield)
global net inpools contextpool outpools;
patcount = net.(pfield);
pname = patterns(pnum).pname;
% resetchar ='@';
k=1;
for i=1:numel(inpools)
    inindx = inpools(i);
    nu = k+ net.pool(inindx).nunits-1;
    net.pool(inindx).activation(1:end) = 0+patterns(pnum).sequence(patcount).ipat(k:nu);
    k = nu+1;
end
net.cpname = char(patterns(pnum).ipattern)';
%set target
k=1;
for i=1:numel(outpools)
    outindx = outpools(i);
    nu = k+ net.pool(outindx).nunits-1;
    t = patterns(pnum).sequence(patcount).tpat(k:nu);
    t(t==1) = procoptions.tmax;
    t(t==0) = 1 - procoptions.tmax;
    net.pool(outindx).target(1:end)=t;
    k = nu + 1;
end
if ~isempty(contextpool)
    for c=1:numel(contextpool)
        pind = contextpool(c).poolnum;
        frind = contextpool(c).fpoolnum;
        if patcount == 1 %numel(patterns(pnum).sequence)+1 %strcmp(pname(end),resetchar)
           net.pool(pind).activation(1:end) = procoptions.clearval; %0+ repmat(procoptions.clearval,1,num);
        else
           net.pool(pind).activation(1:end) = 0 + procoptions.mu * ...
                                              net.pool(pind).activation + ...
                                              net.pool(frind).activation;
           patcount = patcount + 1;                                             
        end
    end
end

function clear_wed()
global net;
for i=1:numel(net.pool)
    for j=1:numel(net.pool(i).proj)
        net.pool(i).proj(j).wed(:) = 0;%repmat(0,size(net.pool(i).proj(j).wed));
    end
end

function compute_output()
global net hidpools outpools;
non_input =[hidpools outpools];
for i=1:numel(non_input)
    cind=0;
    ind = non_input(i);
    ninput = repmat(0,1,net.pool(ind).nunits);
    for j=1:numel(net.pool(ind).proj)
        if strcmpi(net.pool(ind).proj(j).constraint_type,'copyback')
            cind=1;
            continue;
        end
        from = net.pool(ind).proj(j).frompool;
        sender= net.pool(strcmpi({net.pool.name},from));  % two pools should not have same name
        z= sender.activation * net.pool(ind).proj(j).weight';
        ninput = ninput + z(1:end);
    end
    net.pool(ind).netinput = ninput;
    if cind ==0
       net.pool(ind).activation = logistic(ninput);
    end
end


function compute_error(procoptions)
global net hidpools outpools;
for i=1:numel(net.pool)
    net.pool(i).error = repmat(0,1,net.pool(i).nunits);
end
for i=1:numel(outpools)
    ind = outpools(i);
%     net.pool(ind).error = repmat(0,1,net.pool(ind).nunits);
    t= net.pool(ind).target;
    net.pool(ind).error (t >=0 ) = t(t>=0) - net.pool(ind).activation(t>=0);
end
backpools =[outpools hidpools(end:-1:1)]; %order is important
for i=1:numel(backpools)
    ind = backpools(i);
    if i <= numel(outpools) && strcmpi(procoptions.errmeas,'cee') % when using cee, the scaling of the error is skipped for the output pools
       net.pool(ind).delta = net.pool(ind).error;
    else    
       net.pool(ind).delta = net.pool(ind).error .* ...
                             net.pool(ind).activation .* ...
                            (1 - net.pool(ind).activation);
    end
    for j=1:numel(net.pool(ind).proj)
        from = net.pool(ind).proj(j).frompool;
        s = strcmpi({net.pool.name},from);
        if strcmpi(net.pool(s).type,'input') 
           continue;
        end
        net.pool(s).error = net.pool(s).error + (net.pool(ind).delta * ...
                       net.pool(ind).proj(j).weight);
    end
end

function sumstats()
global net outpools;
net.pss = 0.0;
net.pce = 0.0;
for i=1:numel(outpools)
    ind =outpools(i);
    t = net.pool(ind).error(net.pool(ind).target>=0);
    net.pss = net.pss + sum(t .* t);
    % for computing cross entropy error,use local variable 'act' to limit
    % values between 0.001 and 0.999, then logically index into 'act' for
    % getting logs of activation values for units that have target 1 or
    % 0.Sum of the logs evaluate to cross entropy error
    act = 0 + net.pool(ind).activation;
    act(act < 0.001) = 0.001;
    act(act > 0.999) = 0.999;
    ce = sum(log(act(net.pool(ind).target == 1))) + ...
         sum(log(1 - act(net.pool(ind).target == 0)));
    net.pce = net.pce - ce; % to make pce positive value;    
end
net.sss = net.sss + net.pss;
net.tss = net.tss+ net.sss;
net.sce = net.sce + net.pce;
net.tce = net.tce + net.pce;
 
function compute_wed()
global net outpools hidpools;
non_input =[hidpools outpools];
for i=1:numel(non_input)
    ind = non_input(i);
    for j=1:numel(net.pool(ind).proj)
%         from = net.pool(ind).proj(j).frompool;
        s = net.pool(ind).proj(j).fromindex; %strcmpi({net.pool.name},from);
        x= (net.pool(ind).delta' * net.pool(s).activation);
        net.pool(ind).proj(j).wed(1:end) = net.pool(ind).proj(j).wed + x;
%         (net.pool(ind).delta' * net.pool(s).activation);
    end
end

function change_weights(opts)
global net hidpools outpools;
non_input =[hidpools outpools];
for i=1:numel(non_input)
    ind = non_input(i);
    for j=1:numel(net.pool(ind).proj)
        if strcmpi(net.pool(ind).proj(j).constraint_type,'copyback')
           continue;
        end
        lr  = net.pool(ind).proj(j).lrate;
        if isnan(lr)
           lr = opts.lrate;
        end
        if (opts.wdecay)
           net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).wed  - ... 
                                            opts.wdecay * net.pool(ind).proj(j).weight + ...
                                            opts.momentum * net.pool(ind).proj(j).dweight;
        else
           net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).wed  + ... 
                                           opts.momentum * net.pool(ind).proj(j).dweight;
        end 
        net.pool(ind).proj(j).weight = net.pool(ind).proj(j).weight + ...
                                       net.pool(ind).proj(j).dweight;
        net.pool(ind).proj(j).wed = repmat(0,size(net.pool(ind).proj(j).wed));      
    end
end

function change_weights_follow(opts)
global net hidpools outpools;
non_input =[hidpools outpools];
p_css = net.css;
net.css = 0.0;
dp =0;
for i=1:numel(non_input)
    ind = non_input(i);
    for j=1:numel(net.pool(ind).proj)
        if strcmpi(net.pool(ind).proj(j).constraint_type,'copyback')
           continue;
        end
        lr  = net.pool(ind).proj(j).lrate;
        if isnan(lr)
           lr = opts.lrate;
        end
        if (opts.wdecay)
           net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).wed  - ... 
                                            opts.wdecay * net.pool(ind).proj(j).weight + ...
                                            opts.momentum * net.pool(ind).proj(j).dweight;
        else
           net.pool(ind).proj(j).dweight = lr * net.pool(ind).proj(j).wed  + ... 
                                           opts.momentum * net.pool(ind).proj(j).dweight;
        end 
        net.pool(ind).proj(j).weight = net.pool(ind).proj(j).weight + ...
                                       net.pool(ind).proj(j).dweight;
        sqwed = net.pool(ind).proj(j).wed .^ 2;
        net.css = net.css + sum(sqwed(1:end));
        dpprod = net.pool(ind).proj(j).wed .*  net.pool(ind).proj(j).pwed;       
        dp = dp + sum(dpprod(1:end));
        net.pool(ind).proj(j).pwed = net.pool(ind).proj(j).wed;
        net.pool(ind).proj(j).wed = repmat(0,size(net.pool(ind).proj(j).wed));         
    end
end
den = p_css * net.css;
if den > 0.0
   net.gcor = dp/(sqrt(den));
else
   net.gcor =0.0;
end
constrain_weights();


function constrain_weights()
global net;
for i=1:numel(net.pool)
    for j=1:numel(net.pool(i).proj)
        w = net.pool(i).proj(j).weight; %just for readability
        switch lower(net.pool(i).proj(j).constraint_type)
            case 'prandom'
                 net.pool(i).proj(j).weight(w < 0) = 0.0;
            case 'nrandom'
                 net.pool(i).proj(j).weight(w > 0) = 0.0;
        end
    end
end

function currcount = update_display_step(currcount,pno)
global runparams;
currcount = currcount+1;
if currcount == runparams.count
    currcount = 0;
    update_display(pno);
    if strncmpi(runparams.mode,'step',length(runparams.mode))
       currcount  = -1;
    end
end

function range = getpatternrange(patterns,procoptions)
N = numel(patterns);
switch procoptions.trainmode(1)
    case 's'  %strain
        range = 1:N;
    case 'p'  %ptrain
        range = randperm(N);
    case 'r' %rtrain
        range = ceil(N * rand(1,N));
    otherwise
        range =1: N;
end