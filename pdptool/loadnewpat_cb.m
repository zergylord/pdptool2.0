function loadnewpat_cb(listhandle,gcfhandle)
% This is a callback routine executed for every train/test option GUI's 
% 'Load new' button. This presents a File open dialog box, checks if the 
% selected file is already loaded or not. If not, it updates the pattern
% files pop-up menu item and the 'patfiles' appdata.

global net;

filematch = [];
if strcmp(net.type,'tdbp')
    [filename, pathname] = uigetfile({'*.m','Environment Classes (*.m)'}, ...
        'Load Environment Class');
else
    [filename, pathname] = uigetfile({'*.pat','Pattern Files (*.pat)'}, ...
        'Load Pattern File');
end
if isequal([filename,pathname],[0,0])
	return;
else
    File = fullfile(pathname,filename);
    [p,name,e]=fileparts(filename);
    if strcmpi (pathname(1:end-1),pwd)
       File = filename;
    end    
    pfiles = getappdata(gcfhandle,'patfiles');
    if ~isempty(pfiles)
        filematch = find(strcmpi(File,pfiles(:,2)));
    end
    if isempty(filematch)        
       pfiles{end+1,1}=name;
       pfiles{end,2}=File;
       patlist = update_patlist(listhandle,name);
       set(listhandle,'String',patlist,'Value',numel(patlist));        
    end
    setappdata(gcfhandle,'patfiles',pfiles);
end