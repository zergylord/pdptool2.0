function setoutputlog(varargin)
global net outputlog PDPAppdata filevarname;
fileind = find(strcmpi(varargin,'file'));
if isempty(fileind) || fileind+1 > numel(varargin)
   error('Output log cannot be specified without file name');
else
    fi = 0;
    if isempty(outputlog)
       outputlog=struct('file','','process','','frequency','','status','off',...
                    'writemode','text','logopts','off','objects',{},'cobjects',{},...
                    'onreset','clear','plot','off','plotlayout',[],...                    
                    'graphdata',[],'binout',[],'varstruct',{});
    else
        fname = varargin{fileind+1};
        [tf, fi] = ismember (fname,{outputlog.file});
    end
    if fi == 0
       newlog.file = 'log1.mat';
       newlog.process = 'test';
       newlog.frequency = 'pattern';
       newlog.status = 'off';
       newlog.writemode = 'binary';
       newlog.logopts = 'off';
       newlog.objects = [];
       newlog.cobjects = '';
       newlog.onreset = 'clear';       
       newlog.plot = 'off';
       newlog.plotlayout = [];
       newlog.graphdata = []; 
       newlog.binout = [];
       newlog.varstruct = {};
       nextlog = numel(outputlog) +1;
    else
       newlog = outputlog(fi);
       nextlog = fi;
    end
end
currlog = parse_pairs(newlog,varargin);
if isempty(currlog.objects)
    return;
end
networks= PDPAppdata.networks;
if fi == 0 || any(strcmpi(currlog.objects, outputlog(fi).objects))
   currlog.varstruct = setwritelog(currlog.objects);
   currlog.cobjects = currlog.objects;
   currlog.cobjects = regexprep(currlog.cobjects,'\(|\)','');
   currlog.cobjects = regexprep(currlog.cobjects,'\.','_');
   currlog.cobjects = regexprep(currlog.cobjects,',','x');
   currlog.cobjects = regexprep(currlog.cobjects,':','_');    
end
% outputlog(lognum).cvars = cvars;
if PDPAppdata.lognetwork
   tempA = rmfield(currlog,{'file','varstruct','cobjects','graphdata','binout'});
   tempB = rmfield(newlog,{'file','varstruct','cobjects','graphdata','binout'});
   update_params = compare_struct(tempA,tempB); %cell array of changed field names and values
   if ~isempty(update_params)
       if isempty(filevarname)
          statement = sprintf('%s (''file'', ''%s'',',mfilename,currlog.file);
       else
          statement = sprintf('%s (''file'', %s,',mfilename,filevarname);
       end
       for i=1:size(update_params,1)
           field_name = update_params{i,1};
           field_val = update_params{i,2};
           if iscell(field_val)
              field_val = sprintf('''%s'',',field_val{1:end});
              field_val(end)='}';
              field_val = strcat('{',field_val);
              statement = sprintf('%s''%s'', %s,',statement, ...
                          field_name,field_val);
           else
              if ~ischar(field_val) && ~isscalar(field_val)
                 statement = sprintf('%s''%s'', %s,',statement,field_name,...
                             mat2str(field_val));
              else
                 statement = sprintf('%s''%s'', ''%s'',',statement,field_name,...
                            field_val);
              end
           end
       end
       statement= strcat(statement(1:end-1),');');
       updatelog(statement);
   end
%    fields = fieldnames(rmfield(currlog,{'file','varstruct','cobjects','graphdata','binout'}));
%    tempA = struct2cell(rmfield(currlog,{'file','varstruct','cobjects','graphdata','binout'}));
%    diffind = 1:numel(tempA);
%    if fi > 0
%       currlog = orderfields(currlog, outputlog(fi));       
%       tempB = struct2cell(rmfield(outputlog(fi),{'file','varstruct','cobjects','graphdata','binout'}));
%       diffind = cellfun(@isequal,tempA,tempB); 
%       diffind = find(~diffind);     
%    end
%    if isempty(filevarname)
%       statement = sprintf('%s (''file'', ''%s'',',mfilename,currlog.file);
%    else
%       statement = sprintf('%s (''file'', %s,',mfilename,filevarname);
%    end
%    for i = 1 : numel(diffind)
%        fval =  currlog.(fields{diffind(i)});       
%        if iscell(fval)
%           fval = sprintf('''%s'',',fval{1:end});
%           fval(end)='}';
%           fval = strcat('{',fval);
%           statement = sprintf('%s''%s'', %s,',statement, ...
%                       fields{diffind(i)},fval);
%        else
%           if ~ischar(fval) && ~isscalar(fval)
%              statement = sprintf('%s''%s'', %s,',statement,...
%                       fields{diffind(i)},mat2str(fval));
%           else
%              statement = sprintf('%s''%s'', ''%s'',',statement,...
%                       fields{diffind(i)},fval);
%           end
%        end
%    end
%    statement= strcat(statement(1:end-1),');');
%    if ~isempty(diffind)
% %    statement= sprintf('%s(''%s'',%d,''%s'',''%s'',''%s'',''%s'',{%s});',mfilename,filename,...
% %                       lognum,procname{:},writefreq,logstatus,logmode,v);
%       updatelog(statement);
%    end
end
outputlog(nextlog) = currlog;
if strcmpi(outputlog(nextlog).plot,'on')
   if isempty(outputlog(nextlog).graphdata)
      outputlog(nextlog).graphdata = struct('px',[],'py',[],'fighandle',[],...
                                     'axhandle',[],'plotnum',[],'yvariables',{''},'title','',...
                                     'plot3d',0,'az',0,'el',90,'tag','',...
                                     'xlabel','','ylabel','','ylim',[]);
   end
else
   if ~isempty(outputlog(nextlog).graphdata)
      fig = outputlog(nextlog).graphdata.fighandle;
      if ishandle(fig)
         delete (fig);
      end
      outputlog(nextlog).graphdata = [];
   end
end
lnum =numel(net.outputfile)+1;
if ~isempty(net.outputfile)
    [tf, lmatch] = ismember(currlog.file, net.outputfile);
    if (lmatch~=0)
       lnum = lmatch;
    end
end
net.outputfile{lnum}= currlog.file;
networks{net.num}=net;
PDPAppdata.networks = networks;
if ~PDPAppdata.gui
   fprintf(1,'Outputlog %s set for current network\n',currlog.file);
end