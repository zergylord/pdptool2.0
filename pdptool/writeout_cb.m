function writeout_cb(chkhandle,handles)
% This examines the value of write output checkbox and enables or disables
% output log process for the current network. It looks for the network's
% output files in the current entries of the outputlog structure
% variable.If logs are found, it will close them when checkbox is unchecked
% and open them when checkbox is checked.

global net outputlog;
status = 'off';
val = 0;
if get(chkhandle,'Value') == 1
   status = 'on';
   val = 1;
end
set (handles.setwritebtn,'enable',status);
if isempty(outputlog)
   return;
end
[foundmem memindex] = ismember (net.outputfile,{outputlog.file});
memindex = memindex(memindex > 0);
if any (foundmem)
   [outputlog(memindex).status] = deal(val);
end
