% In nogui mode, this script can be run by typing 'pdptool nogui topo.m'
% on the matlab command line.
pdp
loadscript ('topo.net');
loadtemplate ('topo.tem');
loadpattern ('file','topo.pat','usefor','both');
launchnet;
setcolormap 'jmap.mat';
setcolorbar 'on';
settrainopts ('trainmode','ptrain','nepochs',200,'lrate',0.1);
net.pool(3).swan = 1;
cl_reset;
