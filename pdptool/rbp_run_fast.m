function rbp_run_fast 
global net outputlog inpools outpools hidpools patn binarylog runparams;
global tstpatn trpatn PDPAppdata;
[options,data,cpno,N,cepoch lepoch,patfield,sttick,orig_tss,orig_tce,...
 appflag] = initialize_params;
logoutflag = 0;
freqrange = {};
if ~isempty(outputlog) || ~isempty(net.outputfile)
    [logoutflag freqrange] = getloggingdetails(runparams.process);
end
PDPAppdata.stopprocess = [];
inpools =find(~cellfun('isempty',regexp({net.pool.type},'input|inout')));
hidpools = find(strcmpi({net.pool.type},'hidden'));
outpools = find(~cellfun('isempty',regexp({net.pool.type},'output|inout')));
non_input = [hidpools outpools];
if runparams.alltest==0
    clear_dEdw;
end
updatecount = 0;
lgraincount = 1;
dt = 1/net.ticksperinterval;
runmode = 0;
if strncmpi(runparams.process,'train',length(runparams.process))
   runmode =1;
end
for iter = cepoch:lepoch
    abort = PDPAppdata.stopprocess; 
    if ~isempty(abort)
       PDPAppdata.stopprocess = [];         
       PDPAppdata.(appflag) = 1;           
       return;       
    end
    net.epochno = iter;
    net.tss = orig_tss;
    net.tce = orig_tce;    
    if iter > cepoch || isempty(patn)
       patn = getpatternrange(data,options);  %gets fresh pattern range for next epoch          
       if runmode
          trpatn = patn;
       else
          tstpatn = patn;
       end
    end
    for p = cpno: N       
        pno = patn(p);
        net.stateno = 1;
        net.pss = 0;
        net.pce = 0;        
        setupextinputandtarget(data,pno);
        net.(patfield) = p;
        timesequence(net,net.pool,dt,outpools,runmode,1);
        if (options.lflag)         
           compute_dEdw;
           if isequal(lower(options.lgrain(1)),'p')
              if isequal(options.lgrainsize,lgraincount)
                 if (options.follow)
                    change_weights_follow_mx(net,options,non_input);                    
                 else
                    change_weights_mx(net.pool,options,non_input);                    
                 end
                    lgraincount = 1;                 
              else
                 lgraincount = lgraincount + 1;
              end
              for i =1:numel(net.pool)
                  for k=1:numel(net.pool(i).oproj)
                      tp = net.pool(i).oproj(k).toindex;
                      pindex = net.pool(i).oproj(k).projindex;
                      net.pool(i).oproj(k).weight = net.pool(tp).proj(pindex).weight;
                  end
              end
           end
        end
        net.tickno = 1;        
    end
    cpno = 1;
    if (options.lflag) && isequal(lower(options.lgrain(1)),'e')
        if isequal(options.lgrainsize,lgraincount)
           if (options.follow)
              change_weights_follow_mx(net,options,non_input);
           else
               change_weights_mx(net.pool,options,non_input);
           end
           lgraincount = 1;
        else
           lgraincount = lgraincount + 1;
        end
        for i =1:numel(net.pool)
            for k=1:numel(net.pool(i).oproj)
                tp = net.pool(i).oproj(k).toindex;
                pindex = net.pool(i).oproj(k).projindex;
                net.pool(i).oproj(k).weight = net.pool(tp).proj(pindex).weight;
            end
        end     
    end
    if logoutflag && ~isempty(strmatch('epoch',lower(freqrange)))
       writeoutput(runparams.process,'epoch');
    end    
    if PDPAppdata.gui && strncmpi(runparams.granularity,'epoch',...
       length(runparams.granularity))
       updatecount = update_display_step(updatecount,pno);
    end
    if net.tss < options.ecrit
       disp('Error criterion reached');
       if PDPAppdata.gui
          update_display(net.(patfield));
       end
       net.(patfield)=0;
       break;
    end
    orig_tss = 0.0;    
    orig_tce = 0.0;
end
timesequence(-1);  %this call to simply unlock file from memory
PDPAppdata.(appflag) = 0;
if PDPAppdata.gui
   update_display(-1);    
end
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1)) 
    writematfiles;
end

function [opts,dat,spat,npat,sepoch,lepoch,pfield,stick,otss,otce,...
flag]= initialize_params
global net runparams patn trpatn tstpatn PDPAppdata rangelog;
otss = 0.0;
otce = 0.0;
flag ='tstinterrupt_flag';
stick = 2;
if PDPAppdata.gui && (~isequal(size(net.nancolor),[1 3]) || ...
   any(net.nancolor > 1) || any(net.nancolor < 0))
   fprintf(1,'net.nancolor has an invalid value,switching to default RGB triplet [0.97 0.97 0.97]\n');
   net.nancolor = [0.97 0.97 0.97];
end
if strncmpi(runparams.process,'test',length(runparams.process))
   opts = net.testopts;
   dat = PDPAppdata.testData;
   pfield = 'testpatno';
   if PDPAppdata.tstinterrupt_flag ~= 1
      net.tickno = 0;
      tstpatn = getpatternrange(dat,opts);
   end
   patn = tstpatn;
   pnum = get(findobj('tag','tstpatlist'),'Value');   
   if strncmpi(runparams.mode,'run',length(runparams.mode))
      net.tickno = 1;
      if runparams.alltest
         net.testpatno = 1;
         npat = numel(dat);
      else
         net.testpatno = find(patn==pnum,1);
         npat = net.testpatno;         
      end
   else
       net.tickno = net.tickno + 1;
       if net.tickno > net.nticks+1
          net.tickno = 1;
       end
       if runparams.alltest
          if net.tickno == 1
             net.testpatno = net.testpatno + 1;
             net.pss = 0;
             net.pce = 0;             
             net.intervalno = 0;
          end
          npat = numel(dat);
          if net.testpatno > npat
             net.testpatno = 1;
          end
          if net.testpatno == 1
             net.tss = 0;
             net.tce = 0;             
          end
          otss = net.tss;       
          otce = net.tce;          
       else
         net.testpatno = find(patn==pnum,1);
         net.pss = 0;        
         net.pce = 0;         
         npat = net.testpatno;
       end
       stick = max(2,net.tickno);
   end    
   if isempty(runparams.range)
      sepoch = net.epochno;
      lepoch = net.epochno;
      spat = net.testpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);
   end
else
   opts = net.trainopts;
   pfield = 'trainpatno';
   dat = PDPAppdata.trainData;
   npat = numel(dat);
   if PDPAppdata.trinterrupt_flag
      otss = net.tss; % in interrupt mode, start with current tss
      otce = net.tce;      
   else
      trpatn = getpatternrange(dat,opts);
   end
   patn = trpatn;
   flag = 'trinterrupt_flag';
   net.tickno = 1;
   net.trainpatno = net.trainpatno + 1;
   if net.trainpatno > numel(dat)
      net.trainpatno = 1;
   end
   if net.trainpatno == 1
      net.epochno = net.epochno + 1;
      otss = 0;
      otce = 0;      
   end
   if isempty(runparams.range)
      sepoch = net.epochno ;
      if isempty(runparams.nepochs)
         lepoch = opts.nepochs + floor((sepoch-1)/opts.nepochs)*opts.nepochs;                    
%          lepoch = sepoch + opts.nepochs - 1;
      else
         net.trainpatno = 1;
         lepoch = sepoch + runparams.nepochs - 1;
      end
      spat = net.trainpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);
   end
end
rangelog = [spat sepoch];


function setupextinputandtarget(dat,pnum)
global net inpools outpools;
d = dat(pnum);
for i=inpools
    net.pool(i).extinput(1:end) = nan;
    net.pool(i).clampstates(1:end) = 0;
    net.pool(i).target(1:end) = nan;   
    f= ([d.inpools.poolnum] == i);
    if any(f)
        for j=1: size(d.inpools(f).tickpatterns,1)
            r = d.inpools(f).tickpatterns{j,1};
            v = d.inpools(f).tickpatterns{j,2};
            net.pool(i).extinput(r,1:end)= repmat(v,numel(r),1);
        end
        net.pool(i).clampstates(1:end) = d.inpools(f).hclamps;
    end
end
for i=outpools
    net.pool(i).exttarget(1:end) = nan;
    net.pool(i).target(1:end) = nan;
    net.pool(i).hastargets(1:end) = 0.0;
    f = ([d.outpools.poolnum] == i);
    if any(f)
        for j=1: size(d.outpools(f).tickpatterns,1)
            r = d.outpools(f).tickpatterns{j,1};
            v = d.outpools(f).tickpatterns{j,2};
            net.pool(i).exttarget(r,1:end)= repmat(v,numel(r),1);
            net.pool(i).hastargets(r) = 1.0;
        end
    end
end
net.cpname = dat(pnum).pname;

function clear_dEdw()
global net;
for i=1:numel(net.pool)
    for j=1:numel(net.pool(i).proj)
        net.pool(i).proj(j).dEdw(:) = 0.0; %zeros(size(net.pool(i).proj(j).dEdw));
    end
end


function compute_dEdw()
global net;
for i = 1: numel(net.pool)
    for j=1: numel(net.pool(i).proj)
        s = net.pool(i).proj(j).fromindex;
        net.pool(i).proj(j).dEdw = net.pool(i).proj(j).dEdw + net.pool(i).dEdnethistory(:,2:end)  * net.pool(s).acthistory(:,1:end-1)';
    end
end


function currcount = update_display_step(currcount,pno)
global runparams;
currcount = currcount+1;
if currcount == runparams.count
    currcount = 0;
    update_display(pno);
    if strncmpi(runparams.mode,'step',length(runparams.mode))
       currcount = -1;
    end
end


function range = getpatternrange(patterns,procoptions)
N = numel(patterns);
switch procoptions.trainmode(1)
    case 's'  %strain
        range = 1:N;
    case 'p'  %ptrain
        range = randperm(N);
    case 'r' %rtrain
        range = ceil(N * rand(1,N));
    otherwise
        range =1: N;
end
 