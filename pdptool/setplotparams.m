function setplotparams(varargin)
global outputlog PDPAppdata filevarname;
if isempty(outputlog)
   error ('No output log currently open, plot parameters must be associated with an output log');
end
fileind = find(strcmpi(varargin,'file'));
if isempty(fileind) || fileind+1 > numel(varargin)
   error('No ''file'' parameter - plot parameters must be asscociated with an output log file');
end
index = find(ismember({outputlog.file},varargin{fileind+1}));
if isempty(index)
   error('Cannot find any open output log associated with file name ''%s''',varargin{fileind+1});
end
args = {varargin{1:fileind-1},varargin{fileind+2 : end}};
plotdata.plotnum = 1;
plotdata.yvariables = {''};
plotdata.title = '';
plotdata.plot3d = 0;
plotdata.az = 0;
plotdata.el = 90;
plotdata.tag = '';
plotdata.xlabel = '';
plotdata.ylabel = '';
plotdata.ylim = [];
newplotdata = parse_pairs(plotdata,args);
% pnum = newplotdata.plotnum;
pnum = 1;
if isempty(outputlog(index).graphdata)
   outputlog(index).graphdata = struct('px',[],'py',[],'fighandle',[],...
                                     'axhandle',[],'plotnum',[],'yvariables',{''},...
                                     'title','','plot3d',0,'az',0,'el',90,...
                                     'tag','','xlabel','','ylabel','','ylim',[]); 
else
   gdata = outputlog(index).graphdata;
   positions = cell2mat({gdata.plotnum});
   if ~isempty(positions)
      pn = find(positions == newplotdata.plotnum);
      if isempty(pn)
         pnum = numel(gdata) + 1;
       else
         pnum = pn;
      end
   end
end
outputlog(index).graphdata(pnum).yvariables = newplotdata.yvariables;
outputlog(index).graphdata(pnum).plotnum = newplotdata.plotnum;
outputlog(index).graphdata(pnum).title = newplotdata.title;
outputlog(index).graphdata(pnum).plot3d = newplotdata.plot3d;
outputlog(index).graphdata(pnum).az = newplotdata.az;
outputlog(index).graphdata(pnum).el = newplotdata.el;
outputlog(index).graphdata(pnum).tag = newplotdata.tag;
outputlog(index).graphdata(pnum).xlabel = newplotdata.xlabel;
outputlog(index).graphdata(pnum).ylabel = newplotdata.ylabel;
outputlog(index).graphdata(pnum).ylim = newplotdata.ylim;

if PDPAppdata.lognetwork 
   update_params = compare_struct(newplotdata,plotdata);
   if ~isempty(update_params)
       if isempty(filevarname)
          statement = sprintf('%s (''file'', ''%s'',',mfilename,varargin{fileind+1});
       else
          statement = sprintf('%s (''file'', %s,',mfilename,filevarname);
       end
       for i = 1 : size(update_params,1)
           field_name = update_params{i,1};
           field_val = update_params{i,2};
           statement = sprintf('%s ''%s'',',statement,field_name);
           if iscell(field_val)
              field_val = sprintf('''%s'',',field_val{1:end});
              field_val(end)='}';
              field_val = strcat('{',field_val);
              statement = sprintf('%s %s,',statement,field_val);
           else
              if isnumeric(field_val)
                 if isscalar(field_val)
                    statement = sprintf('%s %d,',statement,field_val);
                 else
                    statement = sprintf('%s %s,',statement,mat2str(field_val));                 
                 end
              else
                 statement = sprintf('%s ''%s'',',statement,field_val);
              end
           end
       end
%    for i = 1 : 2: numel(args)
%        statement = sprintf('%s ''%s'',',statement,args{i});
%        fval =  args{i+1};       
%        if iscell(fval)
%           fval = sprintf('''%s'',',fval{1:end});
%           fval(end)='}';
%           fval = strcat('{',fval);
%           statement = sprintf('%s %s,',statement,fval);
%        else
%           if isnumeric(fval)
%              if isscalar(fval)
%                 statement = sprintf('%s %d,',statement,fval);
%              else
%                 statement = sprintf('%s %s,',statement,mat2str(fval));                 
%              end
%           else
%              statement = sprintf('%s ''%s'',',statement,fval);
%           end
%        end
%    end
     statement= strcat(statement(1:end-1),');');
     updatelog(statement);
   end
end
