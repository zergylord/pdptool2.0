function varargout = cl_testoption(varargin)
% CL_TESTOPTION M-file for cl_testoption.fig
% This is called when user clicks on
% (i)  'Set Testing options' item from the 'Network' menu of the pdp window.
% (ii) 'Options' button on the Test panel of the network viewer window.
% It presents testing parameters for 'cl' type of network that can be 
% modified and saved.  

% Last Modified 03-May-2007 10:14:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cl_testoption_OpeningFcn, ...
                   'gui_OutputFcn',  @cl_testoption_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before cl_testoption is made visible.
function cl_testoption_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cl_testoption (see VARARGIN)


% Choose default command line output for cl_testoption
handles.output = hObject;
movegui('center');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cl_testoption wait for user response (see UIRESUME)
% uiwait(handles.cl_testoption);


% --- Outputs from this function are returned to the command line.
function varargout = cl_testoption_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function tstoptnfilepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
testpatfiles = testpatternspopup_cfn(hObject);
setappdata (findobj('tag','cl_testoption'),'patfiles',testpatfiles);

function writeoutchk_Callback(hObject, eventdata, handles)
writeout_cb (hObject,handles);

function setwritebtn_Callback(hObject, eventdata, handles)

% Calls the module that presents options for writing network output logs.
% Once the setwriteopts window is up, this makes 'test' as the default 
% logging process.
% setwriteopts;
create_edit_log;
logproc = findobj('tag','logprocpop');
if ~isempty(logproc)
    set(logproc,'Value',2);
end

function tstoptnfilepop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');


function tstoptnloadpatbtn_Callback(hObject, eventdata, handles)
loadnewpat_cb(handles.tstoptnfilepop,handles.cl_testoption);

function okbtn_Callback(hObject, eventdata, handles)

% Applies current options and exits.
apply_options;
delete(handles.cl_testoption);


function cancelbtn_Callback(hObject, eventdata, handles)

% Exits without making any changes
delete(handles.cl_testoption);

function applybtn_Callback(hObject, eventdata, handles)

% Calls subroutine that sets network parameters.Does not exit from window.
apply_options;

function apply_options ()

% This reads in the new parameters and applies changes to the network
% testopts structure.
handles = guihandles;
global net PDPAppdata;
n = PDPAppdata.networks;

if isfield(net,'testopts') 
   tstopts = net.testopts;
   tempopts = tstopts;
end
tstopts.nepochs = 1;

patlist = get(handles.tstoptnfilepop,'String');
pfiles = getappdata(handles.cl_testoption,'patfiles');
mainpfiles = PDPAppdata.patfiles;
if ~isempty(pfiles)
    lasterror('reset'); %in case error was caught when loading from main window    
    if isempty(mainpfiles)
       newentries = pfiles;
    else
        [tf, ind] = ismember(pfiles(:,2), mainpfiles(:,2));
        [rows, cols] = find(~tf);
        newentries = pfiles(unique(rows),:);
    end
    for i = 1 : size(newentries,1)
        loadpattern('file',newentries{i,2},'setname',newentries{i,1},...
                     'usefor','test');
        err = lasterror;
        if any(strcmpi({err.stack.name},'readpatterns'))
           delval = find(strcmp(patlist,newentries{i,1}),1);
           patlist(delval) = [];
           set(handles.tstoptnfilepop,'String',patlist,'Value',numel(patlist));
           pfiles(delval-1,:) = [];
           lasterror('reset');
        end
     end
end
plistval = get(handles.tstoptnfilepop,'Value');
tstopts.testset = patlist{plistval};

% -- Calls settestopts command with only the modified fields of the testopts
%    structure.
update_params = compare_struct(tstopts,tempopts);
if ~isempty(update_params)
   args = update_params(:,1:2)'; %Only interested in first 2 columns of nx3 cell array. Transverse array to make column major
   args = reshape(args,[1,numel(args)]); %reshape to make it single row of fieldnames, values
   settestopts(args{1:end});
   if PDPAppdata.lognetwork
      tststmt = 'settestopts (';
      for i=1:size(update_params,1)
          field_name = update_params{i,1};
          field_val = update_params{i,2};
          if ischar(field_val)
             tststmt = sprintf('%s''%s'',''%s'',',tststmt,field_name,field_val);
          else
             tststmt = sprintf('%s''%s'',%g,',tststmt,field_name,field_val);
          end
      end
      tststmt = sprintf('%s);',tststmt(1:end-1));
      updatelog(tststmt);
   end
end

% tempopts =orderfields(tempopts,tstopts);
% fields = fieldnames(tempopts);
% tempA = struct2cell(tempopts);    
% tempB = struct2cell(tstopts);
% diffind = cellfun(@isequal,tempA,tempB);
% diffind = find(~diffind);  
% if ~isempty(diffind)    
%    teststmt = 'settestopts (';
%    args = cell(1,numel(diffind)*2);
%    [args(1:2:end-1)] = fields(diffind);     
%    for i = 1 : numel(diffind)
%        fval =  tstopts.(fields{diffind(i)});
%        args{i*2} = fval;             
%        if ischar(fval)
%           teststmt = sprintf('%s''%s'',''%s'',',teststmt,...
%                      fields{diffind(i)},fval);
%        else
%           teststmt = sprintf('%s''%s'',%g,',teststmt,...
%                      fields{diffind(i)},fval);
%        end
%    end
%    settestopts(args{1:end});
%    if PDPAppdata.lognetwork
%       teststmt = sprintf('%s);',teststmt(1:end-1));
%       updatelog(teststmt);
%    end    
% end

set (findobj('tag','testpatpop'),'String',patlist,'Value',plistval);
n{net.num} = net;
PDPAppdata.networks = n;
% If user applies changes without dismissing window
setappdata(handles.cl_testoption,'patfiles',pfiles);


% -- All edited uicontrols that have KeypressFcn (done through guide) set  
%    to change background color to yellow is changed back to white.This 
%    indicates that modified fields have been saved.
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');
