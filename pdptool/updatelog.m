function updatelog(entry)
global PDPAppdata;
file = PDPAppdata.logfilename;
if isempty(file) &&  ~PDPAppdata.lognetwork 
   fprintf(1,'ERROR: no log file available/log disabled');
   return;
end
logfd = fopen(file,'a');
if ~isdeployed
   hist = PDPAppdata.history;
   ss = textread(hist.fname,'%[^\n]');
   sizehist = size(char(ss),1);
   if hist.histsize > 0 && sizehist > hist.histsize
      fprintf(logfd,'\n%s',ss{hist.histsize+1:end});
      hist.histsize= sizehist;
   end
PDPAppdata.history = hist;
end
if ~isempty(entry)
    fprintf(logfd,'\n%s',entry);
end
fclose(logfd);