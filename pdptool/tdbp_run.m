function tdbp_run
global net outputlog inpools outpools hidpools contextpools cstep add_to_patlist state_vals display_end_state;
global binarylog runparams PDPAppdata;
global envir;
inpools = find(strcmpi( {net.pool.type},'input'));
hidpools = find(strcmpi({net.pool.type},'hidden'));
outpools = find(strcmpi({net.pool.type},'output'));
contxt = 1;
for i = 1:numel(hidpools)
    hind = hidpools(i);
    for j = 1:numel(net.pool(hind).proj)
        if strcmpi(net.pool(hind).proj(j).constraint_type,'copyback')
           from = net.pool(hind).proj(j).frompool;
           fromind = find(strcmpi({net.pool.name},from));
           contextpools(contxt).poolnum = hind;
           contextpools(contxt).fpoolnum = fromind;
           contxt = contxt + 1;
        end
    end
end
[options,envir,cstep,cepoch,lepoch,appflag] = initialize_params;
logoutflag = 0;
freqrange = {};
if ~isempty(outputlog) || ~isempty(net.outputfile)
    [logoutflag freqrange] = getloggingdetails(runparams.process);
end
PDPAppdata.stopprocess = [];
updatecount = 0;
lgraincount = 1;
epoch_count = 0;
reward_sum = zeros(1,net.noutputs);
add_to_patlist = {};    % cell array of inputs within an epoch

if PDPAppdata.gui && strcmp(net.netmode,'afterstate') && display_end_state
    displayTerminalState(appflag);
    return;
end

for iter = cepoch:lepoch    %loops over epochs
    pause(0);
    if strcmpi(runparams.process,'train')
        net.epochno = iter;
        tdbp_settemp();
    end
    almost_done = false;
    done = false;
    cutoff_reached = false;
    while ~done  % loops over steps
        if ~almost_done     % don't count the last loop bc it is only for training and does not reflect environment state
            cstep = cstep + 1;
        end
        %check for stepcutoff limit reached in test mode
        if isfield(options, 'stepcutoff') && cstep >= options.stepcutoff
            cutoff_reached = true;
            add_to_patlist = {add_to_patlist{:}, 'Step cutoff reached'};
            update_gui();
            break;
        end
        %check for stop request
        abort = PDPAppdata.stopprocess;
        if ~isempty(abort)
            PDPAppdata.stopprocess = [];
            if strncmpi(runparams.granularity,'epoch',...
                    length(runparams.granularity))
                net.epochno = net.epochno - 1;
            end
%             if PDPAppdata.gui
%                 update_display(numel(get(findobj('tag','));
%             end
            PDPAppdata.(appflag) = 1;
            return;
        end
        update_context_pools(options);
        if strcmp(net.netmode,'afterstate') && ~envir.isTerminal() % afterstate mode evaluates actions first
            input_state = choose_next_state(options);
        elseif strcmp(net.netmode,'beforestate')
            input_state = envir.getCurrentState(); 
        end
        set_input(input_state);
        compute_output();
        if (~envir.lflag)
            display('not learning');
        end
        if (options.lflag && envir.lflag)
            if cstep ~= 1   % no learning on first time step
                compute_error(options);
                compute_weight_change(options);
                if isequal(lower(options.lgrain(1)),'p')
                    if isequal(options.lgrainsize,lgraincount)
                        change_weights();
                        compute_output();
                        lgraincount = 1;
                    else
                        lgraincount = lgraincount + 1;
                    end
                end
                compute_output();
                save_output();
                compute_delta();
                compute_eligtrace(options);
            else
                compute_error(options);
                reset_eligtrace();
                save_output();
                compute_delta();
                compute_eligtrace(options);
            end
        end
        if strcmp(net.netmode,'beforestate')
            reward = envir.getCurrentReward();  % to display in pattern list
        	selected_next_state = choose_next_state(options);
            set_next_state(options,selected_next_state);
        elseif strcmp(net.netmode,'afterstate') && ~envir.isTerminal()
            set_next_state(options,input_state);
            reward = envir.getCurrentReward();  % to display in pattern list
        end
        net.reward = reward;
        net.steps = cstep;
        if logoutflag
            freq_index = find(ismember(lower(freqrange),{'pattern','patsbyepoch'}));
            freq = cellstr(freqrange);
            for fq = 1: numel(freq_index)
                writeoutput(runparams.process,freq{freq_index(fq)});
            end
        end
        if PDPAppdata.gui && ~almost_done && (strncmpi(runparams.granularity,'step',...
                length(runparams.granularity)) || strncmpi(runparams.granularity,...
                'cycle',length(runparams.granularity)))
            new_pat = '';
            for i=1:length(input_state)
                new_pat = [new_pat,' ',num2str(input_state(i))];
            end
            %new_pat = [num2str(input_state)];  %formats the pattern with
            %                                    large, but even spaces
            new_pat = [new_pat,'  r: ',num2str(reward)];
            add_to_patlist = {add_to_patlist{:}, new_pat, state_vals{:}};
            if envir.isTerminal()
                if display_end_state || runparams.count > 1
%                     add_to_patlist = {add_to_patlist{:},sprintf('END steps: %i terminal r: %s',cstep,num2str(envir.getCurrentReward()))};
%                     update_gui();
                    displayTerminalState(appflag);
                    display_end_state = 0;
                    return;
                else
                    display_end_state = 1;
                end
            end
            updatecount = update_display_step(updatecount);
            if updatecount < 0 % 'step' mode
                PDPAppdata.(appflag) = 1;
                return;     %step mode exits loop here
            end
        end
        if almost_done
            done = true;
        end
        if envir.isTerminal()
            almost_done = true;
        end
    end
    if (options.lflag) && isequal(lower(options.lgrain(1)),'e')
        if isequal(options.lgrainsize,lgraincount)
            change_weights();
            lgraincount = 1;
        else
            lgraincount = lgraincount+1;
        end
    end
    if logoutflag && ~isempty(strmatch('epoch',lower(freqrange)))
        writeoutput(runparams.process,'epoch');
    end
    if ~cutoff_reached && PDPAppdata.gui && strncmpi(runparams.granularity,'epoch',...
            length(runparams.granularity))
        add_to_patlist = {add_to_patlist{:},sprintf('steps: %i terminal r: %s',cstep,num2str(reward))};
        updatecount = update_display_step(updatecount);    
    end
    if PDPAppdata.gui && updatecount < 0 
        break; % to check if binary output files need to be written
    end
    cstep = 0;
    net.steps = 0;
    net.reward = 0;
    reward_sum = reward_sum + envir.getCurrentReward();
    epoch_count = epoch_count + 1;
    reset_eligtrace();
    envir = envir.reset();
end
if strcmpi(runparams.process,'test') && epoch_count > 1    % print the average reward attained over test epochs
    avg_reward = reward_sum / epoch_count;
    add_to_patlist = {sprintf('average reward: %s',num2str(avg_reward))};
    unused = update_display_step(runparams.count-1);
end
if options.runstats
    add_to_patlist = envir.runStats();
    unused = update_display_step(runparams.count-1);
end
if PDPAppdata.gui
    update_display(-1);
end
PDPAppdata.(appflag) = 0;
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1))
    writematfiles;
end

function [opts,environment,curstep,sepoch,lepoch,flag]= initialize_params
global net runparams PDPAppdata envir cstep display_end_state rangelog;



flag ='tstinterrupt_flag';
if PDPAppdata.gui && (~isequal(size(net.nancolor),[1 3]) || ...
        any(net.nancolor > 1) || any(net.nancolor < 0))
    fprintf(1,'net.nancolor has an invalid value,switching to default RGB triplet [0.97 0.97 0.97]\n');
    net.nancolor = [0.97 0.97 0.97];
end
if strcmpi(runparams.process,'test')
    opts = net.testopts;
    opts.gamma = net.trainopts.gamma;
    envir_name = PDPAppdata.testData;
    if (~PDPAppdata.tstinterrupt_flag || envir.isTerminal()) && ~display_end_state
        eval(['environment=',envir_name,'();']);  % create the environment object
        curstep = 0;
        reset_eligtrace();
        display_end_state = 0;
    else
        environment = envir;
        curstep = cstep;
    end
    flag = 'tstinterrupt_flag';
    sepoch = 1;
    if isempty(runparams.nepochs)
        lepoch = opts.nepochs + floor((sepoch-1)/opts.nepochs)*opts.nepochs;
    else
        lepoch = sepoch + runparams.nepochs - 1;
    end
else
    opts = net.trainopts;
    envir_name = PDPAppdata.trainData;
    if (~PDPAppdata.trinterrupt_flag || envir.isTerminal()) && ~display_end_state
        eval(['environment=',envir_name,'();']);  % create the environment object
        curstep = 0;
        reset_eligtrace();
        net.epochno = net.epochno + 1;
        display_end_state = 0;
    else
        environment = envir;
        curstep = cstep;
    end
    flag = 'trinterrupt_flag';
    sepoch = net.epochno;
    if isempty(runparams.nepochs)
        lepoch = opts.nepochs + floor((sepoch-1)/opts.nepochs)*opts.nepochs;
    else
        lepoch = sepoch + runparams.nepochs - 1;
    end
end
rangelog = [cstep sepoch];

function displayTerminalState(appflag)
global display_end_state add_to_patlist PDPAppdata envir state_vals cstep;
    display_end_state = 0;
    new_pat = '';
    end_state = envir.getCurrentState();
    for i=1:length(end_state)
        new_pat = [new_pat,' ',num2str(end_state(i))];
    end
    %new_pat = [num2str(input_state)];  %formats the pattern with
    %                                    large, but even spaces
    add_to_patlist = {add_to_patlist{:}, new_pat, state_vals{:}};
    add_to_patlist = {add_to_patlist{:},sprintf('END steps: %i terminal r: %s',cstep,num2str(envir.getCurrentReward()))};
    set_input(end_state);
    update_gui();
    %updatecount = update_display_step(updatecount);
    PDPAppdata.(appflag) = 1;
    return;

%right now, this function assumes that the network has only one output
%value
function next_state = choose_next_state(options)
global net outpools envir runparams state_vals;
next_states = envir.getNextStates();
s = size(next_states,1);
state_values = zeros(s,net.noutputs);
temp = get_input(); % so we can restore the input state after we test the next states
for i=1:s
    set_input(next_states(i,:));
    compute_output();
    output = zeros(1, net.noutputs);
    k = 1;
    for j=1:numel(outpools)
        ind = outpools(j);
        nu = k + net.pool(ind).nunits - 1;
        output(k:nu) = net.pool(ind).activation;
        k = nu + 1;
    end
    state_values(i,:) = output;
end
state_vals = {};
if options.showvals && strcmp(runparams.granularity,'step')
    for i=1:s(1)
        new_pat = [];
        for j=1:length(next_states(i,:))
            new_pat = [new_pat,' ',num2str(next_states(i,j))];
        end
        state_vals = {state_vals{:}, ['    in:', new_pat, ' out:', num2str(state_values(i,:))]};
    end
end
switch options.policy
    case 'greedy'   % choose best state
        [val choice] = max(state_values(:,1));
        next_state = next_states(choice,:);
    case 'egreedy'  % choose best state with prob 1 - epsilon
        if options.epsilon < rand
            s = size(state_values);
            choice = ceil(rand * s(1));
        else
            [val choice] = max(state_values(:,1));
        end
        next_state = next_states(choice,:);
    case 'softmax'  % choose best state with prob determined by state values and Gibb's distribution
        if options.temp == 0    % if temp is 0, then just do greedy policy
            [val choice] = max(state_values(:,1));
            next_state = next_states(choice,:);
        else
            values = state_values / options.temp;
            values = exp(values);
            total = sum(values);
            values = values / total;
            choice = linear_stochastic_choice(values);
            next_state = next_states(choice,:);
        end
    case 'linearwt'
        choice = linear_stochastic_choice(state_values);
        next_state = next_states(choice,:);
    case 'defer'    % let environment class choose the next state
        next_state = zeros(1, net.ninputs); %dummy variable
    case 'userdef'
        next_state = envir.doPolicy(next_states, state_values);
end
set_input(temp); % restore input state
compute_output();

function set_next_state(options, next_state)
global envir net;
switch options.policy
    case 'greedy'
        envir = envir.setNextState(next_state);
    case 'egreedy'
        envir = envir.setNextState(next_state);
    case 'softmax'
        envir = envir.setNextState(next_state);
    case 'linearwt'
        envir = envir.setNextState(next_state);
    case 'defer'
        envir = envir.setNextState(zeros(1, net.ninputs));
    case 'userdef'
        envir = envir.setNextState(next_state);
end

function choice = linear_stochastic_choice(state_values)
total_value = sum(state_values);
value = total_value * rand;
end_range = 0;
for i=1:numel(state_values)
    end_range = end_range + state_values(i);
    if (value < end_range)
        choice = i;
        break;
    end
end


function set_input(input_state)
global net inpools;
k=1;
for i=1:numel(inpools)
    inindx = inpools(i);
    nu = k + net.pool(inindx).nunits-1;
    net.pool(inindx).activation = input_state(k:nu);
    k = nu+1;
end

function update_context_pools(options)
global net cstep contextpools;
if ~isempty(contextpools)
    for c=1:numel(contextpools)
        pind = contextpools(c).poolnum;
        frind = contextpools(c).fpoolnum;
        if cstep == 1
           num = net.pool(pind).nunits;
           net.pool(pind).activation = repmat(options.clearval,1,num);
        else
           net.pool(pind).activation = options.mu * ...
                                       net.pool(pind).activation + ...
                                       net.pool(frind).activation;
        end
    end
end

function input_state = get_input()
global net inpools;
input_state = zeros(1,net.ninputs);
k=1;
for i=1:numel(inpools)
    inindx = inpools(i);
    nu = k + net.pool(inindx).nunits-1;
    input_state(k:nu) = net.pool(inindx).activation;
    k = nu+1;
end

function retbreak = init_output(pnum)
global net hidpools outpools runparams PDPAppdata cascadeinit;
non_input =[hidpools outpools];
retbreak = 0;
for i=1:numel(non_input)
    ind = non_input(i);
    ninp=repmat(0,1,net.pool(ind).nunits);
    for j=1:numel(net.pool(ind).proj)
        from = net.pool(ind).proj(j).frompool;
        sender= net.pool(strcmpi({net.pool.name},from));  % two pools should not have same name
        if strcmpi(sender.type,'input')
            continue;
        end
        ninp = ninp + (sender.activation * net.pool(ind).proj(j).weight');
    end
    net.pool(ind).netinput = ninp;
    net.pool(ind).activation = logistic(ninp);
end
if PDPAppdata.gui && strncmpi(runparams.granularity,'cycle',...
        length(runparams.granularity))
    update_display(pnum);
    cascadeinit = 1;
    if strcmpi(runparams.mode,'step')
        retbreak = 1;
    end
end

function retval = logistic(val)
retval = val;
retval(retval > 15.9357739741644) = 15.9357739741644; % returns 0.99999988;
retval(retval < -15.9357739741644) = -15.9357739741644; % returns 0.00000012
retval = 1.0 ./(1.0 + exp(-1 * retval));

function compute_output()
global net hidpools outpools;
non_input =[hidpools outpools];
for i=1:numel(non_input)
    cind = 0;
    ind = non_input(i);
    ninput = repmat(0,1,net.pool(ind).nunits);
    for j=1:numel(net.pool(ind).proj)
        if strcmpi(net.pool(ind).proj(j).constraint_type,'copyback');
            cind = 1;
            continue;
        end
        from = net.pool(ind).proj(j).frompool;
        sender= net.pool(strcmpi({net.pool.name},from));  % two pools should not have same name
        z= sender.activation * net.pool(ind).proj(j).weight';
        ninput = ninput + z(1:end);
    end
    net.pool(ind).netinput = ninput;
    if cind == 0
        if isempty(net.pool(ind).actfunction) || strcmp(net.pool(ind).actfunction,'logistic')
            net.pool(ind).activation = logistic(ninput);
        elseif strcmp(net.pool(ind).actfunction,'linear')
            net.pool(ind).activation = ninput;
        end
    end
end

function save_output()  % this saves the activation for use in forming TD errors on next time step
global net outpools;
for i=1:numel(outpools)
    ind = outpools(i);
    net.pool(ind).prevactivation = net.pool(ind).activation;
end


function compute_error(options)
global net hidpools outpools envir cstep;
for i=1:numel(outpools) %zero outpool errors
    ind = outpools(i);
    net.pool(ind).error = zeros(1,net.pool(i).nunits);
end
for i=1:numel(hidpools) %empty hidpool error
    ind = hidpools(i);
    net.pool(ind).error = [];
end
for i=1:numel(outpools) % calculate TD error signal on outpools
    ind = outpools(i);
    gamma  = net.pool(ind).gamma;
    if isnan(gamma)
        gamma = options.gamma;
    end
    if isempty(cstep) || (cstep <= 1)
        net.pool(ind).error = zeros(1,net.pool(ind).nunits);
    else
        if envir.isTerminal()
            reward = get_reward(i);
            net.pool(ind).error = reward - net.pool(ind).prevactivation;
        else
            reward = get_reward(i);
            net.pool(ind).error = reward + gamma * net.pool(ind).activation - net.pool(ind).prevactivation;
        end
    end
end
backpools =[outpools hidpools(end:-1:1)]; %order is important
for i=1:numel(backpools)
    ind = backpools(i);
    for j=1:numel(net.pool(ind).proj)
        if strcmpi(net.pool(ind).proj(j).constraint_type,'copyback');
            continue;
        end
        from = net.pool(ind).proj(j).frompool;
        s = strcmpi({net.pool.name},from);
        if strcmpi(net.pool(s).type,'input') || strcmpi(net.pool(s).type,'bias')
            continue;
        end
        %errors get concatenated at each pool for computing eligibility
        net.pool(s).error = [net.pool(s).error net.pool(ind).error];
    end
end

function compute_delta()
global net hidpools outpools;
for i=1:numel(outpools) %zero outpool deltas
    ind = outpools(i);
    net.pool(ind).delta = zeros(1,net.pool(i).nunits);
end
for i=1:numel(hidpools) %empty hidpool deltas
    ind = hidpools(i);
    net.pool(ind).delta = [];
end
for i=1:numel(outpools) % calculate delta on outpools
    ind = outpools(i);
    if isempty(net.pool(ind).actfunction) || strcmp(net.pool(ind).actfunction,'logistic')
        net.pool(ind).delta = net.pool(ind).activation .* (1 - net.pool(ind).activation);
    elseif strcmp(net.pool(ind).actfunction,'linear')
        net.pool(ind).delta = ones(1,net.pool(ind).nunits);
    end
    for j=1:numel(net.pool(ind).proj)   % calculate deltas on pools directly connecting to outpools
        from = net.pool(ind).proj(j).frompool;
        s = strcmpi({net.pool.name},from);
        if strcmpi(net.pool(s).type,'input') || strcmpi(net.pool(s).type,'bias')
            continue;
        end
        new_delta = zeros(numel(net.pool(ind).error), net.pool(s).nunits);
        for k=1:numel(net.pool(ind).error)
            new_delta(k,:) = (net.pool(ind).delta(k) * net.pool(ind).proj(j).weight(k,:)) .* ...
                (net.pool(s).activation .* (1 - net.pool(s).activation));
        end
        net.pool(s).delta = cat(1,net.pool(s).delta,new_delta);
    end
end
backpools = hidpools(end:-1:1); %order is important
for i=1:numel(backpools)
    ind = backpools(i);
    for j=1:numel(net.pool(ind).proj)
        if strcmpi(net.pool(ind).proj(j).constraint_type,'copyback');
            continue;
        end
        from = net.pool(ind).proj(j).frompool;
        s = strcmpi({net.pool.name},from);
        if strcmpi(net.pool(s).type,'input') || strcmpi(net.pool(s).type,'bias')
            continue;
        end
        new_delta = zeros(numel(net.pool(ind).error), net.pool(s).nunits);
        for k=1:numel(net.pool(ind).error)
            for l=1:net.pool(ind).nunits
                for m=1:net.pool(s).nunits
                    new_delta(k,m) = (net.pool(ind).delta(k,l) * net.pool(ind).proj(j).weight(l,m)) * ...
                        (net.pool(s).activation(m) * (1 - net.pool(s).activation(m)));
                end
            end
        end
        net.pool(s).delta = cat(1,net.pool(s).delta,new_delta);
    end
end

function reward = get_reward(ind)    % gets the reward vector for the output pool outpools(ind), ind is index into outpools
global net envir outpools;
current_reward = envir.getCurrentReward();
start_range = 1;
for i=1:ind-1
    start_range = start_range + net.pool(outpools(i)).nunits;
end
end_range = start_range + net.pool(outpools(ind)).nunits - 1;
reward = current_reward(start_range:end_range);

function compute_eligtrace(opts)
global net outpools;
for i=1:numel(net.pool)
    if strcmpi(net.pool(i).type,'input') || strcmpi(net.pool(i).type,'bias')
        continue;
    end
    for j=1:numel(net.pool(i).proj)
        if strcmpi(net.pool(i).proj(j).constraint_type,'copyback');
            continue;
        end
        lambda = net.pool(i).proj(j).lambda;
        from = net.pool(i).proj(j).frompool;
        s = strcmpi({net.pool.name},from);
        if isnan(lambda)
            lambda = opts.lambda;
        end
        if ismember(i,outpools)
            for k=1:numel(net.pool(i).error)
                for l=1:net.pool(s).nunits
                    net.pool(i).proj(j).eligtrace(k,l) = lambda * net.pool(i).proj(j).eligtrace(k,l)...
                        + net.pool(i).delta(k) * net.pool(s).activation(l);
                end
            end
        else
            for k=1:numel(net.pool(i).error)
                for l=1:net.pool(s).nunits
                    for m=1:net.pool(i).nunits
                        old = lambda * net.pool(i).proj(j).eligtrace(m,l,k);
                        new =  net.pool(i).delta(k,m) * net.pool(s).activation(l);
                        net.pool(i).proj(j).eligtrace(m,l,k) = old + new;                       
                    end
                end
            end
        end
    end
end

function reset_eligtrace()
global net outpools;
opts.gamma = 0;
compute_error(opts);    %dummy opts; this sets the error vectors to the right size
for i=1:numel(net.pool)
    for j=1:numel(net.pool(i).proj)
        from = net.pool(i).proj(j).frompool;
        s = strcmpi({net.pool.name},from);
        if ismember(i, outpools)
            net.pool(i).proj(j).eligtrace = zeros(net.pool(i).nunits, net.pool(s).nunits);
        else
            net.pool(i).proj(j).eligtrace = zeros(net.pool(i).nunits, net.pool(s).nunits, numel(net.pool(i).error));
        end
    end
end

function compute_weight_change(opts)
global net hidpools outpools;
non_input =[hidpools outpools];
for i=1:numel(non_input)
    ind = non_input(i);
    for j=1:numel(net.pool(ind).proj)
        lr  = net.pool(ind).proj(j).lrate;
        if isnan(lr)
            lr = opts.lrate;
        end
        if (opts.wdecay)
            if ismember(i, outpools)
                for k=1:numel(net.pool(ind).error)
                    net.pool(ind).proj(j).dweight(k,:) = (lr * net.pool(ind).proj(j).eligtrace(k,:) * net.pool(ind).error(k)) - ...
                        opts.wdecay * net.pool(ind).proj(j).weight(k,:) + net.pool(ind).proj(j).dweight(k,:);
                end
            else
                for k=1:numel(net.pool(ind).error)
                    net.pool(ind).proj(j).dweight = (lr * net.pool(ind).proj(j).eligtrace(:,:,k) * net.pool(ind).error(k)) - ...
                        opts.wdecay * net.pool(ind).proj(j).weight + net.pool(ind).proj(j).dweight;
                end
            end
        else
            if ismember(i, outpools)
                for k=1:numel(net.pool(ind).error)
                    net.pool(ind).proj(j).dweight(k,:) = (lr * net.pool(ind).proj(j).eligtrace(k,:) * net.pool(ind).error(k))  + ...
                        net.pool(ind).proj(j).dweight(k,:);
                end
            else
                for k=1:numel(net.pool(ind).error)
                    net.pool(ind).proj(j).dweight = (lr * net.pool(ind).proj(j).eligtrace(:,:,k) * net.pool(ind).error(k))  + ...
                        net.pool(ind).proj(j).dweight;
                end
            end
        end
    end
end

function change_weights()
global net hidpools outpools;
non_input =[hidpools outpools];
for i=1:numel(non_input)
    ind = non_input(i);
    for j=1:numel(net.pool(ind).proj)
        net.pool(ind).proj(j).weight = net.pool(ind).proj(j).weight + ...
            net.pool(ind).proj(j).dweight;
        net.pool(ind).proj(j).dweight = zeros(size(net.pool(ind).proj(j).dweight));
    end
end

% this updates the pattern lists with state/epoch information 
function currcount = update_display_step(currcount)
global runparams;
currcount = currcount+1;
if currcount == runparams.count
    currcount = 0;
    update_gui();
    if strncmp(runparams.mode,'step',length(runparams.mode))
        currcount = -1;
    end
end

function update_gui()
global runparams add_to_patlist;
if strcmp(runparams.process,'train')
        patlist = findobj('tag','trpatlist');
    elseif strcmp(runparams.process,'test')
        patlist = findobj('tag','tstpatlist');
end
    current_content = get(patlist,'String');
    if ~iscellstr(current_content) && strcmp(current_content,'none')
        current_content = {};
    end
    new_content = {};
    new_content = {current_content{:}, add_to_patlist{:}};
    set(patlist,'String',new_content);
    add_to_patlist = {};
    update_display(numel(new_content));