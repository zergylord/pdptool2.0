function pdptool(varargin)
% PDPTOOL Parallel Distributed Processing application for simulating 
%         neural network models, as discussed in Parallel distributed 
%         processing: Explorations in the microstructure of cognition
%         (McClelland, J. L., Rumelhart,D. E., and the PDP research group.
%         (1986)).
%         
%      
%      Syntax : 
%      (a) pdptool
%      (b) pdptool <network script(s) | matlab script(s)> 
%      (c) pdptool nogui <network script | matlab script>
%   
%      Description :
%      (a) pdptool by itself starts the application in gui mode. It opens 
%          the main pdp window which allows user to create ,load and run
%          neural network simulations. 
%      (b) pdptool with one or more network script file names (.net) starts
%          the application in gui mode. It loads all the networks defined 
%          in each of the .net files. The last loaded network will be the
%          current network available as a 'net' struct variable in the 
%          MATLAB workspace.The networks are identified by their names and
%          listed in the network pop-up menu of the pdp window.The current
%          network can be changed by selecting from the list of available 
%          networks.
%      (c) pdptool with nogui option starts application without a graphical
%          interface. All network actions would have to be performed 
%          subsequently by invoking specific commands typed on the MATLAB 
%          command line one after the other.Alternatively, a MATLAB 
%          script(.m) file, containing all  commands can be passed as   
%          argument to the pdptool command. pdptool nogui can also be  
%          called with the name of a single network script file. This loads  
%          the network defined in the file just like in (b) ,except there 
%          are no graphical features.
%

%Code Summary : This function parses input command line arguments,
%validates network script file name or MATLAB script filename and performs
%system initialization. If pdptool is run in deployed/standalone mode, it 
%implements a pdp 'shell' that simulates MATLAB command prompt to type
%commands into. 

global PDPAppdata net; % net has to be declared here as eval statement below might parse 'net' variable found in script
gui  = 1;
pdpargs =[];
if nargin > 0
   argchk = strcmpi(varargin,'nogui');
   if any(argchk)
      gui = 0;
   end
   pdpargs = varargin(~argchk);
end
initsystem(gui);
if gui && isempty(pdpargs)
   pdp;
end
%The following block of code iterates through the list of command line
%arguments and calls the loadscript function if argument is a .net file. If
%argument is a MATLAB script, it captures all the statements in the script
%and passes them through the eval function
if ~isempty(pdpargs)
   for i = 1: numel(pdpargs)
       if exist(pdpargs{i},'file') ~=2
          error('File %s cannot be found', pdpargs{i});
       end
       [p,n,e] = fileparts(pdpargs{i});
       if ~any(strcmpi(e,{'.net','.m'}))
          error('Invalid file format - %s : should be .net or .m file',...
                pdpargs{i});
       end
       if strcmpi(e,'.net')
          pdp;
          loadscript(pdpargs{i});
       else
          PDPAppdata.lognetwork = 1;
          fr = textread(pdpargs{i},'%s','delimiter','\n');
          ev_lines = removelinebreaks(fr);
          cellfun(@eval,ev_lines);
       end
   end    
end
if isdeployed
  command = '';
  fprintf(1,'\n\nEnter pdpquit to quit\n\n');
  while ~strcmpi(command,'pdpquit')      
      try
          command = input('>>pdp ','s');
          if exist('PDPAppdata','var') ~= 1
             fprintf(1,'\nPdptool is not available anymore. Please start a new session to continue\n');
             break;
          else
              if strncmp(command,'runscript',9)
                 sarg = strtrim(command(10:end));
                 sarg = strtrim(regexprep(sarg,'[()'']',''));
                 if exist(sarg,'file') ==2
                    tlines = textread(sarg,'%s','delimiter','\n');
                    if any(strcmp(tlines,'end'))
                       blk = 0;
                       stmt = [];
                       numblk = 1;
                       for stnum = 1:numel(tlines)
                           stmt = sprintf('%s\n%s',stmt,tlines{stnum});
                           if any(~cellfun(@isempty,(regexp(tlines{stnum},{'for','while','switch','if','try'}))))
                             if blk
                                 numblk = numblk+1; % find nested blocks
                             else
                                blk =1;
                             end
                           else
                              if strcmp(tlines{stnum},'end')
                                 numblk = numblk -1;
                                 if numblk == 0
                                    blk =0;
                                 end
                              end
                           end
                           if blk == 0
                              eval(stmt);
                              stmt = [];
                           end
                       end
                    else
                        cellfun(@eval,tlines);
                    end
                 else
                    fprintf(1,'File %s does not exist\n',sarg);
                 end
              else
                  T = evalc(command);
                  if ~isempty(T)
                     fprintf(1,'%s \n',T);
                  end
              end
          end
      catch ME1
          e = getReport(ME1);
          if numel(command) > 0 && ~isempty(strtrim(command))
             fprintf(1,'Error using %s\n%s\n',command,e);
          end
     end
  end
end

   

