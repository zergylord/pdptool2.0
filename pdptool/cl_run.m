function cl_run 
global net outputlog binarylog patn trpatn tstpatn outpools hidpools 
global runparams PDPAppdata;
[options,data,cpno,N,cepoch, lepoch,patfield,appflag] = initialize_params;
logoutflag = 0;
freqrange = {};
if ~isempty(outputlog) || ~isempty(net.outputfile)
    [logoutflag freqrange] = getloggingdetails(runparams.process);
end
PDPAppdata.stopprocess = [];
hidpools = find(strcmpi({net.pool.type},'hidden'));
outpools = find(strcmpi({net.pool.type},'output'));
updatecount = 0;
for iter = cepoch:lepoch
    pause(0);
    net.epochno = iter;    
    if iter > cepoch
       patn = getpatternrange(data,options);            
       if strcmpi(runparams.process,'test')    
          tstpatn = patn;
       else
          trpatn = patn;
       end
    end
    if iter == lepoch && ~isempty(runparams.range)
        N = runparams.range(2,1);
    end
    for p = cpno: N
        abort = PDPAppdata.stopprocess; 
        if ~isempty(abort)
           PDPAppdata.stopprocess = [];            
           if strncmpi(runparams.granularity,'epoch',...
              length(runparams.granularity)) || p == 1        
              net.epochno = net.epochno - 1;
            end
            if PDPAppdata.gui
               update_display(patn(net.(patfield)));
            end
            PDPAppdata.(appflag) = 1;
            return;
        end
        net.(patfield) = p;
        pno = patn(p);
        setinput(data,pno);
        compute_output;
        if (options.lflag)
            change_weights(options);
        end
        if logoutflag && ~isempty(strmatch('pattern',lower(freqrange)))
           writeoutput(runparams.process,'pattern');
        end        
        if PDPAppdata.gui && strncmpi(runparams.granularity,'pattern',...
           length(runparams.granularity))
           updatecount = update_display_step(updatecount,pno);
           if updatecount < 0 && p ~= N % step mode
              PDPAppdata.(appflag) = 1;                             
              return;
           end           
        end
    end   
    cpno = 1;
    if logoutflag && ~isempty(strmatch('epoch',lower(freqrange)))
       writeoutput(runparams.process,'epoch');
    end    
    if PDPAppdata.gui 
       if strncmpi(runparams.granularity,'epoch',...
          length(runparams.granularity)) 
          updatecount = update_display_step(updatecount,patn(net.(patfield)));
       end
       if updatecount < 0 %when p==N for 'step' mode or 'stepping' through each epoch 
          break; % to check if binary output files need to be written
        end
    end   
end
if PDPAppdata.gui
   update_display(-1);
end
PDPAppdata.(appflag) = 0;
if ~isempty(binarylog) && ~isempty(find(cell2mat({binarylog.logind}),1)) 
    writematfiles;
end


function [opts,dat,spat,npat,sepoch,lepoch,pfield,flag]= initialize_params
global net runparams patn trpatn tstpatn PDPAppdata rangelog;
flag ='tstinterrupt_flag';
if strcmpi(runparams.process,'test')
   opts = net.testopts;
   dat = PDPAppdata.testData;
   pfield = 'testpatno';
   if PDPAppdata.tstinterrupt_flag ~= 1
      tstpatn = getpatternrange(dat,opts);
   end
   patn = tstpatn;
   net.testpatno = net.testpatno + 1;
   if (runparams.alltest == 0)   %1 for all patterns, 0 for single
      pnum = get(findobj('tag','tstpatlist'),'Value');
       net.testpatno = find(patn==pnum,1); %in case patn is in permuted or random order  
       npat = net.testpatno;
   else
       if net.testpatno > numel(dat)
          net.testpatno = 1;
       end
       npat = numel(dat);
   end
   if isempty(runparams.range)
      sepoch = net.epochno;
      lepoch = net.epochno;
      spat = net.testpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2); 
   end
else    
   opts = net.trainopts;
   pfield = 'trainpatno';   
   dat = PDPAppdata.trainData;
   npat = numel(dat);
   if PDPAppdata.trinterrupt_flag ~= 1
      trpatn = getpatternrange(dat,opts);
   end
   patn = trpatn;
   flag = 'trinterrupt_flag';
   net.trainpatno = net.trainpatno + 1;
   if net.trainpatno > numel(dat)
      net.trainpatno = 1;
   end
   if net.trainpatno == 1
      net.epochno = net.epochno + 1;
   end
   if isempty(runparams.range)
      sepoch = net.epochno;
      if isempty(runparams.nepochs)
         lepoch = opts.nepochs + floor((sepoch-1)/opts.nepochs)*opts.nepochs;        
%          lepoch = sepoch + opts.nepochs - 1;
      else
          net.trainpatno = 1;
          lepoch = sepoch + runparams.nepochs - 1;
      end
      spat = net.trainpatno;
   else
      spat = runparams.range(1,1);
      sepoch = runparams.range(1,2);
      lepoch = runparams.range(2,2);   
   end   
end
rangelog = [spat sepoch];

function setinput(pattern,patnum)
global net clspecial;
net.cpname=pattern(patnum).pname;
% net.patno=patnum;
if clspecial ==1   % create gaussian blob of activity
   in_x = pattern(patnum).ipattern(1);
   in_y = pattern(patnum).ipattern(2);
   sigma = pattern(patnum).ipattern(3);
end
for i=1:numel(net.inindx)
    np = net.inindx(i);
    if clspecial == 1
       [xi,yi] = ind2sub(net.pool(np).geometry,(1:net.pool(np).nunits));
       dist = (in_x - xi) .^ 2 + (in_y - yi) .^2;
       net.pool(np).activation = exp(-dist/(2*sigma ^ 2))./(2*pi*(sigma ^ 2));
    else
       net.pool(np).activation = pattern(patnum).ipattern ; 
    end
    [maxnet, winindex] = max(net.pool(np).activation);
    net.pool(np).winner=winindex;
    [xwin, ywin] = ind2sub(net.pool(np).geometry,winindex);
    net.pool(np).winRelPos = [xwin/net.pool(np).geometry(1) ywin/net.pool(np).geometry(2)];
end



function compute_output()
global net hidpools outpools;
non_input =[hidpools outpools];
pools = {net.pool.name};
for i=1:numel(non_input)
    y = non_input(i);
    net.pool(y).netinput(:) = 0; %repmat(0,1,net.pool(y).nunits);
    net.pool(y).activation(:) = 0; %repmat(0,1,net.pool(y).nunits);
%     tb =0;
    for j=1:numel(net.pool(y).proj)
%         if strcmpi(net.pool(y).proj(j).constraint_type,'tbias')        
%            tb = 1;
%         end
        from = net.pool(y).proj(j).frompool;
        [tf,np] = ismember(from,pools);
%         acts = find(net.pool(np).activation);
%         wt = repmat(0,size(net.pool(y).proj(j).weight));
%         wt(:,acts) = net.pool(y).proj(j).weight(:,acts);
%         ssum = sum(wt,2);
        net.pool(y).netinput = net.pool(y).netinput + net.pool(np).activation * ...
                               net.pool(y).proj(j).weight';
%         net.pool(y).netinput = net.pool(y).netinput + ssum';
    end
    [maxnet, winindex] = max(net.pool(y).netinput);
    net.pool(y).activation(winindex) =1;
    net.pool(y).winner=winindex;
    [xwin, ywin] = ind2sub(net.pool(y).geometry,net.pool(y).winner);
    net.pool(y).winRelPos = [xwin/net.pool(y).geometry(1) ywin/net.pool(y).geometry(2)];    
    if net.pool(y).swan
        [ri,rj]= ind2sub(net.pool(y).geometry,(1:net.pool(y).nunits));
        dist = ((xwin - ri) .^ 2 ) + ((ywin - rj) .^ 2);
        net.pool(y).activation = exp(-dist ./...
                                (2*net.pool(y).lrange ^ 2));
        net.pool(y).activation = net.pool(y).activation ./...
                                (2*pi*(net.pool(y).lrange ^ 2));
    end
end

function change_weights(opts)
global net;
pools = {net.pool.name};
for i=1:numel(net.pool)
    p=net.pool(i);
    for j=1:numel(p.proj)
        from = p.proj(j).frompool;
        [tf,np] = ismember(from,pools);        
        if strcmpi(p.proj(j).constraint_type,'tbias')
%            [xwin, ywin] = ind2sub(p.geometry,p.winner);
%            [ri,rj]= ind2sub(p.geometry,(1:p.nunits));
%            dist = ((xwin - ri) .^ 2 ) + ((ywin - rj) .^ 2);
%            p.responsibility = net.trainopts.lrate .* exp(-dist ./(p.lrange^2));
           wt = p.proj(j).weight;
           for k =1 :size(wt,1)
               wt(k,:) = wt(k,:) + (opts.lrate .* (p.activation(k)*(net.pool(np).activation ...
                         - wt(k,:))));
           end
           p.proj(j).weight = wt;
        else            
           wt = p.proj(j).weight(p.winner,:);
           wt = wt + (opts.lrate  .* (net.pool(np).activation - ...
                wt));
           p.proj(j).weight(p.winner,:) = wt;
        end
    end
    net.pool(i) = p;
end

function currcount = update_display_step(currcount,pno)
global runparams;
currcount = currcount+1;
if currcount == runparams.count
    currcount = 0;
    update_display(pno);
    if strncmpi(runparams.mode,'step',length(runparams.mode))
       currcount = -1;
    end
end

function range = getpatternrange(patterns,procoptions)
N = numel(patterns);
switch procoptions.trainmode(1)
    case 's'  %strain
        range = 1:N;
    case 'p'  %ptrain
        range = randperm(N);
    case 'r'  %rtrain
        range = ceil(N * rand(1,N));
    otherwise
        range = 1: N;
end