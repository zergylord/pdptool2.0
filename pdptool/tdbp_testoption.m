function varargout = tdbp_testoption(varargin)
% TDBP_TESTOPTION M-file for tdbp_testoption.fig
% This is called when user clicks on
% (i)  'Set Testing options' item from the 'Network' menu of the pdp window.
% (ii) 'Options' button on the Test panel of the network viewer window.
% It presents testing parameters for 'bp' type of network that can be 
% modified and saved.  

% Last Modified 02-May-2007 10:43:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tdbp_testoption_OpeningFcn, ...
                   'gui_OutputFcn',  @tdbp_testoption_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before tdbp_testoption is made visible.
function tdbp_testoption_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to tdbp_testoption (see VARARGIN)

% The GUI opening function sets current/existing values of testing 
% parameters to the corresponding GUI controls.

setcurrentvalues();


% Choose default command line output for tdbp_testoption
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% UIWAIT makes tdbp_testoption wait for user response (see UIRESUME)
% uiwait(handles.tdbp_testoption);


% --- Outputs from this function are returned to the command line.
function varargout = tdbp_testoption_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function nepochsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function muedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function clearvaledit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tstoptnfilepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% This sets appdata for tdbp_testoption figure handle by extracting details 
% of all test patterns currently in the Test pattern list of the main window.
testpatfiles = testpatternspopup_cfn(hObject);
setappdata (findobj('tag','tdbp_testoption'),'patfiles',testpatfiles);

function tstoptnfilepop_Callback(hObject, eventdata, handles)
set (hObject,'BackgroundColor','yellow');

function okbtn_Callback(hObject, eventdata, handles)
% Applies current options and exits.

apply_options();
delete (handles.tdbp_testoption);

function cancelbtn_Callback(hObject, eventdata, handles)
% Exits without making any changes

delete (handles.tdbp_testoption);

function applybtn_Callback(hObject, eventdata, handles)
% Calls subroutine that sets network parameters.Does not exit from window.

apply_options();


function tstoptnloadpatbtn_Callback(hObject, eventdata, handles)
loadnewpat_cb(handles.tstoptnfilepop,handles.tdbp_testoption);

function setcurrentvalues
% This sets the current testing parameter values to the corresponding GUI
% controls.

global net;
handles = guihandles;
tstopts = net.testopts;
set (handles.nepochsedit,'String',tstopts.nepochs);
set (handles.stepcutoffedit,'String',tstopts.stepcutoff);
policylist = get(handles.policypop,'String');
if strcmp(net.netmode,'sarsa')  % remove 'defer' option if in SARSA mode
    ind = find(strcmpi(policylist,'defer'));
    policylist(ind) = [];
    set(handles.policypop,'String',policylist);
end
outpools = find(strcmpi({net.pool.type},'output'));
if strcmpi(net.pool(outpools(1)).actfunction,'linear')    % remove 'linearwt' options if outpool is linear (linearwt doesn't work for negative values)
    ind = find(strcmpi(policylist,'linearwt'));
    policylist(ind) = [];
    set(handles.policypop,'String',policylist);
end
policynum = find(ismember(policylist, tstopts.policy)==1);
set(handles.policypop,'Value',policynum);
set(handles.epsilonedit,'String',num2str(tstopts.epsilon));
set(handles.tempedit,'String',num2str(tstopts.temp));
set(handles.showvalschkbox,'Value',tstopts.showvals);
set(handles.runstatschkbox,'Value',tstopts.runstats);
set (handles.muedit,'String',tstopts.mu);
set (handles.clearvaledit,'String',tstopts.clearval);



function writeoutchk_Callback(hObject, eventdata, handles)
writeout_cb(hObject,handles);


function setwritebtn_Callback(hObject, eventdata, handles)

% Calls the module that presents options for writing network output logs.
% Once the setwriteopts window is up, this makes 'test' as the default 
% logging process.
% setwriteopts;
create_edit_log;
logproc = findobj('tag','logprocpop');
if ~isempty(logproc)
    set(logproc,'Value',2);
end

function apply_options()

% This reads in the new parameters and applies changes to the network
% testopts structure.
handles = guihandles;
global net PDPAppdata;
n = PDPAppdata.networks; 

if isfield (net,'testopts') 
   tstopts = net.testopts;
   tempopts = tstopts;
end
policylist = get(handles.policypop,'String');
tstopts.policy = policylist{get(handles.policypop,'Value')};
tstopts.epsilon = str2double(get(handles.epsilonedit,'String'));
tstopts.temp = str2double(get(handles.tempedit,'String'));
tstopts.showvals = get(handles.showvalschkbox,'Value');
tstopts.runstats = get(handles.runstatschkbox,'Value');
tstopts.nepochs = str2double(get(handles.nepochsedit,'String'));
tstopts.stepcutoff = str2double(get(handles.stepcutoffedit,'String'));
tstopts.mu = str2double(get(handles.muedit,'String'));
tstopts.clearval = str2double(get(handles.clearvaledit,'String'));

patlist= get(handles.tstoptnfilepop,'String');
pfiles = getappdata(handles.tdbp_testoption,'patfiles');
mainpfiles = PDPAppdata.patfiles;
if ~isempty(pfiles)
    lasterror('reset'); %in case error was caught when loading from main window    
    if isempty(mainpfiles)
       newentries = pfiles;
    else
        [tf, ind] = ismember(pfiles(:,2), mainpfiles(:,2));
        [rows, cols] = find(~tf);
        newentries = pfiles(unique(rows),:);
    end
    for i = 1 : size(newentries,1)
        loadpattern('file',newentries{i,2},'setname',newentries{i,1},...
                     'usefor','test');
        err = lasterror;
        if any(strcmpi({err.stack.name},'readpatterns'))
           delval = find(strcmp(patlist,newentries{i,1}),1);
           patlist(delval) = [];
           set(handles.tstoptnfilepop,'String',patlist,'Value',numel(patlist));
           pfiles(delval-1,:) = [];
           lasterror('reset');
        end
     end 
end
plistval = get(handles.tstoptnfilepop,'Value');
tstopts.testset = patlist{plistval};

% -- Calls settestopts command with only the modified fields of the testopts
%    structure. 

tempopts = orderfields(tempopts,tstopts);
fields = fieldnames(tempopts);
tempA = struct2cell(tempopts);
tempB = struct2cell(tstopts);
diffind = cellfun(@isequal,tempA,tempB);
diffind = find(~diffind);
if ~isempty(diffind)
   tststmt = 'settestopts (';
   args = cell(1,numel(diffind)*2);
   [args(1:2:end-1)] = fields(diffind);      
   for i = 1 : numel(diffind)
       fval =  tstopts.(fields{diffind(i)});
       args{i*2} = fval;
       if ischar(fval)
           tststmt = sprintf('%s''%s'',''%s'',',tststmt,fields{diffind(i)},...
               fval);
       elseif numel(fval) > 1
           trstmt = sprintf('%s''%s'',%s,',trstmt,fields{diffind(i)},mat2str(fval));
       else
           tststmt = sprintf('%s''%s'',%g,',tststmt,fields{diffind(i)},fval);
       end
   end
   settestopts(args{1:end});
   if PDPAppdata.lognetwork
      tststmt = sprintf('%s);',tststmt(1:end-1));
      updatelog(tststmt);
   end 
end
set(findobj('tag','testpatpop'),'String',patlist,'Value',plistval);
n{net.num} = net;
PDPAppdata.networks = n;
% If user applies changes without dismissing window
setappdata(handles.tdbp_testoption,'patfiles',pfiles);

% -- All edited uicontrols that have KeypressFcn (done through guide) set  
%    to change background color to yellow is changed back to white.This 
%    indicates that modified fields have been saved.
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');


% --- Executes on selection change in policypop.
function policypop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');



% --- Executes during object creation, after setting all properties.
function policypop_CreateFcn(hObject, eventdata, handles)
% hObject    handle to policypop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function epsilonedit_Callback(hObject, eventdata, handles)
% hObject    handle to epsilonedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of epsilonedit as text
%        str2double(get(hObject,'String')) returns contents of epsilonedit as a double


% --- Executes during object creation, after setting all properties.
function epsilonedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to epsilonedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function nepochsedit_Callback(hObject, eventdata, handles)
% hObject    handle to nepochsedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nepochsedit as text
%        str2double(get(hObject,'String')) returns contents of nepochsedit as a double


% --- Executes on button press in showvalschkbox.
function showvalschkbox_Callback(hObject, eventdata, handles)
% hObject    handle to showvalschkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showvalschkbox



function tempedit_Callback(hObject, eventdata, handles)
% hObject    handle to tempedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tempedit as text
%        str2double(get(hObject,'String')) returns contents of tempedit as a double


% --- Executes during object creation, after setting all properties.
function tempedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tempedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in runstatschkbox.
function runstatschkbox_Callback(hObject, eventdata, handles)
% hObject    handle to runstatschkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of runstatschkbox



function stepcutoffedit_Callback(hObject, eventdata, handles)
% hObject    handle to stepcutoffedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of stepcutoffedit as text
%        str2double(get(hObject,'String')) returns contents of stepcutoffedit as a double


% --- Executes during object creation, after setting all properties.
function stepcutoffedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stepcutoffedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
