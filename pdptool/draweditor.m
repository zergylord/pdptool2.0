function draweditor(array,arrstring)
global properties;
MAX_ROWS = 10;
MAX_COLS = 10;
CELL_HEIGHT = 20;
CELL_WIDTH = 60;
CELL_BORDER = 0;
vslider = 0;
hslider = 0;
if ~isnumeric(properties.maxrows) || properties.maxrows < 1
    properties.maxrows = MAX_ROWS;
end
if properties.maxrows >= properties.numrows
   properties.maxrows = properties.numrows;
else
   vslider = 1;
end
if ~isnumeric(properties.maxcols) || properties.maxcols < 1
    properties.maxcols = MAX_COLS;
end
if properties.maxcols >= properties.numcols
   properties.maxcols = properties.numcols;
else
    hslider = 1;
end
if ~isnumeric(properties.cellheight) || properties.cellheight < 1
    properties.cellheight = CELL_HEIGHT;
end
if ~isnumeric(properties.cellwidth) || properties.cellwidth < 1
    properties.cellwidth = CELL_WIDTH;
end
if ~isnumeric(properties.cellborder) || properties.cellborder < 0
    properties.cellborder = CELL_BORDER;
end
% create column names and row names 
ncols = properties.startcol:properties.startcol+properties.numcols-1;
% ncols = 1:properties.numcols;
cnums = cellstr(num2str(ncols'));
cnums = strtrim(cnums)';
[properties.colnames{1:numel(ncols)}] = deal('C');
properties.colnames = strcat(properties.colnames,cnums);
nrows = properties.startrow:properties.startrow + properties.numrows -1;
% nrows = 1: properties.numrows;
rnums = cellstr(num2str(nrows'));
rnums = strtrim(rnums)';
[properties.rownames{1:numel(nrows)}] = deal('R');
properties.rownames = strcat(properties.rownames,rnums);

% % Get screensize in pixels
Units=get(0,'units'); 
set(0,'units','pixels'); 
ss = get(0,'ScreenSize');
set(0,'units',Units);
swidth = ss(3); 
sheight = ss(4);
button_size = 30;
slidersz= 15;
margin =15;
fig_width = 3*margin + slidersz + button_size + properties.cellwidth +  ...
            properties.cellwidth * properties.maxcols +  ...
            (properties.maxcols - 1) *properties.cellborder;
if fig_width > swidth
   properties.maxcols = MAX_COLS;
   fig_width = 3*margin + slidersz + button_size + properties.cellwidth * ...
               properties.maxcols + (properties.maxcols - 1) * ...
               properties.cellborder + properties.cellwidth;
end
fig_height = 2*margin + properties.cellheight * properties.maxrows + ...
             (properties.maxrows - 1) * properties.cellborder + slidersz+...
             properties.cellheight;
if fig_height < button_size
   fig_height = 2*margin + button_size;
end
if fig_height > sheight
   properties.maxrows = MAX_ROWS;
   fig_height = 2*margin + properties.cellheight * properties.maxrows + ...
             (properties.maxrows - 1) * properties.cellborder + slidersz + ...
             properties.cellheight;
end
left = (swidth-fig_width)/2; 
bottom = (sheight-fig_height)/2;
figrect = [left bottom fig_width fig_height];
editorfig = figure('Name','Array Editor','tag','arreditor','Units','pixels',...
            'Position',figrect,'Menubar','none','NumberTitle','off',...
            'Color', get(0,'defaultUicontrolBackgroundColor'),...
            'Resize','off','Visible','on','CloseRequestFcn',{@closeeditor});

properties.array_pos_y = (fig_height-2*properties.cellheight - margin -...
                         properties.cellborder - (0:properties.numrows-1)* ...
                         (properties.cellheight+ properties.cellborder))';
properties.array_pos_y  = properties.array_pos_y + properties.cellborder; 
properties.array_pos_x = margin + properties.cellwidth +  ...
                         (0:properties.numcols-1) *(properties.cellwidth +...
                         properties.cellborder);

for i = 1:numel(nrows)
    for j = 1:numel(ncols)
        pos = [properties.array_pos_x(j) properties.array_pos_y(i)...
               properties.cellwidth properties.cellheight];
        properties.hedits(i,j) = uicontrol('Style','edit','unit','pixels',...
            'backgroundcolor','w','position', pos,'String',array(i,j),...
            'HorizontalAlignment','left','KeypressFcn',...
             'set(gcbo,''BackgroundColor'',''magenta'')','UserData',[i,j]);
    end
end
if vslider == 1
    set(properties.hedits(properties.maxrows+1:properties.numrows,...
        1:properties.numcols),'Visible','off')    % Hide those who are out of view   
    sld_step = 1 / (properties.numrows - 1);
    sld_step(2) = 5 * sld_step(1);
    pos =[ fig_width - 2*margin - button_size - slidersz,...
          margin+slidersz ,slidersz ,fig_height-2*margin-slidersz-properties.cellheight];
    properties.vSlid = uicontrol('style','slider','units','pixels',...
                       'position',pos,'min',1,'max',properties.numrows,...
                       'Value',properties.numrows,'SliderStep',sld_step,...
                       'BackgroundColor',[0.67 0.67 0.67]); 
    set(properties.vSlid,'callback',{@vslider_callback,margin,slidersz},...
        'UserData',properties.numrows)                   
end
if hslider == 1
    set(properties.hedits(1:properties.numrows,...
        properties.maxcols+1:properties.numcols),'Visible','off')    % Hide those who are out of view 
    sld_step = 1 / (properties.numcols-1);
    sld_step(2) = 5 * sld_step(1);
    pos =[ margin+properties.cellwidth,margin,fig_width - 3*margin - ...
          button_size - slidersz-properties.cellwidth,slidersz];
    properties.hSlid = uicontrol('style','slider','units','pixels',...
                       'position',pos,'min',1,'max',properties.numcols,...
                       'Value',1,'SliderStep',sld_step,...
                       'BackgroundColor',[0.67 0.67 0.67]);  
    set(properties.hSlid,'callback',{@hslider_callback,margin,slidersz},'UserData',1);
                                   
end
properties.currentrows=1: properties.maxrows; %properties.startrow:properties.startrow+ properties.maxrows -1;
properties.currentcols=1 : properties.maxcols; %properties.startcol:properties.startrow + properties.maxcols -1;
setlabels(margin,slidersz);
btnpos = [fig_width - (button_size+ margin), margin+slidersz,...
         button_size, button_size];
okbtn = uicontrol('Style','pushbutton','units','pixels','Position',btnpos,...
        'String','Ok','Callback',{@okbtn_callback,arrstring});
set(properties.hedits,'enable',properties.enableedit);    
    
function vslider_callback(hObject,event,margin,slidersz)
global properties;
val = round(get(properties.vSlid,'Value'));
old_val = get(properties.vSlid,'UserData');
ds = val - old_val;
if ds == 0
   return;
end
set(properties.hedits,'Visible','off');
if (ds < 0)                                         % Slider moved down
    n = properties.numrows - val + 1;
    d_col = properties.numrows - val;
    if (n+properties.maxrows -1 > properties.numrows)             % Case we jumped into the midle zone
        adj = (n+properties.maxrows-1 - properties.numrows);
        n = n - adj;
        d_col = d_col - adj;
    end
    for i = n:min(n+properties.maxrows-1,properties.numrows)   % Update positions
        count =1;
        for j = properties.currentcols(1) : properties.currentcols(end)
            xpos = properties.array_pos_x(count);
            pos = get(properties.hedits(i,j),'Position');
            set(properties.hedits(i,j),'pos',[xpos properties.array_pos_y(i-d_col) pos(3:4)],'Visible','on');
            count = count+1;
        end
    end
    ud = get(properties.hedits(properties.numrows,1),'UserData');
    if (i == ud(1) ) % Bottom reached. Jump to there
        val = 1;    
        set(properties.vSlid,'Value',val) ;        % This also avoids useless UIs repositioning
    end
    properties.currentrows = n:min(n+properties.maxrows-1,properties.numrows);
elseif (ds > 0)                                     % Slider moved up
    n = properties.numrows - val + 1;  
    k = properties.maxrows;
    if (n < properties.maxrows)                          % Case we jumped into the midle zone
        adj = properties.maxrows - n;
        n = n + adj;
    end
    for i = n:-1:max(n-properties.maxrows+1,1)         % Update positions
        count=1;
        for j = properties.currentcols(1) : properties.currentcols(end)
            xpos = properties.array_pos_x(count);
            pos = get(properties.hedits(i,j),'Position');
            set(properties.hedits(i,j),'pos',[xpos properties.array_pos_y(k) pos(3:4)],'Visible','on');
            count = count+1;
        end
        k = k - 1;
    end
    if (i == 1)      % Reached Top. Jump to there
        set(properties.vSlid,'Value',properties.numrows)          % This also avoids useless UIs repositioning
        val = properties.numrows;
    end
    properties.currentrows = max(n-properties.maxrows+1,1):n;
end
set(properties.vSlid,'UserData',val);                    % Save old 'Value'
setlabels(margin,slidersz);

function hslider_callback(hObject,event,margin,slidersz)
global properties;
val = round(get(properties.hSlid,'Value'));
old_val = get(properties.hSlid,'UserData');
ds = val - old_val;
if ds == 0
   return;
end
set(properties.hedits,'Visible','off');
if (ds > 0)                                         % Slider moved right
    n = val;
    if (n+ properties.maxcols -1 > properties.numcols)
        adj = n + properties.maxcols -1 - properties.numcols;
        n = n - adj;
    end
    count=1;
    for i = properties.currentrows(1):properties.currentrows(end)    % Update positions
        d_col = n-1;
        ypos = properties.array_pos_y(count);
        for j = n:min(n+properties.maxcols-1,properties.numcols)
            pos = get(properties.hedits(i,j),'Position');
            set(properties.hedits(i,j),'pos',[properties.array_pos_x(j-d_col) ypos pos(3:4)],'Visible','on');
        end
        count = count+1;
    end
    ud = get(properties.hedits(1,properties.numcols),'UserData');
    if (j == ud(2) ) % right end reached. Jump to there
        val = properties.numcols;    
        set(properties.hSlid,'Value',val)         % This also avoids useless UIs repositioning
    end
    set(properties.hedits(1:end,j+1:end),'Visible','off');
    properties.currentcols = n:min(n+properties.maxcols-1,properties.numcols);
elseif (ds <0)                       % Slider moved left
       n = val;
%       if (properties.maxcols+n-1 > properties.numcols)                          % Case we jumped into the midle zone
%          adj = properties.maxcols - n;
%          n = n + adj;
%       end
    count=1;
    if n >= properties.maxcols
       allj = n - properties.maxcols +1 : n;
     else
       allj = n : max(properties.maxcols+n-1,properties.maxcols);
     end
    for i =properties.currentrows(1):properties.currentrows(end)        % Update positions
        k=1;
        ypos = properties.array_pos_y(count);
        for j = allj
            pos = get(properties.hedits(i,j),'Position');
            set(properties.hedits(i,j),'pos',[properties.array_pos_x(k) ypos pos(3:4)],'Visible','on'); 
            k = k+1;    
        end
        count = count+1;
    end
    if (allj(1)==1)      % Reached left. Jump to there
        set(properties.hSlid,'Value',1)          % This also avoids useless UIs repositioning
        val = 1;
    end
    properties.currentcols = allj;
end
set(properties.hSlid,'UserData',val)                      % Save old 'Value'
setlabels(margin,slidersz);

function okbtn_callback(hObject,event,arrstring)
global net properties PDPAppdata;
out = [];
if  strcmpi(properties.enableedit,'off')
    delete(findobj('tag','arreditor'));
    return;
end
        
% outcell = cell(properties.numrows,properties.numcols);
outcell = reshape(cellstr(get(properties.hedits,'String')),properties.numrows,properties.numcols);
try
    out = cellfun(@str2num,outcell);
    properties.out = out;
    if (PDPAppdata.lognetwork)
        stmt = sprintf('%s = %s;',arrstring,mat2str(out));
        updatelog(stmt);
    end
    arrstring = sprintf('%s = out',arrstring);
    evalc(arrstring);
    delete(findobj('tag','arreditor'));
catch
    fprintf(1,'Error :cell values should be numeric');
end
% refresh scalar tree item with current value
if isscalar(properties.array)
   varlist = cellstr(get(findobj('tag','vartree'),'String'));
   curr = get(findobj('tag','vartree'),'Value');
   str = varlist{curr};
   [first,second] = strtok(str,':');
   if round(properties.out) == properties.out
      str = sprintf('%s: %d',first,properties.out);
   else
      str = sprintf('%s: %.2f',first,properties.out);
   end
   varlist{curr} = str;
   set(findobj('tag','vartree'),'String',varlist);
end

function setlabels(m,s)
global properties;
figrect = get(gcf,'Position');
fig_height = figrect(4);
pos = [m + properties.cellwidth,fig_height - m-properties.cellheight,properties.cellwidth, properties.cellheight];
for i=1:properties.maxcols
    properties.cbuttons = uicontrol('Style','pushbutton','Units','pixels',...
                          'Position',pos,'String',properties.colnames{properties.currentcols(i)});
    pos(1) = pos(1) + properties.cellborder+ properties.cellwidth;                      
end
pos = [m,m+s,properties.cellwidth, properties.cellheight];
for i=properties.maxrows:-1:1
    properties.rbuttons = uicontrol('Style','pushbutton','Units','pixels',...
                          'Position',pos,'String',properties.rownames{properties.currentrows(i)});
    pos(2) = pos(2) + properties.cellheight+properties.cellborder;                      
end


function closeeditor(hObject, event,handles)
delete(hObject);
