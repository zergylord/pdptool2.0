function setcolorbar(status)
global PDPAppdata;
netdisp=findobj('tag','netdisplay');
if isempty(netdisp)
    fprintf(1,'ERROR: pdp Network viewer not available');
    return;
end
switch status
    case 'on'
          axes('Units','normalized','Position',[0.1 0.1 0.5 0.5]);
          colorbar('tag','colbar','Location','Southoutside',....
          'ButtonDownFcn',@colorbarselect,'Position',[0.1 0.05 0.5 0.02]);
          setappdata(netdisp,'Colorbaron',1);
          axis off;
          set(findobj('tag','cbaronmenu'),'checked','on');
    case 'off'
          cb=findobj('tag','colbar');         
          if ~isempty(cb)
             delete(cb);
          end
          set(findobj('tag','cbaronmenu'),'checked','off');          
    otherwise
         fprintf(1,'ERROR: Staus can be either ''on'' or ''off'' only');
end
if PDPAppdata.lognetwork
   statement = sprintf('%s (''%s'');',mfilename,status);
   updatelog(statement);
end
