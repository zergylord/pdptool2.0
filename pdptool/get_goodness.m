function dg = get_goodness()
global net;
dg =0.0;
if (net.testopts.harmony ==1 || numel(net.pool) <= 1 )
   return;
end
pools = {net.pool.name};
for plnum=2:numel(net.pool)
    p = net.pool(plnum);
    for prjn=1:numel(p.proj)
        from = p.proj(prjn).frompool;
        np = strmatch(from,pools);
        if np ==1  %bias connection
           dg = dg + sum(p.proj(prjn).weight .* p.activation');
           continue;
        end
        fromp = net.pool(np);
        wt = p.proj(prjn).weight;
        for i = 1:size(wt,1)
            if np == plnum %self block
               jstart = i+1;
            else
               jstart = 1;
            end
            for j=jstart:size(wt,2)
                dg = dg + (wt(i,j) * p.activation(i) * fromp.activation(j));
            end
        end
    end
    a(plnum-1).activation = p.activation;
    a(plnum-1).extinput = p.extinput;
end
if net.testopts.clamp ==0
  dg = dg * net.testopts.istr;
  for i=1:size(a,1)
      dg =dg + sum(a(i).activation .* (a(i).extinput * net.testopts.estr));
  end
end