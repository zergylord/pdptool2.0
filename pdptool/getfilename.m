function filename = getfilename(prefix,extension)
%GETFILENAME Returns unique filename in the current directory.
%
%   GETFILENAME(PREFIX,EXTENSION) searches through the current directory
%   for file names having extension, EXTENSION that start with PREFIX and
%   generates a unique name by attaching the next available positive number  
%   to the PREFIX string.
%   EXTENSION string must begin with a '.'

%   Author:      Sindhu Elsy John,
%                Center for the neural basis of Cognition,CMU.

index=1;
wildcard = sprintf('*%s',extension);
dirstruct = dir(wildcard);
allextfiles = {dirstruct.name};
X=regexp(allextfiles,['^',prefix]);
Y=cellfun('isempty',X);
Z=find(~Y);
if ~isempty(Z)
    files=allextfiles(Z);
    indarr=[];
    for i=1:numel(files)
        [fpath fname ext] = fileparts(files{i});
        if length(fname) <= length(prefix) 
           indarr(i,:)=0;
        else
        tail = fname(length(prefix)+1 : end);
        p = regexp(tail,'_');
        if ~isempty(p)
           indarr(i,:) = str2double(tail(1:p(1)-1));
        else
           indarr(i,:) = str2double(tail);
        end
%         indarr(i,:) = str2double(fname(length(prefix)+1 : end));
%         S =files{i}(1:end-2);
%         indarr(i,:)=str2double(S(length(prefix)+1 :end));
        end
    end
    index=max(indarr)+1;
end
filename=sprintf('%s%d%s',prefix,index,extension);