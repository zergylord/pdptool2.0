function colorbarselect(hObject,event)
handles=guihandles;
stype = get(handles.netdisplay,'SelectionType');
if strcmpi(stype,'normal')
   selectmoveresize;
else
   set(hObject,'SelectionHighlight','off');
end