function n = bias_define(n)
if ~isfield(n,'pool') || ~isfield(n.pool,'type') ||...
   ~strcmpi(n.pool(1).type,'bias')
   error('Bias pool must be defined as pool(1)');
end
n.pool(1).nunits = 1;
[tf,loc] = ismember({n.pool.type},'bias');
ind = find(loc);
if numel(ind) > 1
   fprintf(1,'There can be only one bias pool');
   n.pool(ind(2):ind(end))=[];
end