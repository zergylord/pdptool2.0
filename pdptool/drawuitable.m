function drawuitable(array,arrstring)
global properties;
MAX_ROWS = 10;
MAX_COLS = 10;
CELL_WIDTH = 60;
if ~isnumeric(properties.maxrows) || properties.maxrows < 1
    properties.maxrows = MAX_ROWS;
end
if properties.maxrows >= properties.numrows
   properties.maxrows = properties.numrows;
end
if ~isnumeric(properties.maxcols) || properties.maxcols < 1
    properties.maxcols = MAX_COLS;
end
if properties.maxcols >= properties.numcols
   properties.maxcols = properties.numcols;
end
if ~isnumeric(properties.cellwidth) || properties.cellwidth < 1
    properties.cellwidth = CELL_WIDTH;
end
% create column names and row names 
ncols = properties.startcol:properties.startcol+properties.numcols-1;
% ncols = 1:properties.numcols;
cnums = cellstr(num2str(ncols'));
cnums = strtrim(cnums)';
[properties.colnames{1:numel(ncols)}] = deal('C');
properties.colnames = strcat(properties.colnames,cnums);
nrows = properties.startrow:properties.startrow + properties.numrows -1;
% nrows = 1: properties.numrows;
rnums = cellstr(num2str(nrows'));
rnums = strtrim(rnums)';
[properties.rownames{1:numel(nrows)}] = deal('R');
properties.rownames = strcat(properties.rownames,rnums);

% % Get screensize in pixels
Units=get(0,'units'); 
set(0,'units','pixels'); 
ss = get(0,'ScreenSize');
set(0,'units',Units);
swidth = ss(3); 
sheight = ss(4);
button_size = 30;
slidersz= 15;
margin =15;
properties.cellheight = 17.5;

fig_width = 2*margin + button_size + properties.cellwidth +  ...
            properties.cellwidth * properties.maxcols;
if fig_width > swidth
   properties.maxcols = MAX_COLS;
   fig_width = 3*margin + slidersz + button_size + properties.cellwidth * ...
               properties.maxcols + (properties.maxcols - 1) * ...
               properties.cellborder + properties.cellwidth;
end
fig_height = 3*margin + properties.cellheight *properties.maxrows  + ...
             properties.cellheight + slidersz;
if fig_height < button_size
   fig_height = 2*margin + button_size;
end
if fig_height > sheight
   properties.maxrows = MAX_ROWS;
   fig_height = 2*margin + properties.cellheight * properties.maxrows + ...
             (properties.maxrows - 1) * properties.cellborder;
end
left = (swidth-fig_width)/2; 
bottom = (sheight-fig_height)/2;
figrect = [left bottom fig_width fig_height];
editorfig = figure('Name','Array Editor','tag','arreditor','Units','pixels',...
            'Position',figrect,'Menubar','none','NumberTitle','off',...
            'Color', get(0,'defaultUicontrolBackgroundColor'),...
            'Resize','on','Visible','on','CloseRequestFcn',{@closeeditor});
[widtharray{1:numel(properties.colnames)}] = deal(properties.cellwidth);
% tablewidth = properties.cellwidth*(size(properties.out,2)) + margin;
tablewidth = properties.cellwidth*properties.numcols + 2*margin;
if tablewidth > fig_width
   tablewidth = fig_width - (3*margin + button_size);
end
tableheight = properties.cellheight*(size(properties.out,1) + 2);
if tableheight > fig_height
   tableheight = fig_height - (3*margin);
end
pos = [30 30 tablewidth tableheight];
if strcmpi(properties.enableedit,'on')
   editable = true;
else
   editable = false;
end
table = uitable('data',properties.out,'ColumnWidth',widtharray,...
                'ColumnName',properties.colnames,'Position',pos,...
                'RowName',properties.rownames,'ColumnEditable',editable,...
                'tag','arraytbl');
btnpos = [fig_width - (button_size+margin), margin+slidersz,...
         button_size, button_size];
okbtn = uicontrol('Style','pushbutton','units','pixels','Position',btnpos,...
        'String','Ok','Callback',{@okbtn_callback,arrstring});    
    
function closeeditor(hObject, event,handles)
delete(hObject);        


function okbtn_callback(hObject,event,arrstring)
global net properties PDPAppdata; %net is used inside eval function
out = [];
if  strcmpi(properties.enableedit,'off')
    delete(findobj('tag','arreditor'));
    return;
end
        
% outcell = cell(properties.numrows,properties.numcols);
% outcell = reshape(cellstr(get(properties.hedits,'String')),properties.numrows,properties.numcols);
try
%     out = cellfun(@str2num,outcell);
    properties.out = get(findobj('tag','arraytbl'),'data');
    if (PDPAppdata.lognetwork)
        stmt = sprintf('%s = %s;',arrstring,mat2str(properties.out));
        updatelog(stmt);
    end
    arrstring = sprintf('%s = properties.out',arrstring);
    evalc(arrstring);
    delete(findobj('tag','arreditor'));
catch
    fprintf(1,'Error :cell values should be numeric');
end
% refresh scalar tree item with current value
if isscalar(properties.array)
   varlist = cellstr(get(findobj('tag','vartree'),'String'));
   curr = get(findobj('tag','vartree'),'Value');
   str = varlist{curr};
   [first,second] = strtok(str,':');
   if round(properties.out) == properties.out
      str = sprintf('%s: %d',first,properties.out);
   else
      str = sprintf('%s: %.2f',first,properties.out);
   end
   varlist{curr} = str;
   set(findobj('tag','vartree'),'String',varlist);
end
