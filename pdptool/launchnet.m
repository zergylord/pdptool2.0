function launchnet()
global PDPAppdata;
pause(0.01);
if PDPAppdata.gui
    if ~isempty(findobj('tag','netdisplay'))
       openfig('netdisplay.fig','reuse');
    else
       netdisplay;
    end
end