function varargout = tdbp_troption(varargin)
% TDBP_TROPTION M-file for tdbp_troption.fig
% This is called when user clicks on
% (i)  'Set Training options' item from the 'Network' menu of the pdp window.
% (ii) 'Options' button on the Train panel of the network viewer window.
% It presents training parameters for 'tdbp' type of network that can be 
% modified and saved.  

% Last Modified 03-May-2007 10:13:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tdbp_troption_OpeningFcn, ...
                   'gui_OutputFcn',  @tdbp_troption_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before tdbp_troption is made visible.
function tdbp_troption_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to tdbp_troption (see VARARGIN)

% The GUI opening function sets current/existing values of training 
% parameters to the corresponding GUI controls.
setcurrentvalues();

% Choose default command line output for tdbp_troption
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes tdbp_troption wait for user response (see UIRESUME)
% uiwait(handles.tdbp_troption);


% --- Outputs from this function are returned to the command line.
function varargout = tdbp_troption_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function lrateedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function lgrainpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function lgrainedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function nepochsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function wrangeedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function muedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function clearvaledit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function policypop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function nstepsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function istartedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function iduredit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function troptnfilepop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
trpatfile = trainpatternspopup_cfn(hObject);
setappdata(findobj('tag','tdbp_troption'),'patfiles',trpatfile);


function tpsedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function errorfnpop_CreateFcn(hObject, eventdata, handles)

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tduredit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tstartedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function troptnfilepop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow')

function lgrainpop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow')

function policypop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function errorfnpop_Callback(hObject, eventdata, handles)
set(hObject,'BackgroundColor','yellow');

function okbtn_Callback(hObject, eventdata, handles)
apply_options();
delete(handles.tdbp_troption);

function cancelbtn_Callback(hObject, eventdata, handles)

% Exits without making any changes
delete (handles.tdbp_troption);


function troptnloadpatbtn_Callback(hObject, eventdata, handles)
loadnewpat_cb(handles.troptnfilepop,handles.tdbp_troption);

function applybtn_Callback(hObject, eventdata, handles)
apply_options();


function writeoutchk_Callback(hObject, eventdata, handles)
writeout_cb(hObject,handles);


function setwritebtn_Callback(hObject, eventdata, handles)
% Calls the module that presents options for writing network output logs.
% Once the setwriteopts window is up, this makes 'train' as the default 
% logging process.

% setwriteopts;
create_edit_log;
logproc = findobj('tag','logprocpop');
if ~isempty(logproc)
    set(logproc,'Value',1);
end


function apply_options()
% This reads in the new parameters and applies changes to the network
% trainopts structure.
handles = guihandles;
global net PDPAppdata;
n = PDPAppdata.networks;

if isfield(net,'trainopts') 
   tropts = net.trainopts;
   tempopts = tropts;
end
tropts.epsilon = str2double(get(handles.epsilonedit,'String'));
tropts.nepochs = str2double(get(handles.nepochsedit,'String'));
polval = get(handles.policypop,'Value');
polstr = cellstr(get(handles.policypop,'String'));
tropts.policy = polstr{polval};
tropts.showvals = get(handles.showvalschkbox,'Value');
tropts.runstats = get(handles.runstatschkbox,'Value');
tropts.annealsched = getappdata(handles.annealbtn,'annealsched');
tropts.lflag = get(handles.learnchk,'Value');
tropts.lrate = str2double(get(handles.lrateedit,'String'));
tropts.wrange = str2double(get(handles.wrangeedit,'String'));
grainstr = get(handles.lgrainpop,'String');
grainval= get(handles.lgrainpop,'Value');
tropts.lgrain = lower(grainstr{grainval});
tropts.lgrainsize = str2double(get(handles.lgrainedit,'String'));
tropts.lambda = str2double(get(handles.lambdaedit,'String'));
tropts.gamma = str2double(get(handles.gammaedit,'String'));
tropts.wdecay = str2double(get(handles.wdecayedit,'String'));
tropts.mu = str2double(get(handles.muedit,'String'));
tropts.clearval = str2double(get(handles.clearvaledit,'String'));

patlist = get(handles.troptnfilepop,'String');
pfiles = getappdata(handles.tdbp_troption,'patfiles');
mainpfiles = PDPAppdata.patfiles;
if ~isempty(pfiles)
    lasterror('reset'); %in case error was caught when loading from main window    
    if isempty(mainpfiles)
       newentries = pfiles;
    else
        [tf, ind] = ismember(pfiles(:,2), mainpfiles(:,2));
        [rows, cols] = find(~tf);
        newentries = pfiles(unique(rows),:);
    end
    for i = 1 : size(newentries,1)
        loadpattern('file',newentries{i,2},'setname',newentries{i,1},...
                     'usefor','train');
        err = lasterror;
        if any(strcmpi({err.stack.name},'readpatterns'))
           delval = find(strcmp(patlist,newentries{i,1}),1);
           patlist(delval) = [];
           set(handles.troptnfilepop,'String',patlist,'Value',numel(patlist));
           pfiles(delval-1,:) = [];
           lasterror('reset');
        end
    end
end
plistval = get(handles.troptnfilepop,'Value');
tropts.trainset = patlist{plistval};

% -- Calls settrainopts command with only the modified fields of the 
%    trainopts structure. 
tempopts = orderfields(tempopts,tropts);
fields = fieldnames(tempopts);
tempA = struct2cell(tempopts);
tempB = struct2cell(tropts);
diffind = cellfun(@isequal,tempA,tempB); 
diffind = find(~diffind);
if ~isempty(diffind)
   trstmt = 'settrainopts (';
   args = cell(1,numel(diffind)*2);
   [args(1:2:end-1)] = fields(diffind);   
   for i = 1 : numel(diffind)
       fval =  tropts.(fields{diffind(i)});
       args{i*2} = fval;   
       if ischar(fval)
          trstmt = sprintf('%s''%s'',''%s'',',trstmt,...
                   fields{diffind(i)},fval);
       elseif numel(fval) > 1
          trstmt = sprintf('%s''%s'',%s,',trstmt,fields{diffind(i)},mat2str(fval)); 
       else
          trstmt = sprintf('%s''%s'',%g,',trstmt,fields{diffind(i)},fval);
       end
   end
   settrainopts(args{1:end});
   if PDPAppdata.lognetwork
      trstmt = sprintf('%s);',trstmt(1:end-1));
      updatelog(trstmt);
   end    
end
%update softmax temp
if ~isequal(tropts.annealsched,tempopts.annealsched)
    tdbp_settemp();
    netdisp = findobj('tag','netdisplay');
    if ~isempty(netdisp)
        update_display(0);
    end
end
set(findobj('tag','trpatpop'),'String',patlist,'Value',plistval);

% -- Reset interrupt flag if figure module is invoked from within
%    train panel of network viewer and training mode has been altered.
% netdisp = findobj('tag','netdisplay');
% if ~isempty(netdisp)
%    if ~isempty(prevmode) && ~strcmpi(prevmode,tropts.trainmode)
%       PDPAppdata.trinterrupt_flag = 0;
%    end  
% end
n{net.num} = net;
PDPAppdata.networks = n;
% If user applies changes without dismissing window
setappdata(handles.tdbp_troption,'patfiles',pfiles);

% -- All edited uicontrols that have KeypressFcn (done through guide) set  
%    to change background color to yellow is changed back to white.This 
%    indicates that modified fields have been saved.
set(findobj('BackgroundColor','yellow'),'BackgroundColor','white');


function setcurrentvalues
% This sets the current training parameter values to the corresponding GUI
% controls. 
global net;
handles = guihandles;
tropts = net.trainopts;
polmode =1;
set(handles.nepochsedit,'String',tropts.nepochs);
policylist = cellstr(get(handles.policypop,'String'));
if strcmp(net.netmode,'sarsa')  % remove 'defer' option if in SARSA mode
    ind = find(strcmpi(policylist,'defer'));
    policylist(ind) = [];
    set(handles.policypop,'String',policylist);
end
outpools = find(strcmpi({net.pool.type},'output'));
if strcmpi(net.pool(outpools(1)).actfunction,'linear')    % remove 'linearwt' options if outpool is linear (linearwt doesn't work for negative values)
    ind = find(strcmpi(policylist,'linearwt'));
    policylist(ind) = [];
    set(handles.policypop,'String',policylist);
end
ind = find(strncmpi(tropts.policy,policylist,length(tropts.policy)));
if ~isempty(ind)
    polmode = ind;
end
tropts.policy = policylist{polmode};
set(handles.policypop,'Value',polmode);
set(handles.epsilonedit,'String',tropts.epsilon);
set(handles.showvalschkbox,'Value',tropts.showvals);
set(handles.runstatschkbox,'Value',tropts.runstats);
set(handles.learnchk,'Value',tropts.lflag);
set(handles.lrateedit,'String',tropts.lrate);
set(handles.lambdaedit,'String',tropts.lambda);
set(handles.gammaedit,'String',tropts.gamma);
val = 1;
findgrain = find(strcmpi(get(handles.lgrainpop,'String'),tropts.lgrain));
if ~isempty(findgrain)
    val = findgrain;
end
set (handles.lgrainpop,'Value',val);
set (handles.lgrainedit,'String',tropts.lgrainsize);
set(handles.wdecayedit,'String',tropts.wdecay);
set (handles.wrangeedit,'String',tropts.wrange);
set (handles.muedit,'String',tropts.mu);
set (handles.clearvaledit,'String',tropts.clearval);
setappdata(handles.annealbtn,'annealsched',tropts.annealsched)
net.trainopts = tropts;  % in case some fields were changed from invalid to valid

function wdecayedit_Callback(hObject, eventdata, handles)

function wdecayedit_CreateFcn(hObject, eventdata, handles)

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Produces strange behavior is callback function to checkbox not defined.
% So  this must be left as is.
function learnchk_Callback(hObject, eventdata, handles)
Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function lambdaedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lambdaedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function gammaedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gammaedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in showvalschkbox.
function showvalschkbox_Callback(hObject, eventdata, handles)
% hObject    handle to showvalschkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of showvalschkbox

% --- Executes on button press in annealbtn.
function annealbtn_Callback(hObject, eventdata, handles)
% hObject    handle to annealbtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
getannealschedule;

% --- Executes on button press in runstatschkbox.
function runstatschkbox_Callback(hObject, eventdata, handles)
% hObject    handle to runstatschkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of runstatschkbox
