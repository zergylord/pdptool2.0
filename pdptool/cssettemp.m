function cssettemp()
global net ltemp ctemp ntemp coolrate;
%annealing stuff
ltemp = size(net.testopts.annealsched,1);
ntemp = 1;
ctemp=1;
anneal = net.testopts.annealsched;
net.goodness = 0;
net.updateno = 0;
net.cuname='';
if ~isempty(net.testopts.annealsched) && ltemp ~= ntemp
   ctemp=ntemp;
   ntemp = ntemp+1;
   coolrate = (anneal(ctemp,2) - anneal(ntemp,2))/anneal(ntemp,1);
   net.testopts.temp = annealing(net.cycleno);
end