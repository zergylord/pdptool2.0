function varargout = setnwseed(varargin)
% SETNWSEED M-file for setnwseed.fig
%      SETNWSEED, by itself, creates a new SETNWSEED or raises the existing
%      singleton*.
%
%      H = SETNWSEED returns the handle to a new SETNWSEED or the handle to
%      the existing singleton*.
%
%      SETNWSEED('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SETNWSEED.M with the given input arguments.
%
%      SETNWSEED('Property','Value',...) creates a new SETNWSEED or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before setnwseed_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to setnwseed_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help setnwseed

% Last Modified by GUIDE v2.5 10-Jan-2007 18:07:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @setnwseed_OpeningFcn, ...
                   'gui_OutputFcn',  @setnwseed_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before setnwseed is made visible.
function setnwseed_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to setnwseed (see VARARGIN)

% Choose default command line output for setnwseed
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes setnwseed wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = setnwseed_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function seededit_Callback(hObject, eventdata, handles)


function seededit_CreateFcn(hObject, eventdata, handles)
global net;
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String',net.seed);


function okbtn_Callback(hObject, eventdata, handles)
apply_seed();
delete(handles.setnwseed);

function applybtn_Callback(hObject, eventdata, handles)
apply_seed();

function cancelbtn_Callback(hObject, eventdata, handles)
delete(handles.setnwseed);

function apply_seed()
global PDPAppdata;
handles=guihandles;
seedval = str2double(get(handles.seededit,'String'));
setseed(seedval);
if PDPAppdata.lognetwork
   statement = sprintf('setseed(%g);',seedval);
   updatelog(statement);
end
