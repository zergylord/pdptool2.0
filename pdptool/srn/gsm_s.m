% In nogui mode, this script can be run by typing 'pdptool nogui gsm_s.m'
% on the matlab command line.
pdp;
loadscript ('gsm.net');
loadtemplate ('gsm.tem')
loadpattern ('file','gsm21_s.pat','usefor','both');
settrainopts('momentum', 0);
settrainopts ('lrate',0.1,'wrange',0.1,'lgrain','Pattern','mu',0.0);
settrainopts ('nepochs',50);
settrainopts ('trainmode','ptrain');
srn_reset;
launchnet;