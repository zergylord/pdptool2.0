% In nogui mode, this script can be run by typing 'pdptool nogui gsm.m'
% on the matlab command line.
pdp;
loadscript ('gsm.net');
loadtemplate ('gsm.tem')
loadpattern ('file','gsm21.pat','usefor','both');
settestopts ('mu',0);
settrainopts('momentum', 0,'mu',0);
settrainopts ('lrate',0.1,'wrange',0.1,'lgrain','pattern');
settrainopts ('nepochs',50,'trainmode','ptrain');
srn_reset;
launchnet;