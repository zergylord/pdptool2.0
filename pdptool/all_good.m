function goodness = all_good()
boltzcube;
global net
act = dec2bin(0:2^16-1) - '0';
goodness = zeros(1,2^16);
for i=1:(2^16)
   net.pool(2).activation = act(i,:);
goodness(i) = get_goodness;
end