function [valid, data,errmsg] = read_pair_rbpdata(Filename)
global net;
valid=0;
data=[];
errmsg='';
fid = fopen (Filename,'r');
tline='';
lcount = 0;
while isempty(tline)
      tline = strtrim(fgetl(fid));
      lcount = lcount +1;
end
headerparts =strsplit(':',tline);
tickinfo = strsplit(' ',strtrim(headerparts{2}));
tickinfo = tickinfo(~cellfun(@isempty,tickinfo));
if ~strcmpi(strtrim(headerparts{1}),'rbp') || numel(tickinfo) < 2 || ...
    isnan(str2double(tickinfo{1})) || isnan(str2double(tickinfo{2}))
    fprintf(1,'Error reading %s : Invalid header information\n',Filename);
    return;
end
nsteps = str2double(tickinfo{1});
tps =str2double(tickinfo{2});
data= struct('pname','','inpools',struct('poolname','','poolnum',[],'tickpatterns',{}),...
             'outpools',struct('poolname','','poolnum',[],'tickpatterns',{}));
tline='';
while isempty(tline)
      tline = strtrim(fgetl(fid));
      lcount = lcount +1;
end
dno =1;
validpools = {net.pool.name};
while tline ~= -1
     data(dno).pname = tline;
     data(dno).ipattern = ' ';     
      try
          tline = strtrim(fgetl(fid));
          lcount = lcount+1;
          while isempty(tline)
                tline = strtrim(fgetl(fid));
                lcount = lcount +1;
          end
      catch
          break;
      end      
      while ~strncmpi(tline,'end',3)
             pstring = strsplit(' ',tline);  
             ind= ~cellfun(@isempty,pstring);
             pstring = pstring(ind);% get a list of non-white space entries of the line;
             if strncmpi(pstring{1},'H',1) || strncmpi(pstring{1},'S',1)
                 if isempty(data(dno).inpools)
                    data(dno).inpools = struct('poolname','','poolnum',[],...
                                        'tickpatterns',{},'hclamps',[]);
                 end
%                  j=numel(data(dno).inpools)+1;
%                  data(dno).inpools(j).hclamps = repmat(0,1,nsteps*tps+1);
                 pool = pstring{4};
                 poolindex = find(strcmpi(validpools,pool),1);                
                 if isempty(poolindex) %~any(strcmpi(validpools,pool))
                    errmsg = sprintf('Line %d - invalid pool name ''%s''\n',...
                             lcount,pool);
                    data = [];
                    return;
                 end
                 istart = str2double(pstring{2});
                 idur = str2double(pstring{3});
                 vec = str2num(char(pstring{5:end}))';
                 if isfield(data(dno).inpools,'poolname')
                     t= find(strcmpi({data(dno).inpools.poolname},pool),1);
                 end
                 if isempty(t)
                      j=numel(data(dno).inpools)+1;
                      data(dno).inpools(j).hclamps = repmat(0,1,nsteps*tps+1);   
                 else
                     j=t;
                 end
                 data(dno).inpools(j).poolname =pool;
                 data(dno).inpools(j).poolnum =poolindex;
                 st = (istart-1)* tps +1;
                 et= st+idur*tps-1;
                 if et > nsteps * tps +1
                    errmsg = sprintf(['Line %d - total ticks less than '...
                             'specified duration\n'],lcount);
                    data=[];
                    return;
                 end
                 data(dno).inpools(j).tickpatterns{end+1,1}=st: et;
                 data(dno).inpools(j).tickpatterns{end,2} = vec;
                 data(dno).inpools(j).hclamps(st:et) = fix('H'/pstring{1});
                 
             end
             if strncmpi(pstring{1},'T',1)
                 if isempty(data(dno).outpools)
                     data(dno).outpools = struct('poolname','','poolnum',[],'tickpatterns',{});
                 end                 
                 j=numel(data(dno).outpools)+1;
                 pool= pstring{4};
                 poolindex = find(strcmpi(validpools,pool),1);                
                 if isempty(poolindex) %~any(strcmpi(validpools,pool))
                    errmsg = sprintf('Line %d - invalid pool name ''%s''\n',...
                             lcount,pool);
                    data = [];
                    return;
                 end                
                 istart = str2double(pstring{2});
                 idur = str2double(pstring{3});
                 vec = str2num(char(pstring{5:end}))';
                 t= find(strcmpi({data(dno).outpools.poolname},pool),1);
                 if ~isempty(t)
                     j=t;
                 end
                 data(dno).outpools(j).poolname =pool;
                 data(dno).outpools(j).poolnum =poolindex;                 
                 st = (istart-1)* tps +2;
                 et= st+idur*tps-1;
                 if et > (nsteps * tps + 1 )
                    et = et - 1;
                 end
                 if et > nsteps * tps +1
                    errmsg = sprintf(['Line %d - total ticks less than '...
                             'specified duration\n'],lcount);
                    data=[];
                    return;
                 end                 
                 data(dno).outpools(j).tickpatterns{end+1,1}=st: et;
                 data(dno).outpools(j).tickpatterns{end,2} = vec;
                 
             end 
             try
                 tline = strtrim(fgetl(fid));
                 lcount = lcount+1;
                 while isempty(tline)
                       tline = strtrim(fgetl(fid));
                       lcount = lcount +1;
                 end                
             catch
                 break;
             end
      end
      try
          tline = strtrim(fgetl(fid));
          lcount = lcount+1;
          while isempty(tline)
                tline = strtrim(fgetl(fid));
                lcount = lcount +1;
          end         
      catch
          break;
      end        
      dno = dno+1;
end
net.nintervals = nsteps;
net.ticksperinterval = tps;
net.nticks = nsteps * tps;
valid =1;
