function setcolormap(File)
netdisp =findobj('tag','netdisplay');
m = load(File);
if isstruct(m)
   mf = fieldnames(m);
   map_matrix = m.(mf{1});
else
   map_matrix=m;
end
set(netdisp,'Colormap',map_matrix);