
semnet
loadpattern ('file','EightThings.pat','setname','EightThings','usefor','both');
bp_reset;
settrainopts ('trainset','EightThings','fastrun',1);
runprocess ('process','train','granularity','epoch','count',1,'range',[1 1;32 500]);
runprocess ('granularity','pattern','count',1,'alltest',1,'range',[1 500;5 500]);