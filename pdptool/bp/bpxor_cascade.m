% In nogui mode, this script can be run by typing 'pdptool nogui bpxor.m'
% on the matlab command line.
pdp;
loadscript ('xor.net');
loadpattern ('file','xor.pat','usefor','both');
loadtemplate ('xor_cascade.tem')
settrainopts ('nepochs',30,'ecrit',0.04,'lgrain','epoch');
settestopts ('cascade',1);
launchnet;
loadweights ('xor.wt');
runprocess ('process','test','mode','run','granularity','pattern','count',1,'alltest',1,'range',[1 0;4 0]);
% This can be also be written as runprocess('alltest',1, range[1 0;4 0])
% since the rest of the properties have default values as above.