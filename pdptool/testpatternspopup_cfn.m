function tstpatfile = testpatternspopup_cfn(popuphandle)

% This is a common function for all <net_type>_testoption M files that have
% a pattern list popup menu item.It is called when the pop-up menu labelled
% 'Pattern files' is created. The purpose of this routine is to create
% appdata for <net_type>_testoption figure handle from pdp handle 
% appdata by extracting details of all test patterns currently in the Test 
% pattern list of the main window . 'patfiles' is a cell array of strings ,
% each row of which contains a pattern set name and its corresponding 
% file path. The Pattern file popup menu is then populated with the current
% list.

% Last Modified 02-May-2007 11:32:15

global PDPAppdata;
tstpatfile = [];
pdp_handle = findobj('tag','mainpdp');
if isempty(pdp_handle)
   disp('Can only be called when running pdp in gui mode');
   return;
end
testlist = findobj('tag','testpatpop');
currentpats = cellstr(get(testlist,'String'));
val = get(testlist,'Value');
patfiles = PDPAppdata.patfiles;
if ~isempty(patfiles)
   patsetnames = patfiles(:,1);
   [foundmem, memindex] = ismember(currentpats,patsetnames);
   memindex = memindex(memindex > 0 );
   tstpatfile = patfiles(memindex,:);
end
set (popuphandle,'String',currentpats,'Value',val);