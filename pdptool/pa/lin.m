% In nogui mode, this script can be run by typing 'pdptool nogui lin.m'
% on the matlab command line.
pdp ;
loadscript ('pa.net');
settrainopts ('nepochs',1,'actfunction','li','lrate',0.125,'lrule','hebb');
loadpattern ('file','one.pat','setname','one','usefor','both');
settestopts ('actfunction','li');
loadtemplate ('8X8.tem');
launchnet;