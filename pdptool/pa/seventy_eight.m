% In nogui mode, this script can be run by typing 'pdptool nogui
% seventy_eight.m' on the matlab command line.
pdp ;
loadscript ('pa.net');
loadtemplate ('8x8.tem');
loadpattern ('file','78.pat','usefor','both');
settrainopts ('nepochs',10,'lrate',0.05,'trainmode','ptrain','temp',1.0);
settestopts('temp',1);
launchnet;