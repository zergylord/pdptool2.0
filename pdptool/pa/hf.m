% In nogui mode, this script can be run by typing 'pdptool nogui hf.m'
% on the matlab command line.
pdp ;
loadscript ('pa.net');
loadpattern ('file','hf.pat','usefor','train');
settrainopts ('nepochs',10,'actfunction','li');
settestopts ('actfunction','li');
loadtemplate ('8X8.tem');
launchnet;