% In nogui mode, this script can be run by typing 'pdptool nogui ct.m'
% on the matlab command line.
pdp ;
loadscript ('pa.net');
loadpattern ('file','ortho.pat','setname','Ortho','usefor','both');
logfname = getfilename('cttsslog','.mat');
setoutputlog ('file',logfname,'process', 'train','frequency', 'epoch','status', 'on','writemode', 'binary','objects', {'epochno','tss'},'onreset', 'new','plot', 'on','plotlayout', [1 1]);
setplotparams ('file', logfname, 'plotnum', 1, 'yvariables', {'tss'}, 'ylim', [0 40]);
settrainopts ('nepochs',1,'lrate',0.0125,'noise',0.5,'actfunction','li','nepochs',10);
settestopts ('actfunction','li');
loadtemplate ('8X8.tem');
launchnet;