function probs = goodness_probs(goods)


vals = unique(goods);
len = length(vals);
goodcount = zeros(1,len);
one = ones(size(goods));
for i=1:len
    goodcount(i) = sum(one(goods==vals(i)));
end
numer = zeros(1,len);
for i=1:len
    numer(i) = (goodcount(i)*exp(vals(i)/.5));
end
denom = sum(numer);
probs = zeros(1,len);
for i=1:len
    probs(i) = numer(i)/denom;
end