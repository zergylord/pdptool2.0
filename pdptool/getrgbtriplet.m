function colorvec = getrgbtriplet(value,vcs)
global net;
colorvec = repmat(0,numel(value),3);
netdisp = findobj('tag','netdisplay');
if isempty(netdisp)
   return;
end   
scaledval = value ./(abs(value)+1/vcs);
map = get(netdisp,'Colormap');
cind = ((size(map,1) -1)/2)*scaledval + (size(map,1) +1)/2;
cind_int = floor(cind);
cind_rem = cind - cind_int;
nanindex = find(isnan(cind_int));
if ~isempty(nanindex)
   colorvec(nanindex,:) = repmat(net.nancolor,numel(nanindex),1);
end
notnan1 = find(~isnan(cind_int) & cind_int > 0 & cind_int < size(map,1));
notnan2 = find(~isnan(cind_int) & cind_int > 0 & cind_int >= size(map,1));
notnan3 = find(~isnan(cind_int) & cind_int <= 0);
if ~isempty(notnan1)
   rem = cind_rem(notnan1);
   cindex = cind_int(notnan1);
   if size(rem,2) > 1
      rem = rem';
   end
   colorvec(notnan1,:) = repmat((1-rem),1,3) .* map(cindex,:) + ...
                    repmat(rem,1,3) .* map(cindex+1,:);
end 
if ~isempty(notnan2)
   colorvec(notnan2,:) = repmat(map(end,:),numel(notnan2),1);
end
if ~isempty(notnan3)
   colorvec(notnan3,:) = repmat(map(1,:),numel(notnan3),1);
end